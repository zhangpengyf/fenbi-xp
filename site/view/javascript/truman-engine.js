/*jshint unused:false*/
'use strict';

//NOT sure if it's best practise
var EventListenerService = window.EventListenerService;

function userEnterRoom(userId, userType,userRight,nickname) {
    var nickName = decodeURIComponent(window.escape(window.atob(nickname)));
    EventListenerService.trigger('room.user.enterRoom', userId, userType,userRight,nickName);
}

function userQuitRoom (userId) {
    EventListenerService.trigger('room.user.quitRoom', userId);
}

function micSetTimer(len) {
    EventListenerService.trigger('room.setting.microphone.timer', len * 1000);
}

function onRoomInfo (teacherList, studentList,
    isMicrophoneQueueOpen, microphoneList, speakingUserList,
    userCount, startTime, latestMessageId, latestStrokeId,
    isAssistantOnBoard, assistantList, assistantUserId, latestTopMessageId, bannedUserList,micStatus,isVideoMicOpen,largeUid,question) {

    console.log('onRoomInfo ' + speakingUserList + ' ' + JSON.parse(speakingUserList));

    EventListenerService.trigger(
        'room.onRoomInfo',
        teacherList,
        JSON.parse(studentList),
        isMicrophoneQueueOpen,
        microphoneList,
        JSON.parse(speakingUserList),
        userCount,
        startTime,
        latestMessageId,
        latestStrokeId,
        isAssistantOnBoard,
        assistantList,
        assistantUserId,
        latestTopMessageId,
        bannedUserList,
        JSON.parse(micStatus),
        isVideoMicOpen,
        largeUid,
        JSON.parse(question)
    );
}

function onSyncRoomInfo (userCount) {
    EventListenerService.trigger('room.sync.info', userCount);
}

function onMicrophoneApplied (userId, roomId,nickname) {
    var nickName = decodeURIComponent(window.escape(window.atob(nickname)));
    var scope = angular.element(document.getElementById('microphone-queue-wrapper')).scope();
    if (scope) {
        scope.safeApply(function () {
            scope.onMicrophoneApplied(userId, roomId,nickName);
        });
    }
}

function onMicrophoneCanceled (userId, roomId, senderId, senderType) {
    var scope = angular.element(document.getElementById('microphone-queue-wrapper')).scope();
    if (scope) {
        scope.safeApply(function () {
           scope.onMicrophoneCanceled(userId, roomId, senderId, senderType);
        });
    }
}

function onMicrophoneCancelAll(roomId) {
    var scope = angular.element(document.getElementById('microphone-queue-wrapper')).scope();
    if (scope) {
        scope.safeApply(function () {
            scope.onMicrophoneCancelAll(roomId);
        });
    }
}

function onMicrophoneApproved (userId, roomId,channelId,userType,nickname,status) {
    var nickName = decodeURIComponent(window.escape(window.atob(nickname)));
    var scope = angular.element(document.getElementById('microphone-queue-wrapper')).scope();
    if (scope) {
        scope.safeApply(function () {
            scope.onMicrophoneApproved(userId, roomId,channelId,userType,nickName,status);
        });
    }
}

function onMicrophoneSettingTimer (micId,validTime) {
    console.log('onMicrophoneSettingTimer ' + micId + ' ' + validTime);
    var scope = angular.element(document.getElementById('microphone-queue-wrapper')).scope();
    if (scope) {
        scope.safeApply(function () {
            scope.onMicrophoneSetTimer(micId, validTime);
        });
    }
}

function onMicrophoneQueueOpened (roomId) {
    var scope = angular.element(document.getElementById('microphone-queue-wrapper')).scope();
    if (scope) {
        scope.safeApply(function () {
            scope.onMicrophoneQueueOpened(roomId);
        });
    }
}

function onMicrophoneQueueClosed (roomId) {
    var scope = angular.element(document.getElementById('microphone-queue-wrapper')).scope();
    if (scope) {
        scope.safeApply(function () {
            scope.onMicrophoneQueueClosed(roomId);
        });
    }
}

function OnVideoMicEvent (roomId,state) {
    var scope = angular.element(document.getElementById('microphone-queue-wrapper')).scope();
    var episodes = document.getElementsByClassName('episode-page-wrapper');
    var roomScope;
    if (episodes.length > 0) {
        roomScope = angular.element(episodes[0]).scope();
    } else {
        roomScope = angular.element(document.getElementsByClassName('replay-wrapper')[0]).scope();

    }

    if (scope) {
        scope.safeApply(function () {
            var radio = state > 0 ? true : false;
            scope.onVideoMicStateChanged(roomId,radio);
            roomScope.switchVideoState(state);
        });
    }
}

function OnVideoStyleEvent (roomId,state) {
    var scope = angular.element(document.getElementById('microphone-queue-wrapper')).scope();
    if (scope) {
        scope.safeApply(function () {
            scope.onVideoDisplayStatusChange(roomId, state);
        });
    }
}

function onChatMessage(roomId, userId, messageId, clientMessageId, contentBase64, timestamp, type,nickname) {
    var content = decodeURIComponent(window.escape(window.atob(contentBase64)));
    var nickName = decodeURIComponent(window.escape(window.atob(nickname)));

    var scope = angular.element(document.getElementById('chat-panel')).scope();
    if (scope) {
        scope.safeApply(function () {
            scope.onChatMessage(roomId, userId, messageId, clientMessageId, content, timestamp, type,nickName);
        });
    }
}

function onChatMessageFailed(roomId, userId, clientMessageId) {
    var scope = angular.element(document.getElementById('chat-panel')).scope();
    if (scope) {
        scope.safeApply(function () {
            scope.onChatMessageFailed(roomId, userId, clientMessageId);
        });
    }
}

function onMediaInfo (totalTime, nptList, pageToList) {
    var pageToPointList = [];
    for (var i = 0; i < nptList.length; i ++) {
        pageToPointList.push({
            npt: nptList[i],
            pageTo: pageToList[i]
        });
    }

    EventListenerService.trigger('replay.onMediaInfo', totalTime, pageToPointList);
}

function updateError () {
    var scope = angular.element(document.getElementById('update-state')).scope();
    if (scope) {
        scope.safeApply(function () {
            scope.updateError();
        });
    } else {
        scope = angular.element(document.getElementById('update-available')).scope();
        if (scope) {
            scope.safeApply(function () {
                scope.updateError();
            });
        }
    }
}

function noUpdate () {
    var scope = angular.element(document.getElementById('update-state')).scope();
    if (scope) {
        scope.safeApply(function () {
            scope.noUpdate();
        });
    }
}

function updateAvailable () {
    var scope = angular.element(document.getElementById('update-state')).scope();
    if (scope) {
        scope.safeApply(function () {
            scope.updateAvailable();
        });
    }
}

function updateDownloaded () {
    var scope = angular.element(document.getElementById('update-available')).scope();
    if (scope) {
        scope.safeApply(function () {
            scope.updateDownloaded();
        });
    }
}

function updateDownloadProgress (progress) {
    var scope = angular.element(document.getElementById('update-available')).scope();
    if (scope) {
        scope.safeApply(function () {
            scope.updateDownloadProgress(progress);
        });
    }
}

function onPageTo (pageIndex,strokeCached) {
    var episodeScope = angular.element(document.getElementById('episodePPT')).scope();
    var replayScope = angular.element(document.getElementById('replayPPT')).scope();

    if (episodeScope) {
        episodeScope.safeApply(function () {
            episodeScope.onPageTo(pageIndex,strokeCached);
        });
    }
    if (replayScope) {
        replayScope.safeApply(function () {
            replayScope.onPageTo(pageIndex);
        });
    }
}

function onKeynoteInfo (keynoteId, totalPageNum, currentPageIndex, maxStrokePageNum) {
    EventListenerService.trigger(
        'episode.keynote.onKeynoteInfo',
        keynoteId,
        totalPageNum,
        currentPageIndex,
        maxStrokePageNum
    );
    EventListenerService.trigger(
        'replay.keynote.onKeynoteInfo',
        keynoteId,
        totalPageNum,
        currentPageIndex,
        maxStrokePageNum
    );
}

function onSyncStroke (pageNum, points,width,color) {
    var padding = 6;
    var hex = Number(color).toString(16);
    while (hex.length < padding) {
        hex = '0' + hex;
    }

    EventListenerService.trigger(
        'room.stroke.onSyncStroke',
        pageNum,
        points,
        width,
        hex
    );
}

function onEraseStroke (pageNum) {
    EventListenerService.trigger('room.stroke.onEraseStroke', pageNum);
}

function onStartClass (startTime) {
    EventListenerService.trigger(
        'room.onStartClass',
        startTime
    );
}

function onEndClass () {
    EventListenerService.trigger('room.onEndClass');
}

function onSyncMedia() {
    var scope = angular.element(document.getElementById('replay-player')).scope();
    if (scope) {
        scope.safeApply(function () {
            scope.onSyncMedia();
        });
    }
}

function onOAuthLoginSuccess() {
    var scope = angular.element(document.getElementById('login')).scope();
    if (scope) {
        scope.safeApply(function () {
            scope.requestAccount();
        });
    }
}

function onAssistantOnBoard(userId) {
    var scope = angular.element(document.getElementById('microphone-queue-wrapper')).scope();
    if (scope) {
        scope.safeApply(function () {
            scope.onAssistantOnBoard(userId);
        });
    }
}

function onAssistantOffBoard(userId) {
    var scope = angular.element(document.getElementById('microphone-queue-wrapper')).scope();
    if (scope) {
        scope.safeApply(function () {
            scope.onAssistantOffBoard(userId);
        });
    }
}

function OnUserBanned(executioner, userId) {
    var scope = angular.element(document.getElementById('microphone-queue-wrapper')).scope();
    if (scope) {
        scope.safeApply(function () {
            scope.onUserBanned(userId);
        });
    }
}

function OnUserUnBanned(liberator, userId) {
    var scope = angular.element(document.getElementById('microphone-queue-wrapper')).scope();
    if (scope) {
        scope.safeApply(function () {
            scope.onUserUnBanned(userId);
        });
    }
}

function OnTopMessageCanceled() {
    var scope = angular.element(document.getElementById('chat-panel')).scope();
    if (scope) {
        scope.safeApply(function () {
            scope.onTopMessageCanceled();
        });
    }
}
function getStorageData(key) {
    EventListenerService.trigger('replay.storageData.get', key);
}

function OnNetStatistics(delay,lost,rtt,recvbytes,sendbytes) {
    var scope = angular.element(document.getElementById('epi-footer')).scope();
    if (scope) {
        scope.safeApply(function () {
            scope.onNetStatistics(delay,lost,rtt,recvbytes,sendbytes);
        });
    }

    lost = lost  / 256.0;
    EventListenerService.trigger('av.network.status', lost);
}

function OnServerChannged(index) {
    console.log('server changed ' + index);
}


function onVideoData(mic,rotation,url) {
    var event = 'room.video.event.' + mic;
    EventListenerService.trigger(event,rotation,url);
}

function OnDeviceEvent(userId,hasAudio,hasVideo) {
    EventListenerService.trigger('room.user.device.event', userId,hasAudio,hasVideo);
}

function onRecMediaUpdProgress(current,total) {
    EventListenerService.trigger('rec.media.upload.progress', current,total);
    console.log('onRecMediaUpdProgress ' + current + ' ' + total);
}

function OnUploadClientLog(type,log) {
    EventListenerService.trigger('upload.client.log', type,log);
}

function OnEndQuestion(questionId) {
    EventListenerService.trigger('room.question.end', questionId);
}

function OnRemoveQuestion(questionId) {
    EventListenerService.trigger('room.question.remove', questionId);
}

function OnAddQuestion(question) {
    EventListenerService.trigger('room.question.add', question);
}

function OnAnswerSummary(summary) {
    EventListenerService.trigger('room.question.summary', summary);
}

function OnMyAnswer(answer) {
    EventListenerService.trigger('room.question.myanswer', answer);
}