'use strict';

var messagePrefix = 'engine.';
var maxVolume = 255;
module.exports = {
    /*
        callback = function (retCode, volumePercent) {};
    */
    getOutputVolume: function (callback) {
        truman.sendMessage(messagePrefix + 'getSpeakerVolume', [], callback);
    },
    setOutputVolume: function (volumePercent, callback) {
        truman.sendMessage(messagePrefix + 'setSpeakerVolume', [Math.round(volumePercent*maxVolume)], callback);
    },
    /*
        callback = function (retCode, volumePercent) {};
    */
    getInputVolume: function (callback) {
        truman.sendMessage(messagePrefix + 'getMicrophoneVolume', [], callback);
    },
    setInputVolume: function (volumePercent, callback) {
        truman.sendMessage(messagePrefix + 'setMicrophoneVolume', [Math.round(volumePercent*maxVolume)], callback);
    },
    muteOutput: function (callback) {
        truman.sendMessage(messagePrefix + 'muteSpeaker', [], callback);
    },
    unMuteOutput: function (callback) {
        truman.sendMessage(messagePrefix + 'unMuteSpeaker', [], callback);
    },
    /*
        callback = function (retCode, isMute) {};
    */
    isOutputMute: function (callback) {
        truman.sendMessage(messagePrefix + 'isSpeakerMute', [], callback);
    },
    muteInput: function (callback) {
        truman.sendMessage(messagePrefix + 'muteMicrophone', [], callback);
    },
    unMuteInput: function (callback) {
        truman.sendMessage(messagePrefix + 'unMuteMicrophone', [], callback);
    },
    /*
        callback = function (retCode, isMute) {};
    */
    isInputMute: function (callback) {
        truman.sendMessage(messagePrefix + 'isMicrophoneMute', [], callback);
    },

    isSysInputMute: function (callback) {
        truman.sendMessage(messagePrefix + 'isSysMicrophoneMute', [], callback);
    },

    setSysInputMute: function (mute,callback) {
        truman.sendMessage(messagePrefix + 'setSysMicrophoneMute', [mute], callback);
    },
    /*
        Gets the microphone speech |level|
        param: callback = function (retCode, inputLevel) {};
        note: inputLevel mapped non-linearly to the range [0,9].
    */
    getInputLevel: function (isTestEngine, callback) {
        truman.sendMessage(messagePrefix + 'getMicrophoneLevel', [isTestEngine], callback);
    },
    startMicrophoneTest: function (callback) {
        truman.sendMessage(messagePrefix + 'startMicrophoneTest', [], callback);
    },
    endMicrophoneTest: function (callback) {
        truman.sendMessage(messagePrefix + 'endMicrophoneTest', [], callback);
    },
    /*
        callback = function (retCode, devicesNameArray)
    */
    getRecordingDevices: function (callback) {
        truman.sendMessage(messagePrefix + 'getRecordingDevices', [], callback);
    },

    setRecordingDevice: function (deviceIndex, callback) {
        truman.sendMessage(messagePrefix + 'setRecordingDevice', [deviceIndex], callback);
    },

    getVideoCaptureDevices: function (callback) {
        truman.sendMessage(messagePrefix + 'getVideoCaptureDevices', [], callback);
    },

    setVideoCaptureDevice: function (deviceIndex, callback) {
        truman.sendMessage(messagePrefix + 'setVideoCaptureDevice', [deviceIndex], callback);
    },

    openVideoPreview: function (callback) {
        truman.sendMessage(messagePrefix + 'openVideoPreview', [], callback);
    },

    closeVideoPreview: function ( callback) {
        truman.sendMessage(messagePrefix + 'closeVideoPreview', [], callback);
    },
};