'use strict';

var messagePrefix = 'file_api.';

module.exports = {
    saveKeynote: function (fileName, content, callback) {
        truman.sendMessage(messagePrefix + 'saveKeynote', [fileName, content], callback);
    },
    getKeynoteDir: function (callback) {
        truman.sendMessage(messagePrefix + 'getKeynoteDir', [], callback);
    },
    showOpenDialog: function (allowMultipleSelection, chooseDirectory, title, initialPath, fileTypes, callback) {
        setTimeout(function () {
            var showOpenDialogOptions = [
                allowMultipleSelection,
                chooseDirectory,
                title || 'Open',
                initialPath || '',
                fileTypes ? fileTypes.join(' ') : '',
            ];
            truman.sendMessage(messagePrefix + 'showOpenDialog', showOpenDialogOptions, callback);
        }, 10);
    },
    copyFile: function (srcFilePath, dstFileName, callback) {
        truman.sendMessage(messagePrefix + 'copyFile', [srcFilePath, dstFileName], callback);
    },
    getCookie:function(key, callback) {
        truman.sendMessage(messagePrefix + 'getCookie', [key], callback);
    },
    setCookie:function(url, key, value, host, callback) {
        truman.sendMessage(messagePrefix + 'setCookie', [url, key, value, host], callback);
    },
    deleteCookie: function(url, key, callback) {
        truman.sendMessage(messagePrefix + 'deleteCookie', [url, key], callback);
    },
    flushCookie: function(callback) {
        truman.sendMessage(messagePrefix + 'flushCookie', [], callback);
    }
};