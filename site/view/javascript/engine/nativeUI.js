'use strict';

var messagePrefix = 'nativeUI.';

module.exports = {
    maximize: function () {
        truman.sendMessage(messagePrefix + 'maximize', [], null);
    },
    minimize: function () {
        truman.sendMessage(messagePrefix + 'minimize', [], null);
    },
    toggleFullscreen: function () {
        truman.sendMessage(messagePrefix + 'toggleFullscreen', [], null);
    },
    isFullscreen: function (callback) {
        truman.sendMessage(messagePrefix + 'isFullscreen', [], callback);
    },
    move: function (x, y, sendMoving) {
        truman.sendMessage(messagePrefix + 'move', [x, y, sendMoving || true], null);
    },
    sizeTo: function (width, height, centering) {
        truman.sendMessage(messagePrefix + 'sizeTo', [width, height, centering ||  true], null);
    },
    close: function () {
        truman.sendMessage(messagePrefix + 'close', [], null);
    },
    restore: function () {
        truman.sendMessage(messagePrefix + 'restore', [], null);
    },
    openBrowser: function (url) {
        truman.sendMessage(messagePrefix + 'openBrowser', [url], function(ret){
            if(ret <= 32) {
                console.log('openBrowser ' + ret);
            }
        });
    },
    /*
        callback = function (retCode, idleIntervalInSeconds);
    */
    getIdleInterval: function (callback) {
        truman.sendMessage(messagePrefix + 'getIdleInterval', [], callback);
    },
    setMinWidth: function (minWidth) {
        truman.sendMessage(messagePrefix + 'setMinWidth', [minWidth], null);
    },
    setMinHeight: function (minHeight) {
        truman.sendMessage(messagePrefix + 'setMinHeight', [minHeight], null);
    },
    setMaxWidth: function (maxWidth) {
        truman.sendMessage(messagePrefix + 'setMaxWidth', [maxWidth], null);
    },
    setMaxHeight: function (maxHeight) {
        truman.sendMessage(messagePrefix + 'setMaxHeight', [maxHeight], null);
    },
    setTitleBarArea: function (height, clickThroughAreas) {
        truman.sendMessage(messagePrefix + 'setTitleBarArea', [height, clickThroughAreas], null);
    }
};