'use strict';

var messagePrefix = 'offlinePlay.';

module.exports = {
    openMedia: function (ticket, callback) {
        var ticketInList = [
            parseInt(ticket.id),
            parseInt(ticket.type),
            parseInt(ticket.userId),
            parseInt(ticket.userType),
            ticket.host.toString(),
            parseInt(0),
            parseInt(0),
            parseInt(0),
            ticket.signature.toString(),
            ticket.cookie.toString()
        ];
        truman.sendMessage(messagePrefix + 'openMedia', ticketInList, callback);
    },
    closeMedia: function (callback) {
        truman.sendMessage(messagePrefix + 'closeMedia', [], callback);
    },
    playMedia: function (callback) {
        truman.sendMessage(messagePrefix + 'playMedia', [], callback);
    },
    getPlayProgress: function (callback) {
        truman.sendMessage(messagePrefix + 'getPlayProgress', [], callback);
    },
    stopMedia: function (callback) {
        truman.sendMessage(messagePrefix + 'stopMedia', [], callback);
    },
    pauseMedia: function (callback) {
        truman.sendMessage(messagePrefix + 'pauseMedia', [], callback);
    },
    seekMedia: function (seekedMillseconds, callback) {
        truman.sendMessage(messagePrefix + 'seekMedia', [seekedMillseconds], callback);
    },
    setStorageData: function (key, data, status, callback) {
        truman.sendMessage(messagePrefix + 'setStorageData', [key, data, status], callback);
    },
    setPlaySpeed: function (speed, callback) {
        truman.sendMessage(messagePrefix + 'setPlaySpeed', [speed], callback);
    },
    uploadRecFile: function (id, url,cookie,callback) {
        truman.sendMessage(messagePrefix + 'uploadRecFile', [id,url,cookie], callback);
    }
};