'use strict';

var messagePrefix = 'live.';

module.exports = {
    enterRoom: function (ticket, callback) {
        var ticketInList = [
            parseInt(ticket.id),
            parseInt(ticket.type),
            parseInt(ticket.userId),
            parseInt(ticket.userType),
            ticket.host.toString(),
            parseInt(ticket.tcpPort),
            parseInt(ticket.rtpPort),
            parseInt(ticket.rtcpPort),
            ticket.signature.toString(),
            ticket.cookie.toString(),
            JSON.stringify(ticket.serverList)
        ];
        truman.sendMessage(messagePrefix + 'enterRoom', ticketInList, callback);
    },
    syncStroke: function (points,width,color, callback) {
        truman.sendMessage(messagePrefix + 'syncStroke',[points,width,color],callback);
    },
    eraseStroke: function (callback) {
        truman.sendMessage(messagePrefix + 'eraseStroke', [], callback);
    },
    publishKeynote: function (keynoteId, totalPageNum, currentPageIndex, callback) {
        truman.sendMessage(messagePrefix + 'publishKeynote', [keynoteId, totalPageNum, currentPageIndex], callback);
    },
    startClass: function (callback) {
        truman.sendMessage(messagePrefix + 'startClass', [], callback);
    },
    endClass: function (callback) {
        console.log('live endClass!');
        truman.sendMessage(messagePrefix + 'endClass', [], callback);
    },
    quitRoom: function (callback) {
        truman.sendMessage(messagePrefix + 'quitRoom', [], callback);
    },
    pageUp: function (callback) {
        truman.sendMessage(messagePrefix + 'pageUp', [], callback);
    },
    pageTo: function (currentPageIndex, callback) {
        truman.sendMessage(messagePrefix + 'pageTo', [currentPageIndex], callback);
    },
    pageDown: function (callback) {
        truman.sendMessage(messagePrefix + 'pageDown', [], callback);
    },
    sendChatMessage: function (clientMessageId, content, type, callback) {
        truman.sendMessage(messagePrefix + 'sendChatMessage', [clientMessageId, content, type], callback);
    },
    startSend: function (callback) {
        truman.sendMessage(messagePrefix + 'startSend', [], callback);
    },
    applyMicrophone: function (callback) {
        truman.sendMessage(messagePrefix + 'applyMicrophone', [], callback);
    },
    approveMicrophone: function (userId, callback) {
        truman.sendMessage(messagePrefix + 'approveMicrophone', [userId], callback);
    },
    revokeMicrophone: function (userId, callback) {
        truman.sendMessage(messagePrefix + 'revokeMicrophone', [userId], callback);
    },
    openMicrophoneQueue: function (callback) {
        truman.sendMessage(messagePrefix + 'openMicrophoneQueue', [], callback);
    },
    closeMicrophoneQueue: function (callback) {
        truman.sendMessage(messagePrefix + 'closeMicrophoneQueue', [], callback);
    },
    openVideoMic: function (callback) {
        truman.sendMessage(messagePrefix + 'openVideoMic', [], callback);
    },
    closeVideoMic: function (callback) {
        truman.sendMessage(messagePrefix + 'closeVideoMic', [], callback);
    },
    cancelAllMicrophone: function (callback) {
        truman.sendMessage(messagePrefix + 'cancelAllMicrophone', [], callback);
    },
    setTimerMicrophone: function (micId,time,callback) {
        truman.sendMessage(messagePrefix + 'settingMicrophone', [micId,time], callback);
    },
    /*
        channelIndex: teacher 0 student 1
    */
    getOutputLevel: function (channelIndex, callback) {
        truman.sendMessage(messagePrefix + 'getOutputLevel', [channelIndex], callback);
    },
    assistantOnBoard: function(callback) {
        truman.sendMessage(messagePrefix + 'assistantOnBoard', [], callback);
    },
    assistantOffBoard: function(callback) {
        truman.sendMessage(messagePrefix + 'assistantOffBoard', [], callback);
    },
    banUser: function(userId, callback) {
        truman.sendMessage(messagePrefix + 'banUser', [userId], callback);
    },
    unBanUser: function(userId, callback) {
        truman.sendMessage(messagePrefix + 'unBanUser', [userId], callback);
    },
    banAllUser: function(callback) {
      truman.sendMessage(messagePrefix + 'banAllUser', [], callback);
    },
    unBanAllUser: function(callback) {
      truman.sendMessage(messagePrefix + 'unBanAllUser', [], callback);
    },
    cancelTopMessage: function(callback) {
        truman.sendMessage(messagePrefix + 'cancelTopMessage', [], callback);
    },
    setNickname: function(name,callback) {
        truman.sendMessage(messagePrefix + 'setNickname', [name], callback);
    },

    beginSendVideo: function(callback) {
        truman.sendMessage(messagePrefix + 'beginSendVideo', [], callback);
    },

    stopSendVideo: function(callback) {
        truman.sendMessage(messagePrefix + 'stopSendVideo', [], callback);
    },

    switchVideoStyle : function(uid,callback) {
        truman.sendMessage(messagePrefix + 'switchVideoStyle', [uid], callback);
    },

    addQuestion : function(quiz,callback) {
        truman.sendMessage(messagePrefix + 'addQuestion', [quiz], callback);        
    },

    endQuestion : function(questionId,callback) {
        truman.sendMessage(messagePrefix + 'endQuestion', [questionId], callback);        
    },

    removeQuestion : function(questionId,callback) {
        truman.sendMessage(messagePrefix + 'removeQuestion', [questionId], callback);        
    },

    myAnswer : function(answer,callback) {
        truman.sendMessage(messagePrefix + 'myAnswer', [answer], callback);        
    }
};