'use strict';

var messagePrefix = 'versionInfo.';

module.exports = {
    getOSInfo: function (callback) {
        truman.sendMessage(messagePrefix + 'getOSInfo', [], callback);
    }
};