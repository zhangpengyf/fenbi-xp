'use strict';
var $ = require('jquery');

var Sketch = function (
    $scope,
    WebApiService,
    $interval,
    EventListenerService,
    $timeout,
    StrokeBufferService
) {
    var self = this;

    var $keynoteCotent = $('#keynote-content');

    var $stroke = $('#stroke');
    var ctx = $stroke[0].getContext('2d');

    var $tempStroke = $('#temp-stroke');
    var tempStroke = $tempStroke[0];
    var tmpCtx = tempStroke.getContext('2d');

    /**
        use hasEraseCommand and startWebApi together to deal with this problem:
        stroke request -------> eraseCommand ------> response is back
        when response is back, we check eraseCommand has come or not after request
        if has eraseCommand, we don't render the response
    */
    var hasEraseCommand = false; // if eraseCommand is coming after strokes
                                 // quest is send but response has't been
                                 // dealed with, this is set to be true
    var startWebApi = false; //when start web request, this is set true
                             //and set false when response is dealed with
    var penColor = '#ff0000';
    var lineWidth = 3;

    self.strokeCanvas = (function () {
        var resizing = false;
        var resizePageNum = -1;
        var correctCount = 0;
        var oldWidth = 0;
        var oldHeight = 0;
        var initParams = function () {
            resizing = false;
            resizePageNum = -1;
            correctCount = 0;
            oldWidth = 0;
            oldHeight = 0;
        };
        var _resizeStrokeCanvas = function (keynoteWidth, keynoteHeight, successCb) {
            var strokeWidth = keynoteWidth;
            var strokeHeight = keynoteHeight;

            $stroke[0].width = strokeWidth;
            $stroke[0].height = strokeHeight;

            tempStroke.width = strokeWidth;
            tempStroke.height = strokeHeight;

            tmpCtx.lineWidth = lineWidth;
            tmpCtx.lineJoin = 'round';
            tmpCtx.lineCap = 'round';
            tmpCtx.strokeStyle = penColor;
            tmpCtx.fillStyle = penColor;

            successCb();
        };
        var resize = function (pageNum, successCb) {
            console.log('resize ' + pageNum + ' key '  + $scope.keynote.currentPageIndex);

            if (!$scope.keynote ||
                pageNum !== $scope.keynote.currentPageIndex) {
                endResize();
                return ;
            }

            if (!oldWidth ||
                !oldHeight ||
                oldWidth !== $keynoteCotent[0].width ||
                oldHeight !== $keynoteCotent[0].height
            ) {
                correctCount = 0;
                self.eraseStrokeCanvas();
                oldWidth = $keynoteCotent[0].width;
                oldHeight = $keynoteCotent[0].height;

                var _successCb = function () {
                    if (successCb) {
                        successCb();
                    } else if (self.onKeynoteRendered) {
                        self.onKeynoteRendered();
                    }
                    endResize();
                };
                _resizeStrokeCanvas(oldWidth, oldHeight, _successCb);
            }
        };
        var endResize = function () {
            initParams();
        };
        var startResize = function (pageNum, successCb) {
            console.log('startResize ' + pageNum + ' ' + $scope.keynote.currentPageIndex);
            pageNum = pageNum ? pageNum : $scope.keynote.currentPageIndex;
            if (resizing &&
                resizePageNum === $scope.keynote.currentPageIndex) {
                return;
            }
            initParams();
            resizing = true;
            resizePageNum = pageNum;
            resize(pageNum, successCb);
        };
        return {
            resize: startResize
        };
    })();

    EventListenerService.subscribe('room.keynote.rendered.success', function (pageNum) {
        hasEraseCommand = false;
        startWebApi = false;

        self.strokeCanvas.resize(pageNum);
    });

    EventListenerService.subscribe('room.stroke.render', function () {
        EventListenerService.trigger('room.keynote.rendered.success');
    });

    var getPointFromPercentList = function (pointPercentList) {
        return {
            x: pointPercentList[0] * $stroke.width(),
            y: pointPercentList[1] * $stroke.height()
        };
    };

    this.getPointArrayFromPercentListArray = function (percentListArray) {
        var pointArray = [];
        for (var i = percentListArray.length - 1; i >= 0; i--) {
            var point = getPointFromPercentList(percentListArray[i]);
            pointArray.push(point);
        }
        return pointArray;
    };

    this.curveLine = function(points) {
        tmpCtx.beginPath();
        tmpCtx.moveTo(points[0].x, points[0].y);
        for (var i = 0; i < points.length - 1; i ++)
        {
            var xc = (points[i].x + points[i + 1].x) / 2;
            var yc = (points[i].y + points[i + 1].y) / 2;
            tmpCtx.quadraticCurveTo(points[i].x, points[i].y, xc, yc);
        }
        // curve through the last two points
        tmpCtx.lineTo(points[i-1].x, points[i-1].y);

        tmpCtx.stroke();
    };

    this.doCurveThroughPoints = function(points) {
        // Tmp canvas is always cleared up before drawing.
        tmpCtx.clearRect(0, 0, tempStroke.width, tempStroke.height);
        self.curveLine(points);
    };

    this.curveThroughPoints = function (points) {
        var pointNum = points.length;
        if (pointNum < 3) {
            return;
        }
        if(arguments.length >= 3) {
            this.setStokeStyle(arguments[1], arguments[2]);
        }
        this.doCurveThroughPoints(points,pointNum);
    };

    this.getCanvasWidth = function () {
        return $stroke[0].width;
    };
    this.getCanvasHeight = function () {
        return $stroke[0].height;
    };

    this.getMousePosition = function(event) {
        var rect = $stroke[0].getBoundingClientRect();
        return {
            x: event.clientX - rect.left,
            y: event.clientY - rect.top
        };
    };

    this.setStokeStyle = function(width,color)
    {
        if(width === 0) {
            width = 3;
        }

        var colorStr = color.toString();
        if(colorStr.indexOf('#') !== 0) {
            colorStr = '#' + colorStr;
        }

        if(lineWidth != width) {
            lineWidth = width;
            tmpCtx.lineWidth = lineWidth;
        }

        if(penColor != colorStr) {
            penColor = colorStr;
            tmpCtx.strokeStyle = penColor;
            tmpCtx.fillStyle = penColor;
        }
    };

    this.getLineWith = function() {
        return parseInt(tmpCtx.lineWidth);
    };

    this.getLineColor = function() {
        var color = tmpCtx.strokeStyle;
        var colorValue = '0x' + color.substring(1);
        return parseInt(colorValue);
    };

    this.dumpTempContext  = function () {
        if (tempStroke.width === 0 || tempStroke.height === 0) {
            return;
        }
        // Writing down to real canvas now
        ctx.drawImage(tempStroke, 0, 0);
        // Clearing tmp canvas
        //tmpCtx.clearRect(0, 0, tempStroke.width, tempStroke.height);
    };

    this.eraseStrokeCanvas = function () {
        ctx.clearRect(0, 0, $stroke[0].width, $stroke[0].height);
        tmpCtx.clearRect(0, 0, tempStroke.width, tempStroke.height);
    };

    this.renderStrokes = function(pointsObjArrs){
        if(!pointsObjArrs || pointsObjArrs.length <= 0 ) {
            return;
        }

        tmpCtx.clearRect(0, 0, tempStroke.width, tempStroke.height);

        pointsObjArrs.forEach(function (pointsObj) {
            if(pointsObj.points.length > 3) {
                self.setStokeStyle(pointsObj.lineWidth,pointsObj.lineColor);
                self.curveLine(self.getPointArrayFromPercentListArray(pointsObj.points));
            }
        });

        self.dumpTempContext();
    };

    var dec2hex = function(decValue){
        var padding = 6;
        var hex = Number(decValue).toString(16);
        while (hex.length < padding) {
            hex = '0' + hex;
        }
        return hex;
    };   

    this.getPointsArrayFromRes = function (data) {
        var pointsArray = [];
        for (var i = 0; i < data.length; i++) {
            var stroke = data[i];
            var content = $.parseJSON(stroke.content);
            var points = content.points;

            var width = 3 ;
            if(content.hasOwnProperty('lineWidth')) {
                width = content.lineWidth;
                if(width === 0) {  width = 3;  }
            }

            var color = 'ff0000';
            if(content.hasOwnProperty('lineColor')) {
                color = dec2hex(content.lineColor);
            }

            var pointArray = [];
            for (var j = 0; j < points.length; j++) {
                var point = points[j];
                pointArray.push([point.x, point.y]);
            }

            pointsArray.push({
                            lineWidth : width,
                            lineColor : color,
                            points : pointArray
                            });
        }
        return pointsArray;
    };

   this.fetchStrokes = function(episodeId,pageNum)  {               

        WebApiService.get({
            path: 'stroke/dataurl',
            server: 'live'            
        }, {
            params: {
                episodeId: episodeId,
                pageNum: pageNum                
            }
        }).success(function (data) {
            if(data.code == 1 && data.data.url) {
                WebApiService.get({
                            path: data.data.url,
                            server: 'external'                
                }).success(function(stroke){
                    if(stroke.code == 1 && stroke.datas.length > 0) {
                        var pointsArray = self.getPointsArrayFromRes(stroke.datas);
                        pointsArray.forEach(function(points) {
                            StrokeBufferService.add(
                                $scope.roomId,
                                $scope.keynote.keynoteId,
                                pageNum,
                                points.points,
                                points.lineWidth,
                                points.lineColor
                            );                            
                        });
                        self.renderStrokes(pointsArray);                       
                    }                   
                    
                }).error(function(res, status) {
                    console.log('get storke failed ' + data + ' ' + status);
                });                
            }            
        }).error(function (data, status) {
            console.log('get storke url failed ' + data + ' ' + status);
        });
    };

    EventListenerService.subscribe('room.stroke.onSyncStroke', function (pageNum, points,lineWidth,lineColor) {       

        StrokeBufferService.add(
            $scope.roomId,
            $scope.keynote.keynoteId,
            pageNum,
            points,
            lineWidth,
            lineColor
        );  
        
        var strokeColor = '#' + lineColor;
        self.curveThroughPoints(self.getPointArrayFromPercentListArray(points),lineWidth,strokeColor);
        self.dumpTempContext();        
    });
    
    EventListenerService.subscribe('replay.stroke.clearCurrentBuffer', function () {
        StrokeBufferService.clearAll();
        self.eraseStrokeCanvas();
    });     

    EventListenerService.subscribe('room.live.fetch.stroke', function (pageNum) {
        self.fetchStrokes($scope.roomId,pageNum);
    });

    EventListenerService.subscribe('room.stroke.onEraseStroke', function (pageNum) {
        if (pageNum < 0) {
            return;
        }
        if (startWebApi) {
            hasEraseCommand = true;
        }

        if(pageNum == $scope.keynote.currentPageIndex) {
            self.eraseStrokeCanvas();
        }        

        StrokeBufferService.remove(
            $scope.roomId,
            $scope.keynote.keynoteId,
            pageNum
        );
    });

    $scope.$on('$destroy', function () {
        StrokeBufferService.clearAll();
    });
};
module.exports = Sketch;