'use strict';
var $ = require('jquery');

var Chat = function (
    $scope,
    $interval,
    WebApiService,
    ChatMessageService,
    DialogService,
    $compile,
    UserInfoService,
    EventListenerService
) {
    var needScroll = true;
    var scrollBottom = false;
    var showHasNewMessage = true;
    var $messageList = $('.message-list');

    $scope.latestTopMessage = {};
    $scope.chatMessageList = [];
    $scope.pendingMessageList = [];
    $scope.messageMaxLength = 140;
    if($scope.isTeacher || $scope.isAssistant) {
        $scope.messageMaxLength = 500;
    }
    $scope.hasMoreMessage = false;
    $scope.loadingMessage = false;
    $scope.draft = '';
    $scope.banned = false;
    $scope.pinned = false;
    $scope.messageDisplayLimit = 150;
    $scope.messageFetchLimit = 10;
    $scope.hasNewMessage = false;

    var chatMessageSelected = function () {
        var selection = document.getSelection();
        if (selection.toString().length > 0 &&　$(selection.anchorNode).closest('.message-list').length > 0) {
            return true;
        } else {
            return false;
        }
    };

    var renderMessagePromise = $interval(function () {
        if (!$scope.pendingMessageList || $scope.pendingMessageList.length === 0) {
            return;
        }

        if ($messageList[0].scrollHeight - $messageList[0].clientHeight <= $messageList[0].scrollTop &&
            !chatMessageSelected()) {
            needScroll = true;
        } else {
            needScroll = false;
        }

        var isScrollBottom = needScroll || scrollBottom;

        $scope.chatMessageList = $scope.chatMessageList.concat($scope.pendingMessageList);
        $scope.pendingMessageList.forEach(function (message) {
            if(!message.nickname) {
                UserInfoService.getPool('chat').add(message.userId);
            }
        });
        $scope.pendingMessageList = [];
        if (isScrollBottom &&　$scope.chatMessageList.length > $scope.messageDisplayLimit) {
            $scope.chatMessageList.splice(0, $scope.chatMessageList.length - $scope.messageDisplayLimit);
            $scope.hasMoreMessage = true;
        }
        if (isScrollBottom) {
            scrollBottom = false;
            $scope.scrollToBottom();
        } else if (showHasNewMessage) {
            $scope.hasNewMessage = true;
        }
        showHasNewMessage = true;
        $scope.safeApply();
    }, 500);

    var renderLatestTopMessage = function () {
        var $topMessage = $('.top-message');
        if (!$topMessage.length) {
            setTimeout(renderLatestTopMessage, 30);
            return;
        }

        if ($scope.latestTopMessageId && $scope.latestTopMessageId !== '0' && $scope.latestTopMessage !== undefined) {
            UserInfoService.getPool('chat').add($scope.latestTopMessage.userId);
            var chatMessge = '<chat-message message="{{latestTopMessage.content}}"></chat-message>';
            if ($scope.latestTopMessage && ($scope.latestTopMessage.id || $scope.latestTopMessage.messageId)) {
                $topMessage.children('.latest-top-message-content').html('').append($compile(chatMessge)($scope));
                EventListenerService.trigger('episode.chat.messageList.keepBottom');
                window.setTimeout(function () {
                    EventListenerService.trigger('episode.chat.messageList.keepBottom');
                    var height = $topMessage.outerHeight(true);
                    $('.top-message').css({'opacity' : 1});
                    $('.message-list-wrapper').animate(
                        {
                            'paddingTop': height + 1
                        },
                        300
                    );
                }, 100);
            }
        } else {
            $topMessage.children('.latest-top-message-content').html('');
            $('.top-message').css({'opacity' : 0});
            $('.message-list-wrapper').animate({'paddingTop': 0}, 300);
        }
    };

    $scope.$watch('latestTopMessageId', function () {
        renderLatestTopMessage();
    });

    $scope.scrollToTop = function () {
        setTimeout(function () {
            $messageList.scrollTop(0);
        }, 0);
    };

    $scope.scrollToBottom = function () {
        setTimeout(function () {
            $messageList.scrollTop($messageList[0].scrollHeight);
            $scope.hasNewMessage = false;
        }, 0);
    };

    $scope.onChatMessage = function (roomId, userId, messageId, clientMessageId, content, timeStamp, type,nickname) {
        var chatMessage = ChatMessageService.createServerMessage(roomId, userId, messageId,
              clientMessageId, content, timeStamp, type,nickname);
        if (type === 1) {
            $scope.latestTopMessageId = messageId;
            $scope.latestTopMessage = chatMessage;
        }

        if(nickname) {
            UserInfoService.set(userId,nickname);
        }

        $scope.pendingMessageList.push(chatMessage);

        if($scope.pendingMessageList.length > 10) {
            scrollBottom = true;
        }

        if (userId === $scope.userId && !type) {
            scrollBottom = true;
        }
        if (type) {
            showHasNewMessage = false;
        }
    };

    $scope.onTopMessageCanceled = function () {
        $scope.latestTopMessageId = '';
        $scope.latestTopMessage = {};
    };

    $scope.onChatMessageFailed = function (roomId, userId, clientMessageId) {
        console.log('message send failed' + roomId + userId + clientMessageId);
    };

    $scope.$on('$destroy', function () {
        $interval.cancel(renderMessagePromise);
    });

};
module.exports = Chat;
