'use strict';
var fileApi = require('../../engine/fileApi.js');
var moment = require('moment');
var $scope = null;
var DialogService = null;
var $timeout = null;
var isStopListenError = null;
var Room = function (
    _$scope,
    WebApiService,
    _DialogService,
    $window,
    CourseMapService,
    _$timeout,
    EventListenerService,
    UserInfoService,
    $location
) {
    var self = this;
    $scope = _$scope;

    var params = $location.search();
    $scope.courseId = parseInt(params.courseId);
    $scope.episodeId = parseInt(params.episodeId);
    $scope.lastUserCount = 0;

    isStopListenError = false;
    DialogService = _DialogService;
    $timeout = _$timeout;
    UserInfoService.initPool('chat', 0, 3);
    UserInfoService.initPool('preFetch', 10, 30, true);
    var fetchStudentsHandle,
        fetchAssistantsHandle;
    if (!$scope.episode) {
        WebApiService.get({
            path: 'episodes/info/' + $scope.episodeId,   //path: 'episodes/' + $scope.episodeId,
            courseId: $scope.courseId
        }, {
            params: {
                userType: $scope.trumanUserType
            }
        }).success(function (data) {
            $scope.episode = data;

            fileApi.getCookie('sess', function (ret, value) {
                if (ret === 1) {
                    $scope.sessionCookie = value;
                    
                    console.log('episode ' + JSON.stringify(data));

                    if($scope.episode.ticket.type == 1) {
                        WebApiService.get({
                            path: 'config/server/list',
                            courseId: $scope.courseId
                        }, {
                            params: {
                                episodeId: $scope.episodeId,
                                userType: $scope.trumanUserType
                            }
                        }).success(function (data) {
                            $scope.episode.ticket.serverList = data;
                            $scope.episode.ticket.host = '';
                            $scope.episode.ticket.tcpPort = 0;
                            $scope.episode.ticket.rtpPort = 0;
                            $scope.episode.ticket.rtcpPort = 0;

                            self.onDataLoaded();                            

                            if (!$scope.allAssistantList) {
                                WebApiService.get({
                                    path: CourseMapService.getCourseUrl($scope.episode.courseId) + '/episodes/' + $scope.episode.id + '/assistants'
                                }).success(function (data) {
                                    $scope.allAssistantList = data;
                                });
                            }                    
                            }).error(function(data,status){
                                if (status !== 0) {
                                    DialogService.alert('服务器信息加载失败', $scope.backToList);
                                } else {
                                    DialogService.alert('进入教室过程中，服务器信息加载失败', $scope.backToList, '退出教室');
                                }
                            });
                        } 
                        else{
                            self.onDataLoaded();
                        }
                    }
                    else{
                        DialogService.alert('Cookie信息加载失败，清注销重新登录', $scope.backToList);                                        
                    }
                }.bind(self));
        }).error(function (data, status) {
             if (status !== 0) {
                DialogService.alert('课时信息加载失败', $scope.backToList);
            } else {
                DialogService.alert('进入教室过程中，网络连接出现问题', $scope.backToList, '退出教室');
            }
        });
    }
   
    $window.addEventListener('truman.onError', this.trumanErrorHandler.bind(this));
    this.enterTimeoutPromise = null;
    this.updateUserListTimerPromise = null;
    this.stopListenError = function () {
        console.log('stop listen error');
        isStopListenError = true;
        $window.removeEventListener('truman.onError');
    };
    this.tryEnterRoom = function (errorMessage) {
        this.enterRoom(errorMessage);
    };
    this.onRoomInfo = function () {
        var _arguments = arguments[0];
        var index = 0;
        var roomInfo  = {
            'teacherList': _arguments[index++],
            'studentList': _arguments[index++],
            'isMicrophoneQueueOpen': _arguments[index++],
            'microphoneList': _arguments[index++],
            'speakingUserList': _arguments[index++],
            'userCount': _arguments[index++],
            'startTime': _arguments[index++],
            'latestMessageId': _arguments[index++],
            'latestStrokeId': _arguments[index++],
            'isAssistantOnBoard': _arguments[index++],
            'assistantList': _arguments[index++],
            'assistantUserId': _arguments[index++],
            'latestTopMessageId': _arguments[index++],
            'bannedUserList': _arguments[index++],
            'micStatus': _arguments[index++],
            'isVideoMicOpen': _arguments[index++],
            'largeUid': _arguments[index++],
            'questionInfo': _arguments[index],
        };

        console.log('onRoomInfo: ' + $scope.lastUserCount + ' mic ' + roomInfo.micStatus + 'isVideoMicOpen ' + roomInfo.isVideoMicOpen + ' largeUid ' + roomInfo.largeUid);

        $scope.roomInfo = roomInfo; //       

        $scope.isVideoMicOpen = roomInfo.isVideoMicOpen;
        $scope.teacherList = [];
        $scope.assistantList = [];
        $scope.largeUid = roomInfo.largeUid;

        $scope.switchVideoState = function(status) {
            $scope.isVideoMicOpen = status;
        };

        for(var i = 0; i < roomInfo.teacherList.length; i++) {
            $scope.teacherList.push({'userId': roomInfo.teacherList[i],'channelId':-1});
        }

        for(i = 0; i < roomInfo.assistantList.length; i++) {
            $scope.assistantList.push({'userId': roomInfo.assistantList[i],'channelId':-1});
        }

        $scope.studentList = roomInfo.studentList;  //{id,name,right,type}
        $scope.micStatus = roomInfo.micStatus;

        if ($scope.isTeacher) {
            fetchStudentsHandle = UserInfoService.fetchAllStudent($scope.studentList);
            fetchAssistantsHandle = UserInfoService.fetchAllUsers($scope.assistantList);
        }

        UserInfoService.getPool('preFetch').addArrayObj($scope.studentList);
        UserInfoService.getPool('preFetch').addArray($scope.assistantList);

        $scope.$broadcast('RoomInfo', roomInfo);
    };

    $scope.setIsTeacherVideoOpen = function(status) {
        $scope.isTeacherVideoOpen = status;
        console.log($scope.isTeacherVideoOpen);
    };

    $scope.getUserName = function(userObj) {
        if(userObj.name && userObj.name.length > 0) {
            return userObj.name;
        }
        return $scope.getUser(userObj.id).name;
    };

    $scope.getUser = function (userId) {
        if ($scope.episode.teacher.userId === userId) {
            return {
                id: userId,
                name: $scope.episode.teacher.name
            };
        } else if ($scope.user.id === userId) {
            return {
                id: userId,
                name: $scope.user.name
            };
        }
        var user = UserInfoService.get(userId);
        return user;
    };
    $scope.$on('$destroy', function () {
        UserInfoService.setAllOld();
        if (fetchStudentsHandle) {
            fetchStudentsHandle.cancel();
        }
        if (fetchAssistantsHandle) {
            fetchAssistantsHandle.cancel();
        }
        UserInfoService.destroyPool('chat');
        UserInfoService.destroyPool('preFetch');
        EventListenerService.cancel('room');
        self.stopListenError();
    }.bind(this));
};

Room.prototype.baseEnterRoom = function (episode) {
    $scope.dialog.entering = true;
    var ticket = episode.ticket;
    $scope.teacherName = episode.teacher.name;
    $scope.userId = parseInt(ticket.userId);
    $scope.roomId = parseInt(ticket.id);
    $scope.courseId = episode.courseId;
    $scope.episodeTitle = episode.title;
    var startTime = $scope.episode.startTime;
    var endTime = $scope.episode.endTime;
    $scope.timeRangeStr = moment.unix(startTime / 1000).format('YY年M月D日 HH:mm') +
            ' - ' + moment.unix(endTime / 1000).format('HH:mm');
};
Room.prototype.clearEnterTimeout = function () {
    if (this.enterTimeoutPromise) {
        $timeout.cancel(this.enterTimeoutPromise);
    }
};
Room.prototype.enterTimeout = function (errorMessage) {
    this.clearEnterTimeout();
    this.enterTimeoutPromise = $timeout(function () {
        if ($scope.dialog.entering) {
            $scope.dialog.entering = false;
            setTimeout(function () {
                DialogService.alert(errorMessage, function () {
                    $scope.backToList(true);
                }, '退出教室');
            }, 0);
        }
    }, 20000);
};

Room.prototype.clearUpdateUserListTimer = function () {
    if (this.updateUserListTimerPromise) {
        window.clearInterval(this.updateUserListTimerPromise);
    }
};

Room.prototype.setUpdateUserListTimer = function() {
    this.clearUpdateUserListTimer();
    this.updateUserListTimerPromise = window.setInterval(function () {
        //console.log('onUpdateUserList:' + $scope.lastUserCount + ' users ' + $scope.studentList.length);

        if($scope.studentList && $scope.lastUserCount != $scope.studentList.length)
        {
            $scope.lastUserCount = $scope.studentList.length;

            var displayLen = Math.min($scope.studentList.length,50);
            $scope.studentListUi = $scope.studentList.slice(0,displayLen);  //TODO fix display user list
        }
    },   1000 );
};

Room.prototype.trumanErrorHandler = function (event) {
    if (isStopListenError) {
        return;
    }
    var errorCode = parseInt(event.detail);
    var kickout = function () {
        $scope.backToList(true);  // force quit
        $scope.safeApply();
    };
    if (errorCode === 400) {
        $scope.dialog.entering = false;
        DialogService.alert('网络连接有点问题，无法连接服务器', kickout, '退出教室');
        this.stopListenError();
    } else if (errorCode === 401) {
        DialogService.alert('无法进入教室，请稍后再试', kickout, '退出教室');
        this.stopListenError();
    } else if (errorCode === 402) {
        $scope.dialog.entering = true;
        $scope.dialog.reconnecting = true;
    } else if (errorCode === 403) {
        DialogService.alert('无法进入教室，请稍后再试', kickout, '退出教室');
        this.stopListenError();
    } else if (errorCode === 404) {
        DialogService.alert('直播课不存在，你将退出教室', kickout, '退出教室');
        this.stopListenError();
    } else if (errorCode === 405) {
        DialogService.alert('无法进入教室，请稍后再试', kickout, '退出教室');
        this.stopListenError();
    } else if (errorCode === 409) {
        DialogService.alert('你已在其他客户端登录，请退出后重新登录', kickout, '退出教室');
        this.stopListenError();
    } else if (errorCode === 505) {
        DialogService.alert('网络连接中断，请退出重试', kickout, '退出教室');
        this.stopListenError();
    }
};

Room.prototype.onDataLoaded = function () {
    throw new Error('onDataLoaded in Room Class is virtual function');
};

module.exports = Room;