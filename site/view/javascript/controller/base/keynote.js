'use strict';
var $ = require('jquery');
var _ = require('underscore');
var $scope = null;
var KeynoteService = null;

var Keynote = function (
    _$scope,
    _KeynoteService,
    $interval,
    $window,
    KeynoteManageService,
    WebApiService,
    EventListenerService,
    $timeout
) {
    $scope = _$scope;
    $scope.videoNum = 0;
    $scope.currentVolume = 0;
    $scope.largeVideoNickname = '';    

    $scope.answer = null;
    $scope.closedQuestionId = -1;    
    $scope.questionMask = false;
    $scope.questionTpl = {};   

    KeynoteService = _KeynoteService;
    var self = this;
    this.forceUpdatePnView = function (delay) {
        setTimeout(function () {
            if ($('.current-page-number') && $('.current-page-number').val() != $scope.pageNumber) {
                $('.current-page-number').val($scope.pageNumber);
            }
        }, delay === 0? 0 : (delay || 500));
    };
    var downloadHandle = null;
    var downloadAborted = null;
    var resetKeynoteLoadStatus = function () {
        $scope.keynoteLoaded = false;
        $scope.keynoteLoadFail = false;
    };
    var downloadKeynote = function () {
        resetKeynoteLoadStatus();
        self.loadKeynoteId = $scope.keynote.keynoteId;

        WebApiService.get({
            path: 'v3/keynotes/' + $scope.keynote.keynoteId + '/path',
            courseId: $scope.courseId
        }).success(function (data) {
            console.log('start download keynote ' + data.code);
            if (data.code == 1) {                
                if (data.data.urls && data.data.urls instanceof Array) {
                    var progressCb = function (evt) {
                        var percent = parseInt(100.0 * evt.loaded / evt.total);
                        if (percent >= 100) {
                            self.loadKeynoteId = null;
                        }
                        $scope.downloadStatusString = percent + '%';
                    };
                    return WebApiService.getUrls(data.data.urls.map(function(url){
                        return {
                            path: url,
                            server:'external'
                        };
                    }), {
                        responseType: 'arraybuffer',
                        __from: 'room',
                        transformResponse: function (data) {
                            return {data: data};
                        }
                    }, progressCb).then(function(data){
                        $timeout(function () {
                            $scope.downloadStatusString = '';
                            KeynoteManageService.load(
                                data.data,
                                function () {
                                    $scope.renderKeynote();
                                }
                            );
                            EventListenerService.trigger('room.keynote.loaded');
                            $scope.keynoteLoaded = true;
                            $('.keynote-content-wrapper').css(
                                {
                                    background : '#DBDBDB'
                                }
                            );
                        }, 50);
                    },function () {
                        $scope.downloadStatusString = '';
                        self.loadKeynoteId = null;
                        if (downloadAborted) {
                            downloadAborted = false;
                            return;
                        }
                        $scope.keynoteLoadFail = true;
                        console.log('download keynote error');
                    });
                } else {
                    return WebApiService.get({
                        path: data.data.url,
                        server:'external'
                    }, {
                        responseType: 'arraybuffer',
                        __from: 'room',
                        transformResponse: function (data) {
                            return {data: data};
                        }
                    }).progress(function (evt) {
                        var percent = parseInt(100.0 * evt.loaded / evt.total);
                        if (percent >= 100) {
                            self.loadKeynoteId = null;
                            $timeout(function () {
                                $scope.downloadStatusString = '';
                                KeynoteManageService.load(
                                    evt.currentTarget.response,
                                    function () {
                                        $scope.renderKeynote();
                                    }
                                );
                                EventListenerService.trigger('room.keynote.loaded');
                                $scope.keynoteLoaded = true;
                                $('.keynote-content-wrapper').css(
                                    {
                                        background : '#DBDBDB'
                                    }
                                );
                            }, 50);
                        }
                        $scope.downloadStatusString = percent + '%';
                    }).error(function () {
                        $scope.downloadStatusString = '';
                        self.loadKeynoteId = null;
                        if (downloadAborted) {
                            downloadAborted = false;
                            return;
                        }
                        $scope.keynoteLoadFail = true;
                        console.log('download keynote error');
                    });
                }
            }
        });
    };

    $scope.loadKeynote = function () {
        if (downloadHandle) {
            downloadHandle.abort();
            downloadAborted = true;
            downloadHandle = null;
        }
        downloadHandle = downloadKeynote();
    };
    self.abortKeynote = function () {
        if (downloadHandle) {
            downloadHandle.abort();
            downloadAborted = true;
            downloadHandle = null;
        }
        resetKeynoteLoadStatus();
        KeynoteManageService.clearPage($('#keynote-content')[0]);
        KeynoteManageService.clearPdfCache();
    };
    $scope.renderKeynote = function () {
        $scope.keynoteRenderFail = false;
        KeynoteManageService.render(
            $scope.pageNumber,
            $('.keynote-content-wrapper'),
            function (pageNum) {
                EventListenerService.trigger('room.keynote.rendered', pageNum - 1);
            },
            function () {
                $scope.keynoteRenderFail = true;
            }
        );
    };

    EventListenerService.subscribe('room.keynote.rendered', function (pageNum) {
        var $keynoteContent = $('#keynote-content');
        if ($keynoteContent.width() - $keynoteContent[0].width >=5 ||
            $keynoteContent.height() - $keynoteContent[0].height >= 5) {
            $scope.renderKeynote();
        } else {
            EventListenerService.trigger('room.keynote.rendered.success', pageNum);
        }        

        if($scope.roomInfo.questionInfo && $scope.roomInfo.questionInfo.status !== 3) {
            if($scope.isStudent && $scope.answer === null) {
                if($scope.roomInfo.questionInfo.status == 2) {  //end question
                    $scope.showQuestionPanel(3);
                }
                else{
                    $timeout(function () {
                      if($scope.answer === null) {
                            $scope.showQuestionPanel(2);
                      }                    
                    }, 200);
                }                                  
            }
            else{
                $scope.showQuestionPanel(3);
            }            
        } 
    });

    $scope.$watch('keynote', function() {
        if (!$scope.keynote) {
            return;
        }
        if (($scope.pageNumber - 1) !== $scope.keynote.currentPageIndex) {
           $scope.pageNumber = $scope.keynote.currentPageIndex + 1;
           $scope.safeApply();
           self.forceUpdatePnView();
        }
        $scope.renderKeynote();
    }, true);   

    $(window).on('resize', _.debounce(
        function () {
            if ($scope.keynote) {
                $scope.renderKeynote();
            }
        },
        60
    ));

    $scope.$on('$destroy', function () {
        self.abortKeynote();
    });   

    EventListenerService.subscribe('room.video.status', function (largeUid, videoNum, microphoneList, teacherList) {
        $scope.microphoneList = microphoneList;
        $scope.teacherList = teacherList;

        $scope.largeUid = largeUid;
        $scope.videoNum = videoNum;
        if (teacherList.length > 0 && teacherList[0].userId === largeUid) {
            $scope.largeVideoNickname = teacherList[0].nickName;
        } else if (microphoneList.length > 0 && microphoneList[0].userId === largeUid) {
            $scope.largeVideoNickname = microphoneList[0].nickName;
        }
        if(teacherList && teacherList.length > 0) {
            console.log('room.video.status' + teacherList[0].userId);
        }        
    });

    EventListenerService.subscribe('room.volume.level', function (level, userId) {
        if($scope.largeUid == userId) {
            $scope.currentVolume = level;
        }
    });

    EventListenerService.subscribe('room.video.countdown', function (min1, min0, sec1, sec0) {
        if (!$scope.teacherList[0] || $scope.teacherList[0] && $scope.largeUid !== $scope.teacherList[0].userId) {
            $scope.countdown = {
                min1: min1,
                min0: min0,
                sec1: sec1,
                sec0: sec0
            };
        } else {
            $scope.countdown = {};
        }
    });

    $scope.removeAnswer = function(){
         $scope.closedQuestionId = $scope.answer.questionId;
    };

    $scope.setAnswer = function(ans) {
        $scope.answer = ans;
    };

    $scope.hideQuestionPanel = function() {
        $scope.questionMask = false;
    };  

    $scope.showQuestionPanel = function(type) {
        if(type == 1) {
            $scope.questionMask = true;
            $scope.questionTpl['contentUrl'] = 'component/quizTeacher.html';    
        }
        else if(type == 2) {
            if($scope.episode.ticket.type === 1) {
                $scope.questionMask = true;
                $scope.questionTpl['contentUrl'] = 'component/quizStudent.html';
            }            
        }
        else if(type == 3) {
            if($scope.isTeacher) {
                $scope.questionMask = true;
                $scope.questionTpl['contentUrl'] = 'component/quizTeacherAnswer.html';
            }
            else{                
                var display = false;

                if($scope.episode.ticket.type !== 1) {
                    display = true;
                }
                else {
                    if($scope.answer ){
                        if( $scope.closedQuestionId != $scope.answer.questionId) {
                            display = true;
                        }                        
                    }
                    else if($scope.roomInfo.questionInfo.status == 2) { //no answer but question end
                        display = true;
                    }
                }                

                if(display) {
                    $scope.questionMask = true;
                    $scope.questionTpl['contentUrl'] = 'component/quizStudentAnswer.html';
                }
              
            }
        }        
    };

    EventListenerService.subscribe('room.question.add', function (question) {    
        if($scope.roomInfo.questionInfo === null)    {
            $scope.roomInfo.questionInfo = question;
        }        
        $scope.roomInfo.questionInfo.type = question.type;
        $scope.roomInfo.questionInfo.status = 1;
        $scope.roomInfo.questionInfo.questionId = question.questionId;
        $scope.roomInfo.questionInfo.optionNum = question.optionNum;
        $scope.roomInfo.questionInfo.correctOptions = question.correctOptions;

        $scope.closedQuestionId = -1;

        console.log('add question ' + JSON.stringify(question));

        if($scope.isTeacher) {
            $scope.showQuestionPanel(3);
        }
        else{
            $scope.showQuestionPanel(2);            
        }
    });

    EventListenerService.subscribe('room.question.end', function (questionId) {                
        if(questionId == $scope.roomInfo.questionInfo.questionId) {
            $scope.roomInfo.questionInfo.status = 2;
            $scope.showQuestionPanel(3);

            $scope.$broadcast('question.summary', $scope.roomInfo.questionInfo.answerSummary);
        }        
    });

    EventListenerService.subscribe('room.question.remove', function (questionId) {                
        console.log('remove question ' + questionId);        
        $scope.roomInfo.questionInfo.status = 3;
        $scope.hideQuestionPanel();                
    });

    EventListenerService.subscribe('room.question.summary', function (summary) {           
        if(!$scope.roomInfo.questionInfo.answerSummary)     {
            $scope.roomInfo.questionInfo['answerSummary'] = summary.answerSummary;
        }       
        
        $scope.roomInfo.questionInfo.answerSummary = summary.answerSummary;                
        
        if(!$scope.questionMask || $scope.episode.ticket.type !== 1) {
            $scope.showQuestionPanel(3);                
        }                

        $scope.$broadcast('question.summary', $scope.roomInfo.questionInfo.answerSummary);
    });

    EventListenerService.subscribe('room.question.myanswer', function (answer) {        
        console.log('my ' + JSON.stringify(answer));
        $scope.answer = answer;
        $scope.questionMask = true;         
    });

};

Keynote.prototype.onKeynoteInfo = function (keynoteId, totalPageNum, currentPageIndex) {
    console.log('fetch keynoteInfo, keynoteId is ' + keynoteId + ', totalPageNum is ' + totalPageNum + ', currentPageIndex is ' + currentPageIndex);
};
Keynote.prototype.fullScreen = function () {
    $scope.toggleFullscreen();
};
module.exports = Keynote;