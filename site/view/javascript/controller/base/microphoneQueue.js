'use strict';
var live = require('../../engine/live');
var device = require('../../engine/device');

var $scope = null;
var MicrophoneQueue = function (
    _$scope,
    $interval,
    UserInfoService,
    EventListenerService
) {
    $scope = _$scope;
    //var teacherVolumeTimer = null;
    //var assistantVolumeTimer = null;
    var studentVolumeTimer = null;
    var microphoneTimer = null;

    $scope.microphoneQueueCapacity = 5;
    $scope.bannedUserList = [];

    $scope.teacherVolumeLevel = 0;
    $scope.studentVolumeLevel = 0;

    $scope.assistantUserId = null;
    $scope.microphoneBanned = false;

    $scope.microphoneList = [];

    $scope.microphoneSettingTimer = 0;
    $scope.microphoneClock = 0;

    $scope.microphoneClockMin0 = 0;
    $scope.microphoneClockMin1 = 0;
    $scope.microphoneClockSec0 = 0;
    $scope.microphoneClockSec1 = 0;

    $scope.hasSyncMedia = 0;

    //var context = null;
    $scope.isVideoMicOpen = false;
    $scope.largeUid = 0;

    var self = this;

    this.isSpeaker = function (userId) {
        return (userId == $scope.userId) && $scope.engineType;
    };

    var getVolumeLevel = function (channelIndex,isSpeakUser) {
        if (!isSpeakUser) {
            live.getOutputLevel(channelIndex, function (retCode, level) {
                if (retCode < 0) {
                    //console.log('getOutputLevel failed, channelIndex is '+ channelIndex);
                } else {
                    for(var i = 0; i < $scope.teacherList.length; i++) {
                        if ($scope.teacherList[i].channelId == channelIndex) {
                            $scope.teacherList[i].volumeLevel = level;

                            $scope.teacherVolumeLevel = level;
                            EventListenerService.trigger('room.volume.level', level, $scope.teacherList[i].userId);
                            break;
                        }
                    }

                    for(i = 0; i < $scope.assistantList.length; i++) {
                        if ($scope.assistantList[i].channelId == channelIndex) {
                            $scope.assistantList[i].volumeLevel = level;

                            $scope.assistantVolumeLevel = level;
                            break;
                        }
                    }

                    for(i = 0; i < $scope.microphoneList.length; i++){
                        if($scope.microphoneList[i].channelId == channelIndex) {
                            $scope.microphoneList[i].volumeLevel = level;
                            EventListenerService.trigger('room.volume.level', level, $scope.microphoneList[i].userId);
                            break;
                        }
                    }
                }
            });
        } else {
            device.getInputLevel(false, function (retCode, level) {
                if (retCode < 0) {
                    console.log('getMicroPhoneInputLevel failed');
                } else {
                    for(var i = 0; i < $scope.teacherList.length; i++) {
                        if ($scope.teacherList[i].channelId == channelIndex) {
                            $scope.teacherList[i].volumeLevel = level;
                            EventListenerService.trigger('room.volume.level', level, $scope.teacherList[i].userId);
                            $scope.teacherVolumeLevel = level;
                        }
                    }

                    for(i = 0; i < $scope.assistantList.length; i++) {
                        if ($scope.assistantList[i].channelId == channelIndex) {
                            $scope.assistantList[i].volumeLevel = level;

                            $scope.assistantVolumeLevel = level;
                        }
                    }

                    for(i = 0; i < $scope.microphoneList.length; i++){
                        if($scope.microphoneList[i].channelId == channelIndex) {
                            $scope.microphoneList[i].volumeLevel = level;
                            EventListenerService.trigger('room.volume.level', level, $scope.microphoneList[i].userId);
                            return;
                        }
                    }
                }
            });
        }
    };

    this.starStudentVolumeTimer = function () {
        cleanStudentVolumeTimer();//clean first
        studentVolumeTimer = $interval(function () {
            for(var i = 0; i < $scope.teacherList.length; i++) {
                if($scope.teacherList[i].channelId >= 0) {
                    getVolumeLevel($scope.teacherList[i].channelId,$scope.teacherList[i].isSpeaking);
                }
            }

            for( i = 0; i < $scope.assistantList.length; i++) {
                if($scope.teacherList[i].channelId >= 0) {
                    getVolumeLevel($scope.assistantList[i].channelId, $scope.assistantList[i].isSpeaking);
                }
            }

            for( i = 0; i < $scope.microphoneList.length; i++) {
                getVolumeLevel($scope.microphoneList[i].channelId,$scope.microphoneList[i].isSpeaking);
            }
        }, 100);
    };

    var cleanStudentVolumeTimer = function () {
        if (studentVolumeTimer !== null) {
            $interval.cancel(studentVolumeTimer);
        }
    };

    var starMicrophoneTimer = function (newcycle) {        
        cleanMicrophoneTimer();

        if(newcycle) {
            $scope.microphoneClock = $scope.microphoneSettingTimer;
        }

        console.log('starMicrophoneTimer ' + $scope.microphoneSettingTimer + ' ' + $scope.microphoneClock);

        microphoneTimer = $interval(function () {
            $scope.microphoneClock -= 200;
            if($scope.microphoneClock < 0) {
                $scope.microphoneClock = 0;
            }

            //TODO filter
            var minutes = Math.floor($scope.microphoneClock / (1000 * 60));
            var seconds =Math.floor($scope.microphoneClock / 1000  - minutes * 60);

            $scope.microphoneClockMin1 = Math.floor(minutes % 10);
            $scope.microphoneClockMin0 = Math.floor((minutes / 10) % 10);

            $scope.microphoneClockSec1 = Math.floor(seconds % 10);
            $scope.microphoneClockSec0 = Math.floor((seconds / 10) % 10);

            EventListenerService.trigger('room.video.countdown', $scope.microphoneClockMin1,
            $scope.microphoneClockMin0, $scope.microphoneClockSec1, $scope.microphoneClockSec0);

            //console.log('starMicrophoneTimer ' + $scope.microphoneClockMin0 + $scope.microphoneClockMin1 + ':' + $scope.microphoneClockSec0 + $scope.microphoneClockSec1);
        }, 200);
    };

    this.beginMicrophoneTimer = starMicrophoneTimer;

    var cleanMicrophoneTimer = function () {
        if (microphoneTimer !== null) {
            $interval.cancel(microphoneTimer);
            microphoneTimer = null;
        }
    };


    this.createSpeakerWithSpk = function (userObj, isSpeaking) {
        var student = {};

        student.userId = userObj.userId;
        student.channelId = userObj.channel;
        student.nickName = userObj.nickname;
        student.isAudioOpen = userObj.isAudioOpen;
        student.isVideoOpen = userObj.isVideoOpen;
        student.volumeLevel = 0;
        student.isSpeaking = isSpeaking;

        return student;
    };


    this.createSpeaker = function (userId, isSpeaking,channelId,nickName,hasAudio,hasVideo) {
        var student = {};

        if(typeof channelId === 'undefined') {
            channelId =  -1;
        }

        if(typeof hasAudio === 'undefined') {
            hasAudio =  1;
        }

        if(typeof hasVideo === 'undefined') {
            hasVideo =  0;
        }

        nickName = nickName || userId;

        student.userId = userId;
        student.isSpeaking = isSpeaking;
        student.channelId = channelId;
        student.volumeLevel = 0;
        student.nickName = nickName;
        student.isAudioOpen = hasAudio;
        student.isVideoOpen = hasVideo;

        return student;
    };

    EventListenerService.subscribe('room.user.device.event', function (userId,hasAudio,hasVideo) {
        for(var i = 0; i < $scope.teacherList.length; i++) {
            if($scope.teacherList[i].userId == userId) {
                $scope.setIsTeacherVideoOpen(hasVideo);
            }
        }

        for(i = 0; i < $scope.microphoneList.length; i++) {
            if($scope.microphoneList[i].userId == userId) {
                $scope.microphoneList[i].isVideoOpen = hasVideo;
                if (!hasVideo && $scope.largeUid === userId) {
                    $scope.largeUid = 0;
                }
            }
        }

        self.onVideoStatusChange();        
        console.log('room.user.device.event ' + userId + ' ' + $scope.isTeacherVideoOpen);
    });

    EventListenerService.subscribe('room.user.quitRoom', function (userId) {
        var videoChange = false;

        for (var i = 0; i < $scope.studentList.length; i++) {
            if ($scope.studentList[i].id === userId) {
                $scope.studentList.splice(i, 1);               
                break;
            }
        }
        for (i = 0; i < $scope.teacherList.length; i++) {
            if ($scope.teacherList[i].userId === userId) {
                $scope.teacherList.splice(i, 1);
                videoChange = true;                
                break;
            }
        }
        for (i = 0; i < $scope.assistantList.length; i++) {
            if ($scope.assistantList[i].userId === userId) {
                $scope.assistantList.splice(i, 1);
                break;
            }
        }
        for (i = 0; i < $scope.microphoneList.length; i++) {
            if ($scope.microphoneList[i].userId === userId) {
                $scope.microphoneList.splice(i, 1);
                videoChange = true;
                if ($scope.microphoneList[i].userId === $scope.largeUid) {
                    $scope.largeUid = 0;
                }
                break;
            }
        }

        if(videoChange) {
            console.log('user quit video change');
            self.onVideoStatusChange();
        }

        if($scope.teacherList.length <= 0) {
            $scope.setIsTeacherVideoOpen(false);
        }                

        UserInfoService.getPool('preFetch').delete(userId);
        UserInfoService.setOld(userId);
        updateUserCount();
    });

    var updateUserCount = function () {
        var userNum = $scope.teacherList.length + $scope.studentList.length + $scope.assistantList.length;
        if ($scope.hasSyncMedia === 0) {
            $scope.userCount = userNum;
        }
        else{
            if($scope.userCount < userNum) {        //in case server notify delay
                $scope.userCount = userNum;
            }
        }
    };

    EventListenerService.subscribe('room.user.enterRoom', function (userId, userType,userRight,nickName) {
        //console.log('room.user.enter ' + userId + ' type ' + userType + ' right ' + userRight + ' name ' + nickName);

        if (userType === 1) {
            updateSpeakerInfo($scope.teacherList,userId,nickName);
        } else if (userType === 2) {
            var user = {id:userId,type:userType,right:userRight,name:nickName};
            if($scope.studentList) {
                $scope.studentList.push(user);
            }
            else
            {
                $scope.studentList = [];
            }

        } else if (userType === 4) {
            updateSpeakerInfo($scope.assistantList,userId,nickName);
        }

        if(nickName && !isNaN(nickName))
        {
            UserInfoService.getPool('chat').add(userId,userType,userRight,nickName);
        }
        else
        {
            UserInfoService.getPool('chat').add(userId,false,userRight);
        }

        updateUserCount();
    });

    EventListenerService.subscribe('room.sync.info', function (userCount) {
        $scope.userCount = userCount;
        $scope.hasSyncMedia = 1;
    });

    $scope.onMicrophoneQueueOpened = function (roomId) {
        if ($scope.roomId != roomId) {
            return;
        }
        EventListenerService.trigger('episode.chat.messageList.keepBottom');
        //this.starStudentVolumeTimer();
        $scope.switchMicrophoneQueue(true);
        if ($scope.isAssistant && $scope.assistantUserId === $scope.user.id) {
            $scope.assistantOffBoard();
        }
    }.bind(this);

    $scope.removeStudentMicrophone = function() {
        var student =$scope.microphoneList;
        $scope.microphoneList = [];

        for(var i = 0; i < student.length; i++) {
            if(student[i].userType == 2) {
                $scope.microphoneList.push(i);
            }
        }
        $scope.largeUid = 0;

        self.onVideoStatusChange();
    };

    $scope.removeMicrophoneById = function(userId) {
        for(var i = 0; i < $scope.microphoneList.length; i++) {
            if($scope.microphoneList[i].userId == userId) {
                $scope.microphoneList.splice(i,1);
                return;
            }
        }
    };

    $scope.insertSpeakerList = function(spklist,speaker) {
        for(var i = 0; i < spklist.length; i++) {
            if(spklist[i].userId == speaker.userId) {
                spklist[i] = speaker;
                return;
            }
        }
        spklist.push(speaker);
    };

    var updateSpeakerInfo = function(userList,userId,nickName) {
        for(var i = 0; i < userList.length; i++) {
            if(userList[i].userId == userId) {
                if(nickName || nickName.length > 0) {
                    userList[i].nickName = nickName;
                    return;
                }
            }
        }

        userList.push({userId:userId,isSpeaking:false,channelId:-1,volumeLevel:0,nickName:nickName});
    };

    this.beginSpeak = function(spklist,userId,channelId,nickName,hasAudio,hasVideo) {
        var spliceIndex = -1;
        for(var i = 0; i < spklist.length; i++) {
            if(spklist[i].userId == userId) {
                spliceIndex = i;
                break;
            }
        }

        if(nickName) {
            if(nickName !=='' && !isNaN(nickName)) {
                UserInfoService.set(userId, nickName);
            }
            else
            {
                UserInfoService.getPool('chat').add(userId);
            }
        }
        
        var speaker = this.createSpeaker(userId, this.isSpeaker(userId),channelId,nickName,hasAudio,hasVideo);        

        console.log('beginSpeak spkSize ' + spklist.length + ' ' + JSON.stringify(speaker) );
        console.log('speakerList '+ JSON.stringify(spklist));

        if (spliceIndex >= 0) {
            spklist.splice(spliceIndex, 1);
            console.log('updateSpeak '+ ' pre ' + spliceIndex + ' new ' + channelId);
        }

        if(speaker.channelId >= 0) 
        {
             spklist.splice(speaker.channelId, 0, speaker);            
        }
        else
        {
            if($scope.isVideoMicOpen) {
                    spklist.push(speaker);
                }
                else {
                    spklist.unshift(speaker);
                }           
        }       

        this.onVideoStatusChange();

        console.log('speakerList '+ JSON.stringify(spklist));

        return spliceIndex;        
    };

    $scope.onMicrophoneQueueClosed = function (roomId) {
        if ($scope.roomId != roomId) {
            return;
        }
        EventListenerService.trigger('episode.chat.messageList.keepBottom');
        //cleanStudentVolumeTimer();
        $scope.switchMicrophoneQueue(false);

        //$scope.microphoneList = [];
        $scope.removeStudentMicrophone();
    };

    $scope.onVideoMicStateChanged = function(roomId,state) {
        if ($scope.roomId != roomId) {
            return;
        }

        console.log('onVideoMicStateChanged ' + state);
        //TODO
        $scope.switchVideoState(state);
    };

    $scope.switchVideoState = function(isOpen) {
        $scope.isVideoMicOpen = isOpen;
        console.log('switchVideoState ' + $scope.isVideoMicOpen);
    };

    $scope.onVideoDisplayStatusChange = function(roomId,largeUid) {
         if ($scope.roomId != roomId) {
            return;
        }
        
        $scope.largeUid = largeUid;
        self.onVideoStatusChange();
    };

    $scope.switchMicrophoneQueue = function (isOpen) {
        $scope.isMicrophoneQueueOpen = isOpen;
    };

    $scope.onMicrophoneCancelAll = function (roomId) {
        if ($scope.roomId != roomId) {
            return;
        }
        //$scope.microphoneList = [];
        $scope.removeStudentMicrophone();
    };

    $scope.onMicrophoneSetTimer = function (micId,micTime) {
        $scope.microphoneSettingTimer = micTime * 1000;
        $scope.microphoneClock = micTime  * 1000;

        console.log('onMicrophoneSetTimer ' + micId + ' time ' + micTime);
        starMicrophoneTimer(true);
    };

    $scope.onAssistantOnBoard = function (userId) {
        $scope.$emit('assistOnBoard', true);
        //starAssistantVolumeTimer();
        $scope.assistantUserId = userId;
    };

    $scope.onAssistantOffBoard = function () {
        $scope.$emit('assistOnBoard', false);
        //cleanAssistantVolumeTimer();
        //$scope.assistantUserId = null;
        $scope.removeMicrophoneById($scope.assistantUserId);
    };

    $scope.getStudentVideoNum = function () 
    {   
        var num = 0;

        for(var i = 0; i < $scope.microphoneList.length; i++) {
            if($scope.microphoneList[i].channelId > -1) {
                num++;
            }
        }

        return num;        
    };

    $scope.$on('RoomInfo', function (event, roomInfo) {
        $scope.assistantUserId = roomInfo.assistantUserId;
        $scope.bannedUserList = roomInfo.bannedUserList;

        $scope.isVideoMicOpen = roomInfo.isVideoMicOpen;
        $scope.largeUid = roomInfo.largeUid;

        if(roomInfo.micStatus && roomInfo.micStatus.length > 0) {

            $scope.microphoneSettingTimer = roomInfo.micStatus[0].timer * 1000;
            $scope.microphoneClock = roomInfo.micStatus[0].valid  * 1000;

            console.log('room info microphone timer ' + $scope.microphoneSettingTimer + ' valid ' + $scope.microphoneClock);
            if($scope.microphoneClock > 0) {
                starMicrophoneTimer(false);
            }
        }

        updateUserCount();

        var speakingUserList = roomInfo.speakingUserList;
        var microphoneList = roomInfo.microphoneList;
        var i = 0, j = 0;

        console.log('room info speaker list ' + speakingUserList.length);

        $scope.microphoneList = [];
        $scope.teacherList = [];
        $scope.assistantList = [];

        cleanStudentVolumeTimer();

        for (i = 0; i < speakingUserList.length; i++) {
            var speaker = speakingUserList[i];
            var isSpeaking = this.isSpeaker(speaker.userId);

            console.log('speaker ' + speaker.userId + ' speaking ' + isSpeaking + ' type  ' + speaker.userType + ' ch ' + speaker.channel + ' isVideoOpen ' + speaker.isVideoOpen);

            if(speaker.nickname) {
                UserInfoService.set(speaker.userId, speaker.nickname);                     
            }

            if(speaker.userType == 1) {
                if(!speaker.nickName) {
                    speaker.nickName = $scope.teacherName;  //copy teacher name if not
                }               

                if(speaker.isVideoOpen) {
                    $scope.setIsTeacherVideoOpen(true);
                } else {
                    $scope.setIsTeacherVideoOpen(false);
                }
                $scope.insertSpeakerList($scope.teacherList,this.createSpeakerWithSpk(speaker,isSpeaking));
            }
            else if(speaker.userType == 4) {
                $scope.insertSpeakerList($scope.assistantList,this.createSpeakerWithSpk(speaker,isSpeaking));
            }
            else {
                if($scope.isMicrophoneQueueOpen) {
                    $scope.insertSpeakerList($scope.microphoneList,this.createSpeakerWithSpk(speaker,isSpeaking));
                }
            }

            console.log('teacher ' + JSON.stringify($scope.teacherList));
            console.log('student ' + JSON.stringify($scope.microphoneList));
        }

        if ($scope.isMicrophoneQueueOpen) {
            for (i = 0; i < microphoneList.length; i++) {
                var microphoneUserId = microphoneList[i];
                var found = false;
                for (j = 0; j < $scope.microphoneList.length; j++) {
                    var student = $scope.microphoneList[j];
                    if (student.userId == microphoneUserId) {
                        found = true;
                        break;
                    }
                }

                if (found === false) {
                    $scope.microphoneList.push(this.createSpeaker(microphoneUserId, false));
                    console.log(' add mic queue ' + microphoneUserId);
                }
            }
            /*if (speakingUserList.length > 0) {
                this.starStudentVolumeTimer();
            }*/
        } else {
            $scope.microphoneList = [];
        }

        console.log(' teacher ' + $scope.teacherList.length + ' student ' + $scope.microphoneList.length);

        this.onVideoStatusChange();
        this.starStudentVolumeTimer();

        /*if ($scope.assistantUserId) {
            starAssistantVolumeTimer();
        }
        starTeacherVolumeTimer();*/
    }.bind(this));

    $scope.$on('$destroy', function () {
        cleanStudentVolumeTimer();
        //cleanTeacherVolumeTimer();
        //cleanAssistantVolumeTimer();
        cleanMicrophoneTimer();
    });

    this.onMicrophoneApplied = function (userId, roomId,nickName) {
        if ($scope.roomId == roomId) {
            var found = false;
            for (var i = 0; i < $scope.microphoneList.length; i++) {
                var student = $scope.microphoneList[i];
                if (student.userId == userId) {
                    found = true;
                    break;
                }
            }

            if (found === false) {
                $scope.microphoneList.push(this.createSpeaker(userId, false,-1,nickName));
            }

            if(nickName) {
                UserInfoService.set(userId,nickName);
            }
            else{
                UserInfoService.getPool('chat').add(userId);
            }
        }
    };
    this.onMicrophoneCanceled = function (userId, roomId) {
        if ($scope.roomId != roomId) {
            return;
        }

        console.log('onMicrophoneCanceled ' + userId );        

        for (var i = 0; i < $scope.microphoneList.length; i++) {
            if ($scope.microphoneList[i].userId == userId) {
                if ($scope.largeUid == $scope.microphoneList[i].userId) {
                    $scope.largeUid = 0;
                }

                if($scope.microphoneList[i].channel >= 0) {
                    if($scope.microphoneSettingTimer > 0) {
                        cleanMicrophoneTimer();
                    }
                }

                $scope.microphoneList.splice(i, 1);
                break;
            }
        }

        this.onVideoStatusChange();
        UserInfoService.getPool('chat').delete(userId);
    };

    this.onVideoStatusChange = function() {
        console.log('video change ' + $scope.largeUid + ' ' + $scope.getStudentVideoNum());
        EventListenerService.trigger('room.video.status',$scope.largeUid, $scope.getStudentVideoNum(), $scope.microphoneList, $scope.teacherList);
    };
};
MicrophoneQueue.prototype.onUserBanned = function (userId) {
    $scope.bannedUserList.push(userId);
};
MicrophoneQueue.prototype.onUserUnBanned = function (userId) {
    $scope.bannedUserList.splice($scope.bannedUserList.indexOf(userId), 1);
};

MicrophoneQueue.prototype.onMicrophoneApproved = function (userId, roomId,channelId,userType,nickName,status) {
    if ($scope.roomId != roomId) {
        return;
    }
    console.log('onMicrophoneApproved user ' + userId + ' timer ' + $scope.microphoneSettingTimer + ' channelId ' + channelId + ' type  ' + userType + ' name ' + nickName + ' status ' + status);

    var dup = -1;
    var hasAudio = status & 0x01;
    var hasVideo = status & 0x02;

    if(userType == 1) {
        this.beginSpeak($scope.teacherList,userId,channelId,nickName,hasAudio,hasVideo);
    }
    else if(userType == 2) {
        dup = this.beginSpeak($scope.microphoneList,userId,channelId,nickName,hasAudio,hasVideo);                
    }
    else if(userType == 4) {
        this.beginSpeak($scope.assistantList,userId,channelId,nickName,hasAudio,hasVideo);
    }

    if($scope.microphoneSettingTimer > 0 && channelId === 0) {
        this.beginMicrophoneTimer(true);
    }

    return dup;    
};

module.exports = MicrophoneQueue;