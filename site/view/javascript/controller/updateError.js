'use strict';
var nativeUI = require('../engine/nativeUI');
var webServerMap = {};

if (truman.buildType === 'test') {//test
    webServerMap = {
        truman: 'http://ke.fenbi.ws',
        ytk: 'http://ytk1.fenbi.ws',
        cdn: 'http://ke.fenbi.ws'
    };
} else if (truman.buildType === 'beta') {//beta
    webServerMap = {
        truman: 'http://kb.fenbi.com',
        ytk: 'http://www.fenbi.com',
        cdn: 'http://kb.fbcontent.cn'
    };
} else {
    webServerMap = {
        truman: 'http://ke.fenbi.com',
        ytk: 'http://www.fenbi.com',
        cdn: 'http://ke.fbcontent.cn'
    };
}

module.exports = [
    '$scope',
    function ($scope) {
        $scope.downloadClient = function () {
            nativeUI.openBrowser(webServerMap.truman + '/download');
        };
    }
];