'use strict';

module.exports =  [
    '$scope',
    'WebApiService',
    'EpisodesService',
    function ($scope, WebApiService,EpisodesService) {

        $scope.labels = [];

        var lectureId = EpisodesService.getEpisodeId();

        WebApiService.get({
            path: 'v3/offline/rollcall/episodes/' + lectureId + '/users'
        }).success(function (labels) {
            for(var i = 0; i < labels.datas.length; i ++) {
                $scope.labels.push({
                    name:labels.datas[i].name,
                    userId:labels.datas[i].userId,
                    value:true
                });
            }
        }).error(function (data, status) {
            console.log('fetch label failed ' + status + ' ' + data);
        });

        $scope.dosubmit = function() {

            $scope.hideUserCountPanel();

            var usersId = [];

            for(var i = 0; i < $scope.labels.length; i++) {
                if($scope.labels[i].value === true) {
                    usersId.push($scope.labels[i].userId);
                }
            }

            WebApiService.post({
                path: 'v3/offline/rollcall/episodes/' + lectureId
            }, {
                params: {
                    'user_ids': usersId.join(),
                    type:0
                }
            }).success(function () {
                console.log('rollcall ' + usersId.join());
            }).error(function (data, status) {
                console.log('fetch label failed ' + status + ' ' + data);
            });


        };

        $scope.$on('$destroy', function() {


        });
}];