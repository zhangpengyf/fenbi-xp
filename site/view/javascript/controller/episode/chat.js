'use strict';
var Base = require('../base/chat');
var live = require('../../engine/live');
var _ = require('underscore');
var $ = require('jquery');
var Chat = function (
    $scope,
    $interval,
    WebApiService,
    ChatMessageService,
    DialogService,
    $compile,
    UserInfoService,
    EventListenerService,
    $timeout
) {
    Base.call(
        this,
        $scope,
        $interval,
        WebApiService,
        ChatMessageService,
        DialogService,
        $compile,
        UserInfoService,
        EventListenerService
    );
    var lastSendMsgTime = 0;
    var $messageList = $('.message-list');
    var messageListDOM = $messageList.get(0);
    $scope.sendFail = '';
    var isMessageListBottom = function () {
        var redundancy = 10;
        if (messageListDOM.scrollHeight - messageListDOM.scrollTop - redundancy <= messageListDOM.clientHeight) {
            return true;
        } else {
            return false;
        }
    };

    var resizeTimer;
    var bottomKeep;
    var onMessageListResize = function () {
        if (resizeTimer) {
            clearTimeout(resizeTimer);
        } else {
            if (isMessageListBottom()) {
                bottomKeep = true;
            }
        }
        if (bottomKeep) {
            $scope.scrollToBottom();
        }
        resizeTimer = setTimeout(function () {
            resizeTimer = null;
            bottomKeep = false;
        }, 300);
    };

    var onMessageListScroll = _.debounce(function () {
        if (isMessageListBottom()) {
            $scope.hasNewMessage = false;
        }
    }, 60);

    $messageList.scroll(onMessageListScroll);
    $(window).resize(onMessageListResize);

    EventListenerService.subscribe('room.onRoomInfo', function () {
        $scope.chatMessageList = [];
    });
    EventListenerService.subscribe('episode.chat.messageList.keepBottom', function () {
        if (!isMessageListBottom()) {
            return;
        }
        var VERIFY_TIME = 3;
        var VERIFY_INTERVAL = 20;
        var count = VERIFY_TIME;
        var lastHeight = null;
        var newHeight = null;
        var oldHeight = null;
        var _verify = function () {
            newHeight = $messageList.height();
            oldHeight = lastHeight;
            if (newHeight === oldHeight) {
                count --;
            } else {
                count = VERIFY_TIME;
            }
            lastHeight = newHeight;
            $scope.scrollToBottom();
            if (count > 0) {
                $timeout(_verify, VERIFY_INTERVAL);
            }
        };
        _verify();
    });

    $scope.resetBanPosition = function ($elem) {
        var $banUnban = $elem.children('.ban-unban-chat');
        if ($messageList.height() - ($banUnban.offset().top - $messageList.offset().top) <= 32) {
            $banUnban.addClass('top-of-nickname ');
        } else if ($messageList.height() - ($banUnban.offset().top - $messageList.offset().top) > 2 * 32 + $elem.outerHeight(true)) {
            $banUnban.removeClass('top-of-nickname');
        }
    };

    $scope.banUser = function (userId) {
        live.banUser(userId, function (retCode) {
            if (retCode < 0) {
                DialogService.notify('禁言"' + $scope.getUser(userId).name + '"失败');
            }
        });
    };

    $scope.unBanUser = function (userId) {
        live.unBanUser(userId, function (retCode) {
            if (retCode < 0) {
                DialogService.notify('解除"' + $scope.getUser(userId).name + '"禁言失败');
            }
        });
    };

    $scope.setPinned = function () {
        $scope.pinned = true;
    };

    $scope.setNotPinned = function () {
        $scope.pinned = false;
    };

    $scope.setBanned = function () {
      $scope.banned = true;

      live.banAllUser(function (retCode) {
        if (retCode < 0) {
          DialogService.notify('禁言失败');
        } else {
          DialogService.notify('禁言成功');
        }
      });


    };

    $scope.setNotBanned = function () {
      $scope.banned = false;

      live.unBanAllUser(function (retCode) {
        if (retCode < 0) {
          DialogService.notify('解除禁言失败');
        } else {
          DialogService.notify('解除禁言成功');
        }
      });
    };


    $scope.cancelTopMessage = function () {
        live.cancelTopMessage(function (retCode) {
            if (retCode < 0) {
                DialogService.notify('删除置顶消息失败');
            }
        });
    };

    $scope.enterSendChatMessage = function (event) {
        if (event.which === 13) {
            event.preventDefault();
            $scope.sendChatMessage();
        }
    };
    $scope.sendChatMessage = function () {
        var message = $scope.draft;
        var type = 0;
        var curts = new Date().getTime();
        var timeDif = curts - lastSendMsgTime;

        $scope.draft = '';
        if (!message || message.length === 0) {
            return;
        }

        if(timeDif < 5000 && $scope.isStudent) {
            $scope.sendFail = '请不要发太快';
            $timeout(function(){
                $scope.sendFail = '';
            }, timeDif);
            return;
        }

        if ($scope.pinned) {
            type = 1;
            $scope.setNotPinned();
        }

        lastSendMsgTime = new Date().getTime();

        var chatMessage = ChatMessageService.createClientMessage($scope.roomId, $scope.userId, message, type);
        live.sendChatMessage(chatMessage.clientMessageId.toString(), chatMessage.content, chatMessage.type, function (retCode) {
            if (retCode < 0) {
                DialogService.alert('发送消息失败');
            }
        });
    };
};
Chat.prototype = Object.create(Base.prototype);
module.exports = [
    '$scope',
    '$interval',
    'WebApiService',
    'ChatMessageService',
    'DialogService',
    '$compile',
    'UserInfoService',
    'EventListenerService',
    '$timeout',
    Chat
];