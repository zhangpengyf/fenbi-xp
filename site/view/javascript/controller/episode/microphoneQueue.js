'use strict';

var live = require('../../engine/live');
var Base = require('../base/microphoneQueue');

var MicrophoneQueue = function (
    $scope,
    $interval,
    DialogService,
    UserInfoService,
    UAService,
    EventListenerService
) {
    Base.call(
        this,
        $scope,
        $interval,
        UserInfoService,
        EventListenerService
    );

    $scope.engineType = 1;

    $scope.onMicrophoneApplied = function (userId, roomId,nickName) {
        this.onMicrophoneApplied(userId, roomId,nickName);
        if ($scope.userId == userId) {
            DialogService.notify('你已进入麦序');
        }
    }.bind(this);

    $scope.onMicrophoneApproved = function (userId, roomId,channelId,userType,nickName,status) {
        var ret = this.onMicrophoneApproved(userId, roomId,channelId,userType,nickName,status);
        if ($scope.userId == userId && userType != 1 && ret < 0) {
            DialogService.notify('你已上麦，可以开始说话');            
        }
    }.bind(this);

    $scope.onMicrophoneCanceled = function (userId, roomId) {
        this.onMicrophoneCanceled(userId, roomId);
        if ($scope.userId == userId) {
            DialogService.notify('你已下麦');
        }
    }.bind(this);

    $scope.applyMicrophone = function () {
        live.applyMicrophone(function (retCode) {
            if (retCode < 0) {
                DialogService.notify('队列已满，请稍后再试');
            }
        });
    };

    $scope.approveMicrophone = function (userId) {
        live.approveMicrophone(userId, function (retCode) {
            if (retCode < 0) {
                console.log('approveMicrophone failed');
            }
        });
    };

    $scope.revokeMicrophone = function (userId) {
        live.revokeMicrophone(userId, function (retCode) {
            if (retCode < 0) {
                console.log('revokeMicrophone failed');
            }
        });
    };

    $scope.switchMicrophoneStatus = function () {
        if ($scope.isMicrophoneQueueOpen) {
            live.closeMicrophoneQueue(function (retCode) {
                if (retCode < 0) {
                    console.log('closeMicrophoneQueue failed');
                }
               // $scope.switchVideoStatus(false);
            });
        } else {
            live.openMicrophoneQueue(function (retCode) {
                if (retCode < 0) {
                    console.log('openMicrophoneQueue failed');
                }
            });
        }
        UAService.logAction('switchMicrophone');
    };

    $scope.switchVideoStatus = function () {
        if ($scope.isVideoMicOpen) {
            live.closeVideoMic(function (retCode) {
                if (retCode < 0) {
                    console.log('switchVideoStatus failed');
                }
            });
        } else {
            live.openVideoMic(function (retCode) {
                if (retCode < 0) {
                    console.log('switchVideoStatus failed');
                }
            });
        }
        UAService.logAction('switchMicrophone');
    };


    $scope.cancelAllMicrophone = function () {
        var doCancelAllMicrophone = function () {
            live.cancelAllMicrophone(function (retCode) {
                if (retCode < 0) {
                    DialogService.alert('清空操作失败');
                }
            });
        };
        DialogService.confirm({
            message: '确定清空麦序队列',
            okCallback: doCancelAllMicrophone
        });
    };

    $scope.shouldDisableMicrophoneButton = function () {
        if($scope.inArray($scope.user.id, $scope.bannedUserList) ||
            $scope.microphoneList.length >= $scope.microphoneQueueCapacity &&
            !$scope.inArray($scope.user.id, $scope.microphoneList, 'userId')) {
            return true;
        }  else {
            return false;
        }
    };

    $scope.onUserBanned = function (userId) {
        this.onUserBanned(userId);
        if ($scope.user.id == userId) {
            $scope.revokeMicrophone(userId);
            DialogService.alert('你已被禁言');
        }
    }.bind(this);

    $scope.onUserUnBanned = function (userId) {
        this.onUserUnBanned(userId);
        if ($scope.user.id == userId) {
            DialogService.alert('你已被解除禁言');
        }
    }.bind(this);
    $scope.assistantOnBoard = function () {
        live.assistantOnBoard(function (retCode) {
            if (retCode < 0) {
                console.log('assistant on board failed');
            }
        });
    };

    $scope.assistantOffBoard = function () {
        live.assistantOffBoard(function (retCode) {
            if (retCode < 0) {
                console.log('assistant off board failed');
            }
        });
    };

    $scope.isApplyMicrophone = function () {
        return !$scope.inArray($scope.user.id, $scope.microphoneList, 'userId');
    };
    $scope.isRevokeMicrophone = function () {
        for (var i = 0; i < $scope.microphoneList.length; i++) {
            if ($scope.microphoneList[i].userId === $scope.user.id && !$scope.microphoneList[i].isSpeaking) {
                return true;
            }
        }
        return false;
    };

    $scope.isCancelMicrophone = function () {
        for (var i = 0; i < $scope.microphoneList.length; i++) {
            if ($scope.microphoneList[i].userId === $scope.user.id && $scope.microphoneList[i].isSpeaking) {
                return true;
            }
        }
        return false;
    };


};
MicrophoneQueue.prototype = Object.create(Base.prototype);
module.exports = [
    '$scope',
    '$interval',
    'DialogService',
    'UserInfoService',
    'UAService',
    'EventListenerService',
    MicrophoneQueue
];
