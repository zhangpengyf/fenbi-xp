'use strict';

var $ = require('jquery');
var live = require('../../engine/live');
var Base = require('../base/sketch');

var Sketch = function (
    $scope,
    WebApiService,
    $interval,
    DialogService,
    EventListenerService,
    $timeout,
    StrokeBufferService
) {
    Base.call(
        this,
        $scope,
        WebApiService,
        $interval,
        EventListenerService,
        $timeout,
        StrokeBufferService
    );
    var self = this;
    var painting = false;
    var strokePoints = [];
    var lastX = 0;
    var lastY = 0;

    var penColor = '#ff0000';
    var lineWidth = 3;

    var syncStroke = function () {
        if (painting === false) {
            return;
        }
        painting = false;
        if (strokePoints.length <= 1) {
            return;
        }
        self.dumpTempContext();

        //sync to server
        live.syncStroke(strokePoints, self.getLineWith() ,self.getLineColor(),function (retCode) {
            if (retCode < 0) {
                DialogService.alert('同步笔迹失败');
            }
        });

        strokePoints = [];
    };        

    this.onKeynoteRendered = function () {
        var pageNum = $scope.keynote.currentPageIndex;
        
        var pointsArray = StrokeBufferService.get(
            $scope.roomId,
            $scope.keynote.keynoteId,
            pageNum
        );        

        if (pointsArray.length > 0) {
            self.renderStrokes(pointsArray);           
        }      
    };

    $scope.$on('eraseScreen', function(){
        $scope.eraseStroke();
    });

    $scope.$on('switchStoke', function($event,width,color,idx){
        var cursorImg = 'url("../images/pens/' + width + idx + '.png")';
        $('.drawable').css({cursor: cursorImg});

        penColor = color;
        lineWidth = width;

        console.log('switchStoke ',penColor);
        self.setStokeStyle(width,color);
    });    

    $scope.eraseStroke = function () {
        self.eraseStrokeCanvas();

        if ($scope.isTeacher === false) {
            return;
        }
        live.eraseStroke(function(retCode) {
            if (retCode < 0) {
                console.log('erase stroke failed');
            }
        });
    };

    if ($scope.isTeacher ) {
        var $tmpCanvas = $('#temp-stroke');
        $tmpCanvas.mousedown(function(event) {
            painting = true;
            strokePoints = [];
            var mousePosition = self.getMousePosition(event);
            lastX = mousePosition.x;
            lastY = mousePosition.y;
            strokePoints.push([lastX/ self.getCanvasWidth(), lastY / self.getCanvasHeight()]);
            event.preventDefault();
        });

        $(document).bind('keydown', function(event) {
            if(event.keyCode == 27) {
                $scope.eraseStroke();
            }
        });

        $tmpCanvas.mouseup(function() {
            syncStroke();
        });

        $tmpCanvas.mouseleave(function() {
            syncStroke();
        });

        $tmpCanvas.mousemove(function(event) {
            if (painting === false) {
                return;
            }
            var mousePosition = self.getMousePosition(event);
            var mouseX = mousePosition.x;
            var mouseY = mousePosition.y;
            strokePoints.push([mouseX / self.getCanvasWidth(), mouseY / self.getCanvasHeight()]);
            self.curveThroughPoints(self.getPointArrayFromPercentListArray(strokePoints),lineWidth,penColor);
            lastX = mouseX;
            lastY = mouseY;
        });
    }
};
Sketch.prototype = Object.create(Base.prototype);

module.exports = [
    '$scope',
    'WebApiService',
    '$interval',
    'DialogService',
    'EventListenerService',
    '$timeout',
    'StrokeBufferService',
    Sketch
];