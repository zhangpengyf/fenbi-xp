'use strict';
var $ = require('jquery');
var live = require('../../engine/live');
var device = require('../../engine/device');
var moment = require('moment');

module.exports = [
    '$scope',
    '$interval',
    'DialogService',
    'UAService',
    function ($scope,$interval, DialogService, UAService) {
    var overtimePromise = null;
    var volumeSetPromise = null;

    var UserType = {
        Teacher: 1,
        Student: 2,
        Admin: 3,
        Assistant: 4
    };

    var lastClickTime = 0;
    $scope.showMicTimerSetting = false;
    $scope.isTeacher = $scope.userType == UserType.Teacher;
    $scope.clickTimes = 0;
    $scope.delay = 0;
    $scope.lost = 0;
    $scope.rtt = 0;
    $scope.recvbytes = 0;
    $scope.sendbytes = 0;
    $scope.inputVolumePercent = 0;
    $scope.outputVolumePercent = 0;
    $scope.outputMuteState = null;
    $scope.inputMuteState = null;
    $scope.cameraOpenState = $scope.isTeacher ? false : true;
    $scope.isTimeUp = moment().isAfter(moment($scope.classOpenTime));

    var setDuration = function () {
        var duration = Math.floor(($scope.classCloseTime - $scope.classStartTime) / 1000);
        var minutes = Math.floor(duration / 60);
        var seconds = duration - 60 * minutes;
        $scope.classDuration = minutes + ':' + seconds;
    };

    if ($scope.isClassStarted) {
        setDuration();
    } else {
        $scope.$watch('isClassStarted', function () {
            if ($scope.isClassStarted) {
                setDuration();
            }
        });
    }

    var leftTime = moment($scope.classCloseTime).diff(moment(), 'milliseconds');
    if (leftTime < 0) {
        $scope.isOvertime = true;
    } else {
        $scope.isOvertime = false;
        overtimePromise = $interval(function () {
            $scope.isOvertime = true;
        }, leftTime, 1, true);
    }

    $scope.$on('timer-stopped', function (){
        if (!$scope.isClassStarted && !$scope.isTimeUp) {
            $scope.isTimeUp = true;
        }
    });

    $scope.$on('$destroy', function () {
        if (overtimePromise !== null) {
            $interval.cancel(overtimePromise);
        }
        if (volumeSetPromise !== null) {
            $interval.cancel(volumeSetPromise);
        }
    });

    $scope.showStatus = function() {
        var cur = new Date();
        if(cur.getMilliseconds() - lastClickTime < 1000){
            $scope.clickTimes++;
        }
        else{
            $scope.clickTimes = 0;
        }
        lastClickTime = cur.getMilliseconds();

    };

    var convertBytes = function(bytes) {
        var ret = bytes;
        if(bytes > 1024) {
            ret = Math.round(bytes / 1024) + ' KB';
        }

        return ret;
    };

    $scope.onNetStatistics = function(delay,lost,rtt,recvbytes,sendbytes) {
        $scope.delay = delay;
        $scope.lost = lost;
        $scope.rtt = rtt;
        $scope.recvbytes = convertBytes(recvbytes);
        $scope.sendbytes = convertBytes(sendbytes);
    };


    var getOutputVolume = function () {
        device.getOutputVolume( function (retCode, volumePercent) {
            if (retCode >= 0) {
                $scope.outputVolumePercent = volumePercent;
                //$scope.$apply();
            }
        });
    };

    var getInputVolume  = function () {
        device.getInputVolume( function (retCode, volumePercent) {
            if (retCode >= 0) {
                $scope.inputVolumePercent = volumePercent;
                //$scope.$apply();
            }
        });
    };

    $scope.beginSendVideo = function () {

        //device.setVideoCaptureDevice(0);

        live.beginSendVideo(function (retCode) {
            if (retCode < 0) {
                DialogService.alert('打开视频失败');
            }
        });
        $scope.cameraOpenState = true;
    };

    $scope.stopSendVideo = function () {
        live.stopSendVideo(function (retCode) {
            if (retCode < 0) {
                DialogService.alert('关闭视频失败');
            }
        });

        //EventS("room.video.refresh");

        $scope.cameraOpenState = false;
    };

    $scope.muteOutput = function () {
        device.muteOutput(function (retCode) {
            if (retCode < 0) {
                DialogService.alert('听筒设置静音失败');
            }
        });
    };

    $scope.unMuteOutput = function () {
        device.unMuteOutput(function (retCode) {
            if (retCode < 0) {
                DialogService.alert('听筒取消静音失败');
            }
        });
    };

    $scope.muteInput = function () {
        device.muteInput(function (retCode) {
            if (retCode < 0) {
                DialogService.alert('麦克风设置静音失败');
            }
            $scope.inputMuteState = true;
        });
    };

    $scope.unMuteInput = function () {
        device.unMuteInput(function (retCode) {
            if (retCode < 0) {
                DialogService.alert('麦克风取消静音失败');
            }
            $scope.inputMuteState = false;
        });
    };

    $scope.setOutputVolume = function () {
        device.setOutputVolume($scope.outputVolumePercent, function (retCode) {
            if (retCode < 0) {
                DialogService.alert('设置音量失败');
            }
        });
    };

    $scope.setInputVolume = function () {
        device.setInputVolume($scope.inputVolumePercent, function (retCode) {
            if (retCode < 0) {
                DialogService.alert('设置音量失败');
            } else {
                if ($scope.inputMuteState) {
                    $scope.unMuteInput();
                }
            }
        });
    };

    $scope.setMicTimer = function () {
        $scope.showMicTimerSetting = !$scope.showMicTimerSetting;
    };

        $scope.showMicrophoneSettingDialog = function () {
        device.startMicrophoneTest( function (retCode) {
            if (retCode < 0) {
                console.log('startMicrophoneTest failed');
                return false;
            } else {
                $scope.$broadcast('startMicrophoneTest');
                return true;
            }
        });
        $scope.dialog.microphoneSetting = true;
        UAService.logAction('showSettingDialog');
    };

    $scope.setMicrophoneTime = function() {
        var time = parseInt($('#set_mic select').val());
        var prompt = '成功设置麦时长为 ' + time + ' 分钟';

        if(isNaN(time) || time === 0) {
            time = 0;
            prompt = '成功取消设置麦倒计时';
        }

        live.setTimerMicrophone(0,time * 60);
        $scope.showMicTimerSetting = !$scope.showMicTimerSetting;

        DialogService.notify(prompt);
    };

    $scope.hideMicrophoneSettingDialog = function () {
        getOutputVolume();
        device.endMicrophoneTest( function (retCode) {
            if (retCode < 0) {
                console.log('endMicrophoneTest failed');
            }
            $scope.$broadcast('endMicrophoneTest');
        });
        device.closeVideoPreview();
        $scope.dialog.microphoneSetting = false;
    };

    var isSysInputMute = function(){
        if($scope.isTeacher) {
            device.isSysInputMute(function(retCode, isMute){
                if (retCode >= 0) {
                    if(isMute){
                        device.setSysInputMute(false,function(ret){
                            if(ret > 0) {
                                console.log('isSysInputMute auto open');
                            }
                        });
                    }
                }
            });
        }
    };

    $scope.uploadKeynote = function () {
        $('#keynote-to-upload').click();
        UAService.logAction('uploadKeynote');
    };

    $scope.$on('recordingDevicesChange',function() {
        $scope.showMicrophoneSettingDialog();
    });

    $scope.$on('isInputMute',function(event, isMute) {
        $scope.inputMuteState = isMute;
    });

    $scope.$on('isOutputMute',function(event,isMute) {
        $scope.outputMuteState = isMute;
    });

    volumeSetPromise = $interval(function() {
        getOutputVolume();
        getInputVolume();
        isSysInputMute();
    }, 1000);

    $(document).ready(function () {
        $('#set_mic .input_box').keydown(function(event){
            if(event.which == 13) {
                $scope.setMicrophoneTime();
            }
        });
    });

    $(document).mousedown(function(event){
        var elem = $(event.target);

        if($scope.showMicTimerSetting) {
            if (!elem.is('.mic-setting,.mic-setting *') && !elem.is('.sel-mic')) {
                $scope.showMicTimerSetting = false;
            }
        }
    });
}];
