'use strict';
var $ = require('jquery');
var live = require('../../engine/live');
var Base = require('../base/keynote');
var Q = require('q');

var Keynote = function (
    $scope,
    $window,
    $rootScope,
    $location,
    WebApiService,
    KeynoteService,
    DialogService,
    UAService,
    $interval,
    KeynoteManageService,
    EventListenerService,
    $timeout,
    MaterialService
) {
    Base.call(
        this,
        $scope,
        KeynoteService,
        $interval,
        $window,
        KeynoteManageService,
        WebApiService,
        EventListenerService,
        $timeout,
        MaterialService
    );
    var self = this;
    var delayLoadKeynoteTimer = null;
    var singalTimer = null;
    var lineWidth = 3;
    var lineColorIdx = 0;
    var strokeColor = 0xff0000;

    $scope.networkStatus = 0;
    $scope.avgNetworkLost = 0;
    $scope.avgSingalBar = 3;
    $scope.singalPrompt = '';
    $scope.dialog.netWorkWeak = false;
    $scope.showStokeSetting = false;    
    $scope.showLargeVideo = true;  

    var delayLoadKeynote = function () {
        var delayTime = Math.round(Math.random() * 20 * 1000);
        delayLoadKeynoteTimer = $timeout(function () {
            $scope.loadKeynote();
        }, delayTime);
    };

    var clearDelayLoadTimer = function () {
        if (delayLoadKeynoteTimer) {
            $timeout.cancel(delayLoadKeynoteTimer);
            delayLoadKeynoteTimer = null;
        }
    };
    var verifyKeynoteConvert = function (keynoteId) {
        var deferred = Q.defer();
        $scope.keynoteConverting = true;
        var _verify = function () {
            WebApiService.get({
                path: 'keynotes/' + keynoteId + '/isuploaded',
                courseId: $scope.courseId
            }).success(function (data) {
                if (data === 'true') {
                    $timeout(function () {
                        deferred.resolve();
                    }, 3000);
                } else {
                    $timeout(_verify, 1000);
                }
            }).error(function (res, status) {
                deferred.reject();
                console.log('verify keynote convert failed: ' + status);
            });
        };
        _verify();
        return deferred.promise;
    };

    $scope.onPageNumberKeyup = function (event, pageNumber) {
        var keyCode = event.keyCode;
        if (keyCode === 13) {
            if (isNaN(pageNumber) || !pageNumber || pageNumber < 0) {
                this.forceUpdatePnView(0);
                return;
            }
            if ((pageNumber - 1) !== $scope.keynote.currentPageIndex) {
                $scope.pageTo(pageNumber - 1);
            }
        }
    };
    $scope.onPageNumberBlur = function (pageNumber) {
       if (isNaN(pageNumber) || !pageNumber || pageNumber < 0) {
            this.forceUpdatePnView(0);
            return;
        }
        if ((pageNumber - 1) !== $scope.keynote.currentPageIndex) {
            $scope.pageTo(pageNumber - 1);
        }
    };

    $scope.nextPage = function() {
        if ($scope.keynote.currentPageIndex >= $scope.keynote.totalPageNum - 1) {
            return;
        }
        //no need to change currentPageIndex rely on
        //onPageTo method
        live.pageUp(function(retCode) {
            if (retCode < 0) {
                DialogService.alert('翻页操作失败');
            }
        });
    };

    $scope.pageTo = function (pageIndex) {
        live.pageTo(pageIndex, function (retCode) {
            if (retCode < 0) {
                DialogService.alert('跳转到' + pageIndex + '页失败');
            }
        });
    };

    $scope.prevPage = function() {
        if ($scope.keynote.currentPageIndex <= 0) {
            return;
        }
        live.pageDown(function(retCode) {
            //no need to change currentPageIndex rely on
            //onPageTo method
            if (retCode < 0) {
                DialogService.alert('翻页操作失败');
            }
        });
    };

    $scope.onPageTo = function (pageIndex,strokeCached) {        
        $scope.keynote.currentPageIndex = pageIndex;

        if(strokeCached <= 0) {
            EventListenerService.trigger('room.live.fetch.stroke',pageIndex);
        }        
    };

    var clearUploadValue = function () {
        $('#keynote-to-upload').val(null);
    };

    $scope.uploadKeynote = function () {
        $('#keynote-to-upload').click();
        UAService.logAction('uploadKeynote');
    };

    $scope.downloadMaterial = function (materialId) {
        WebApiService.get({
            path: '/api/v3/materials/' +  materialId + '/path'
        }).success(function(data){
            if (data.code == 1) {
                MaterialService.downloadMaterials(data.data.urls);
            }
        }).error(function (res, status) {
           console.log('get download url failed ' + res + ' ' + status);
        });
        //var url = WebApiService.server('truman') + '/api/materials/' +  materialId;
        //nativeUI.openBrowser(url);
    };

    var uploadHandle = null;
    var uploadAborted = false;
    var upload = function (keynoteFile) {
        $scope.isUploading = true;
        return WebApiService.upload(
            {
                path: 'keynotes',
                courseId: $scope.courseId
            },
            {
                params: {
                    episodeId: $scope.roomId
                },
                file: keynoteFile,
                __from: 'episode'
            }
        ).progress(function(evt) {
            var percent = parseInt(100.0 * evt.loaded / evt.total);
            $scope.uploadStatusString = percent + '%';
        }).success(function(data) {
            $scope.safeApply(function () {
                $scope.isUploading = false;
                $scope.uploadStatusString = '';
            });
            clearUploadValue();
            var keynoteId = parseInt(data.id);
            verifyKeynoteConvert(keynoteId).then(function () {
                var totalPageNum = parseInt(data.pageCount);
                live.publishKeynote(keynoteId, totalPageNum, 0, function (retCode) {
                    if (retCode < 0) {
                        DialogService.alert('上传课件信息失败');
                    }
                });
            }).catch(function () {

            }).fin(function () {
                $scope.keynoteConverting = false;
            });
        }).error(function (res, status) {
            $scope.isUploading = false;
            $scope.uploadStatusString = '';
            clearUploadValue();
            if (uploadAborted) {
                uploadAborted = false;
                return;
            }
            DialogService.alert('上传课件失败');
            console.log('upload keynoteId failed: ' + status);
        });
    };

    $scope.onFileSelect = function($files) {
        if ($files.length <= 0) {
            return;
        }
        var keynoteFile = $files[0];
        /*var fileType = keynoteFile.type;
        if (fileType !== 'application/pdf') {
            DialogService.alert('请选择PDF文件');
            return;
        }*/
        var keynoteFileSize = keynoteFile.size;
        if (keynoteFileSize > 10 * 1024 * 1024) {
            DialogService.alert('上传文件不能超过10M，请重新选择文件');
            return;
        }
        if (uploadHandle) {
            uploadHandle.abort();
            uploadAborted = true;
            uploadHandle = null;
        }
        uploadHandle = upload(keynoteFile);
    };

    $scope.erase = function () {
        $scope.$broadcast('eraseScreen');
        UAService.logAction('eraseScreen');
    };

    $scope.switchStoke = function () {        
        $scope.showStokeSetting = !$scope.showStokeSetting;        
    };

    $scope.applyStoke = function() {
        $scope.$broadcast('switchStoke',lineWidth,strokeColor,lineColorIdx);
        UAService.logAction('switchStoke');
    };

    function rgb2hex(rgb) {
        rgb = rgb.match(/^rgba?[\s+]?\([\s+]?(\d+)[\s+]?,[\s+]?(\d+)[\s+]?,[\s+]?(\d+)[\s+]?/i);
        return (rgb && rgb.length === 4) ? '#' +
        ('0' + parseInt(rgb[1], 10).toString(16)).slice(-2) +
        ('0' + parseInt(rgb[2], 10).toString(16)).slice(-2) +
        ('0' + parseInt(rgb[3], 10).toString(16)).slice(-2) : '';
    }


    $scope.switchVideoPPT = function () {
        $scope.showLargeVideo = !$scope.showLargeVideo;        
        console.log('switchVideoPpt ' + $scope.showLargeVideo);
    };

    $scope.addQuestion = function() {
        if(!$scope.questionMask) {
            $scope.showQuestionPanel(1);
        }        
    };

    $(document).ready(function () {
        $.fn.selectBox = function (onchage,idx) {
            var thisinput = $(this).children().eq(0);
            var thisul = $(this).find('ul');

            thisul.css('top', thisinput.outerHeight());
            thisinput.find('.input_box').html(thisul.children().eq(idx).html());

            $(thisinput).each(function () {               

                $(this).click(function () {
                    var thisinput = $(this);
                    var thisul = $(this).parent().find('ul');                    

                    if (thisul.css('display') == 'none') {
                        if (thisul.height() > 200) {
                            thisul.css({ height: '200' + 'px', 'overflow-y': 'scroll' });
                        }

                        thisul.css('z-index', 999999);
                        thisul.fadeIn('100');
                        thisul.hover(function () { }, function () { thisul.fadeOut('100'); thisul.css('z-index', 0); });

                        var thislis = thisul.find('li');

                        thislis.click(function () {
                            var inputbox = thisinput.find('.input_box');                            

                            if(inputbox.is('div')) {
                                thisinput.find('.input_box').html($(this).html());
                            }
                            else{
                                thisinput.find('.input_box').val($(this).html());
                            }

                            thisul.css('z-index', 0);
                            thisul.fadeOut('50');
                            onchage($(this).children().eq(0), thislis.index(this) );
                        }).hover(function () { $(this).addClass('hover'); }, function () {
                            $(this).removeClass('hover');
                        });
                    }
                    else {
                        thisul.css('z-index', 0);
                        thisul.fadeOut('fast');
                    }
                });
            });
        };

        $('#sel_width').selectBox(function (html) {
            lineWidth  = parseInt(html.children().eq(0).css('height'));
            $scope.applyStoke();
        },2);

        $('#sel_color').selectBox(function (html,index) {
            lineColorIdx = index;
            strokeColor = rgb2hex(html.css('background'));
            $scope.applyStoke();
        },0);

    });

    $(document).mousedown(function(event){
        var elem = $(event.target);

        if($scope.showStokeSetting)
        {
            if (!elem.is('.stroke-setting,.stroke-setting *') && !elem.is('.sel-pen')) {
                $scope.switchStoke();
                $scope.applyStoke();
            }
        }
    });

    EventListenerService.subscribe('episode.keynote.loadKeynoteImmediately', function () {
        if (!$scope.keynoteLoaded &&
            self.loadKeynoteId !== $scope.keynote.keynoteId) {
            clearDelayLoadTimer();
            $scope.loadKeynote();
        }
    });

    EventListenerService.subscribe(
        'episode.keynote.onKeynoteInfo',
        function (
            keynoteId,
            totalPageNum,
            currentPageIndex
        ) {
            if(keynoteId <= 0) {        //in case server'bug
                return;
            }

            self.onKeynoteInfo(keynoteId, totalPageNum, currentPageIndex);
            $scope.keynote = KeynoteService.createKeynote(keynoteId, totalPageNum, currentPageIndex);
            $scope.safeApply();
            clearDelayLoadTimer();
            self.abortKeynote();
            if ($scope.isTeacher || $scope.isClassStarted) {
                $scope.loadKeynote();
            } else {
                delayLoadKeynote();
            }

            EventListenerService.trigger('room.live.fetch.stroke',currentPageIndex);
        }
    );

    EventListenerService.subscribe('av.network.status', function (singal) {
        var singalBar = 0;
        if(singal < 0.005) {
            singalBar = 5;
        }
        else if (singal >= 0.005 && singal < 0.01) {
            singalBar = 4;
        }
        else if (singal >= 0.01 && singal < 0.03) {
            singalBar = 3;
        }
        else if (singal >= 0.03 && singal < 0.06) {
            singalBar = 2;
        }
        else if (singal >= 0.06) {
            singalBar = 1;
        }

        $scope.networkStatus = singalBar;
        $scope.avgSingalBar = $scope.avgSingalBar * 0.6 + singalBar * 0.4;
        $scope.avgNetworkLost  = $scope.avgNetworkLost * 0.6 + singal * 0.4;
    });

    singalTimer = $interval(function(){

        if(!$scope.isTeacher) {
            if($scope.avgSingalBar < 3) {
                $scope.singalPrompt = '当前网络与服务器连接不稳定';
            }
            else{
                $scope.singalPrompt = '';
            }
        }
        else
        {
            if($scope.avgSingalBar >= 3) {
                $scope.singalPrompt = '';
            }
            else if($scope.avgSingalBar < 3) {
                $scope.singalPrompt = '当前网络不稳定，学生偶尔会感觉有点卡';
            }
            else if($scope.avgSingalBar <= 2) {
                $scope.singalPrompt = '当前网络比较差，学生明显感觉卡';
            }
            else if($scope.avgSingalBar <= 1) {
                $scope.singalPrompt = '当前网络很差，课程几乎无法进行';
            }
        }

        console.log('network status ' + $scope.avgNetworkLost );

        if($scope.avgNetworkLost > 0.06 && $scope.isTeacher) {  //show dialog
            $scope.dialog.netWorkWeak = true;
            UAService.logAction('showNetworkDialog');
        }
    }, 20000);

    $scope.hideNetworkDialog = function() {
        $scope.dialog.netWorkWeak = false;
    };

    $scope.startClass = function () {
        if (!$scope.keynote) {
            DialogService.alert('请先上传课件');
            return;
        }
        $rootScope.$broadcast('startClass');
    };

    $scope.fullScreen = function () {
        self.fullScreen();
        UAService.logAction('episodeFullScreen');
    };

    $scope.$on('$destroy', function () {
        if (uploadHandle) {
            uploadHandle.abort();
            uploadAborted = true;
            uploadHandle = null;
        }
        clearDelayLoadTimer();
        $interval.cancel(singalTimer);
    });
};
Keynote.prototype = Object.create(Base.prototype);
module.exports = [
    '$scope',
    '$window',
    '$rootScope',
    '$location',
    'WebApiService',
    'KeynoteService',
    'DialogService',
    'UAService',
    '$interval',
    'KeynoteManageService',
    'EventListenerService',
    '$timeout',
    'MaterialService',
    Keynote
];