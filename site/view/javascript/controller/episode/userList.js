'use strict';
var $ = require('jquery');

module.exports = [
    '$scope',
    '$timeout',
    'Levenshtein',
    function ($scope, $timeout, Levenshtein) {
        var blurTimeout = null;
        $scope.searchParam = '';
        $scope.searchResultList = [];
        $scope.noSearchResult = false;

        $scope.onMouseenter = function (event) {
            $(event.currentTarget).children('.right-part').removeClass('display-none');
        };

        $scope.onMouseleave = function (event) {
            $(event.currentTarget).children('.right-part').addClass('display-none');
        };

        $scope.onSearchInputFocus = function () {
            $timeout.cancel(blurTimeout);
            $scope.showClearInput = true;
        };

        $scope.onSearchInputBlur = function () {
            blurTimeout = $timeout(function () {
                $scope.showClearInput = false;
            }, 200);
        };

        $scope.clearSearchParam = function () {
            $scope.searchParam = '';
            $('.search-input').focus();
            $scope.onSearchInputFocus();
        };

        $scope.$watch('searchParam', function () {
            var searchParam = $scope.searchParam.replace(' ', '');
            if (!searchParam) {
                $scope.searchResultList = [];
                $scope.noSearchResult = false;
                return;
            }

            var studentArrId = [];
            for(var j = 0; j < $scope.studentList.length; j++) {
                studentArrId.push($scope.studentList[j].id);
            }

            var matchList = $scope.assistantList.concat(studentArrId);
            matchList.push($scope.episode.teacher.userId);

            var searchResultList = [];
            var pureSearchResult = [];
            for (var i = 0; i < matchList.length; i ++) {
                var matchName = '';
                if (matchList[i] === $scope.episode.teacher.userId) {
                    matchName = $scope.episode.teacher.name;
                } else {
                    matchName = $scope.getUser(matchList[i]).name;
                }
                if (matchName.indexOf(searchParam) >= 0) {
                    searchResultList.push({
                        userId: matchList[i],
                        similarity: Levenshtein.getSimilarity(matchName, searchParam)
                    });
                }
            }

            if (!searchResultList.length) {
                $scope.noSearchResult = true;
                return;
            }

            searchResultList.map(function (item) {
                if (item.similarity == 1) {
                    pureSearchResult.push(item.userId);
                }
            });

            if (pureSearchResult.length <= 0) {
                searchResultList.sort(function (result1, result2) {
                    return result2.similarity - result1.similarity;
                });

                searchResultList.map(function (item) {
                    if (item.similarity > 0) {
                        pureSearchResult.push(item.userId);
                    }
                });
            }
            $scope.noSearchResult = false;
            $scope.searchResultList = pureSearchResult;
        });
    }
];