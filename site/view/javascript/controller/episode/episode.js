'use strict';

var Base = require('../base/room');
var live = require('../../engine/live');
var $ = require('jquery');
var nativeUI = require('../../engine/nativeUI');

var Episode = function (
    $scope,
    $location,
    $interval,
    $timeout,
    WebApiService,
    TimeAdjustService,
    DialogService,
    UserInfoService,
    UAService,
    $window,
    CourseMapService,
    $rootScope,
    $http,
    EventListenerService    
) {
    Base.call(
        this,
        $scope,
        WebApiService,
        DialogService,
        $window,
        CourseMapService,
        $timeout,
        EventListenerService,
        UserInfoService,
        $location
    );
    var self = this;
    var enteringTimeoutPromise = null;
    var updateUserListTimerPromise = null;
    var pptZoom = 'smaller';
    var intervalPromise = null;

    $scope.userType = $scope.trumanUserType;
    $scope.dataLoaded = false;
    $scope.dialog = {};
    $scope.users = {};
    $scope.isAssistantOnBoard = false;
    $scope.isVideoMicOpen = false;
    $scope.isTeacherVideoOpen = false;

    var downloadSelfUserInfo = function (successCb, errorCb) {
        WebApiService.get({
            path: 'users/current'
        }, {
            params: {
                nickname: true
            }
        }).success(function (data) {
            $scope.user = {};
            if (data) {
                $scope.user.id = data.id;
                $scope.user.name = data.nickname.toString();
            }
            if (successCb) {
                successCb(data);
            }
            self.onDataLoaded();
        }).error(function (res, status) {
            if (errorCb) {
                errorCb(res, status);
            }
             if (status !== 0) {
                DialogService.alert('用户信息加载失败', $scope.backToList);
            } else {
                DialogService.alert('进入教室过程中，网络连接出现问题', $scope.backToList, '退出教室');
            }
        });
    };
    var init = function () {
        if (!$scope.episode) {
            downloadSelfUserInfo();
        }
        // teacher related logic for close the class room
        if ($scope.isTeacher) {
            var timeout = 30 * 60 * 1000; // 30 min

            // If the teacher is idle for more than 30min, show the closing dialog
            intervalPromise = $interval(function () {
                var currentTime = TimeAdjustService.getCurrentTime();
                var scheduledEndTime = $scope.episode.endTime;
                console.log('currentTime', currentTime, 'scheduledEndTime', scheduledEndTime);
                if ($scope.isClassStarted && currentTime > scheduledEndTime + timeout) {
                    nativeUI.getIdleInterval(function (retCode, idleTimeInSec) {
                        if (idleTimeInSec > 30 * 60000) { // 30min
                            $scope.showClosingDialog();
                        }
                    });
                }
            }, 60000);
        }
    };

    this.onDataLoaded = function () {        
        console.log('onDataLoaded ' + JSON.stringify($scope.user));

        if ($scope.episode && $scope.user && $scope.sessionCookie ) {
            if ($scope.isTeacher) {
                // if this is a teacher, use the teacher's name as its display name
                $scope.user.name = $scope.episode.teacher.name;
            } else if ($scope.isStudent || $scope.isAssistant) {
                // if this is a student, use the nickname as its display name
                if (!$scope.user.name) {
                    // no nickname, add it
                    EventListenerService.trigger('head.nickname.showNicknameDialog');
                    return;
                }
            }            

            $scope.dialog.entering = false;
            $scope.dataLoaded = true;
            $scope.episode.ticket.cookie = $scope.sessionCookie;
            $rootScope.inEpisode = true; // must set inEpisode to true after dataLoaded
            this.enterRoom();
        }
    };


    this.enterRoom = function (errorMessage) {
        if ($scope.dialog.entering) {
            return;
        }
        var episode = $scope.episode;
        this.baseEnterRoom(episode);

        $scope.classOpenTime = TimeAdjustService.getLocalTime(episode.startTime);
        $scope.classCloseTime = TimeAdjustService.getLocalTime(episode.endTime);
        $scope.isClassStarted = false;
        $scope.classStartTime = 0;
        $scope.isClassEnded = false;
        $scope.userCount = 1;

        this.setUpdateUserListTimer();

        console.log('enter room ' + JSON.stringify(episode.ticket.serverList));

        if($scope.episode.teachChannel === -1) {
            episode.ticket.type = 5; //modify ticket_type for client
        }

        live.enterRoom(episode.ticket, function (retCode) {
            errorMessage = errorMessage || '无法进入教室';
            if (retCode < 0) {
                console.log('enterRoom failed');
                setTimeout(function () {
                    DialogService.alert(errorMessage, $scope.backToList, '退出教室');
                }, 0);
            } else {
                this.enterTimeout(errorMessage);
                $scope.$emit('enterEpisode');
                live.setNickname($scope.user.name,function(){
                    console.log('set nick name ' + $scope.user.name);
                });
            }
        }.bind(this));
    };

    EventListenerService.subscribe('episode.nickname.init', function () {
        downloadSelfUserInfo(function () {
            EventListenerService.trigger('episode.nickname.hideNicknameDialog');
        });
    });

    EventListenerService.subscribe('head.nickname.nicknameDialogHide', function () {
        if (!$scope.user.name) {
            $scope.backToList();
        }
    });


    $scope.doQuitRoom = function (callback) {
        console.log('quitting room');
        $scope.dialog.closedLoading = 'quit';
        live.quitRoom(function () {
            $scope.dialog.closedLoading = null;
            $scope.$emit('quitEpisode');

            console.log(JSON.stringify($scope.episode));

            if (callback) {
                callback();
            }
        });
    };

    $scope.backToList = function (force) {
        console.log('back to episode list from live room');
        $scope.confirmCloseAction(force ? 'error' : 'episodeList', function () {
            $scope.dialog = {};  // clear all the dialog

            if($scope.isStudent) {
                $location.path('/course');
            }
            else{
                $location.path('episodes').search({
                 filter: 'live',
                });
            }
        });
        UAService.logAction('backToEpisodeList');
    };

    $scope.$on('assistOnBoard', function (event, isBoard) {
        $scope.isAssistantOnBoard = isBoard;
    });

    $scope.doEndClass = function (callback) {
        console.log('doEndClass');
        $scope.dialog.closedLoading = 'endClass';
        $scope.safeApply();
        if (callback) {
            $scope.endClassCallback = callback;
        } else {
            $scope.endClassCallback = null;
        }
        live.endClass(function () {
            $scope.dialog.closedLoading = null;
        });

        if($scope.episode.teachChannel === -1)  //offline recoder
        {
            WebApiService.post({
                path: 'v3/offline/episodes/' + $scope.episode.id + '/finish'
            }).success(function () {
                console.log('offline finish ' + $scope.episode.id);
            }).error(function (data, status) {
                console.log('offline finish' + status + ' ' + data);
            });
        }
    };

    $scope.endClass = function () {
        UAService.logAction('endClass');
        DialogService.confirm({
            message: '确定下课？(警告！下课后将无法重新进入教室！)',
            okCallback: function () {
                $scope.doEndClass($scope.backToList);
            }
        });
    };

    $scope.startSend = function () {
        live.startSend();
    };

    EventListenerService.subscribe('room.onRoomInfo', function (
        teacherList,
        studentList,
        isMicrophoneQueueOpen,
        microphoneList,
        speakingUserList,
        userCount,
        startTime, // is String in ms
        latestMessageId,
        latestStrokeId,
        isAssistantOnBoard) {
        self.clearEnterTimeout();
        $scope.dialog.entering = false;  // hide the entering dialog
        $scope.classStartTime = TimeAdjustService.getLocalTime(startTime);
        $scope.isClassStarted = (startTime !== '0');
        $scope.isMicrophoneQueueOpen = isMicrophoneQueueOpen;
        $scope.isAssistantOnBoard = isAssistantOnBoard;
        if ($scope.isTeacher) {
            $scope.startSend();
        }
        self.onRoomInfo(arguments);
    });

    $scope.$on('startClass', function () {
        live.startClass(function (retCode) {
            if (retCode < 0) {
                console.log('startClass failed');
            }
        });
    });

    EventListenerService.subscribe('room.onStartClass', function (startTime) {
        console.log('onStartClass: ' + startTime);
        $scope.classStartTime = TimeAdjustService.getLocalTime(startTime);
        console.log('onStartClass: local startTime: ' + $scope.classStartTime);
        $scope.isClassStarted = true;
        EventListenerService.trigger('episode.keynote.loadKeynoteImmediately');
        EventListenerService.trigger('room.stroke.render');
    });

    EventListenerService.subscribe('room.onEndClass', function () {
        console.log('onEndClass');
        $scope.isClassStarted = false;
        $scope.isClassEnded = true;
        $scope.closedLoading = null;
        if ($scope.isTeacher) {
            if ($scope.endClassCallback) {
                $scope.doQuitRoom($scope.endClassCallback);
            } else {
                $scope.backToList();
            }
        } else if($scope.isAssistant || $scope.isAdmin) {
            $scope.backToList();
        } else {
            // Show comment dialog for student
            $scope.showCommentDialog($scope.episode);
        }
    });

    EventListenerService.subscribe('upload.client.log', function (type,log) {
        WebApiService.updlog(type,log);
    });

    $scope.showCommentDialog = function (episode) {
        $scope.dialog.episodeToComment = episode;
    };

    $scope.hideCommentDialog = function () {
        $scope.dialog.episodeToComment = null;
        $scope.backToList();
    };

    $scope.pptZoomClick = function () {
        $('.user-panel-wrapper').toggleClass('display-none');
        $('.episode-ppt-wrapper').toggleClass('bigger');
        if (pptZoom === 'smaller') {
            $('.episode-ppt-zoom').addClass('bigger');
            UAService.logAction('episodeHideUserPanel');
            pptZoom = 'bigger';
        } else {
            $('.episode-ppt-zoom').removeClass('bigger');
            UAService.logAction('episodeShowUserPanel');
            pptZoom = 'smaller';
        }
    };

    $scope.showClosingDialog = function () {
        $scope.dialog.closing = true;
    };

    $scope.hideClosingDialog = function () {
        $scope.dialog.closing = false;
    };

    // Intercept the close operation
    $scope.registerCloseInterceptor(function (source, closeAction) {
        if (!$scope.isClassStarted) {
            console.log('class not started, do close action');
            $scope.doQuitRoom(closeAction);
            return;
        }

        if (source === 'error') {
            console.log('some error occurred, force quit');
            $scope.doQuitRoom(closeAction);
            return;
        }


        if ($scope.isTeacher) {//teacher
            if (source === 'episodeList') {
                var tConfirmMessage = '当前正在上课，确定要返回直播课列表吗？';
                DialogService.confirm({
                    message: tConfirmMessage,
                    okCallback: function () {
                        $scope.doQuitRoom(closeAction);
                    }
                });
                return;
            }

            var endClassConfirmMessage = '离开当前界面将会下课，确定离开吗？';
            if (source === 'close' ) {
                endClassConfirmMessage = '关闭直播课客户端后将会下课，确定关闭吗？';
            } else if (source === 'logout') {
                endClassConfirmMessage = '退出直播课后将会下课，确定退出吗？';
            }
            DialogService.confirm({
                message: endClassConfirmMessage,
                okCallback: function () {
                    $scope.doQuitRoom(closeAction);
                }
            });
        } else {//student or admin
            var confirmMessage = '当前正在上课，确定离开教室吗？';
            if (source === 'close' ) {
                confirmMessage = '当前正在上课，确定关闭客户端吗？';
            } else if (source === 'logout') {
                confirmMessage = '当前正在上课，确定退出吗？';
            }
            DialogService.confirm({
                message: confirmMessage,
                okCallback: function () {
                    $scope.doQuitRoom(closeAction);
                }
            });
        }
    });

    $scope.$on('$destroy', function () {
        EventListenerService.cancel('episode');
        $rootScope.inEpisode = false;
        $scope.dialog = {};
        $scope.deregisterCloseInterceptor();
        if (intervalPromise) {
            $interval.cancel(intervalPromise);
        }
        $timeout.cancel(enteringTimeoutPromise);
        $timeout.cancel(updateUserListTimerPromise);

        var watchedTime = Math.floor((new Date() - $scope.classOpenTime) / 1000);
        if(watchedTime < 0) {
            watchedTime = 0;
        }
        console.log('$scope.classStartTime ' + $scope.classOpenTime + ' ' + watchedTime);

        WebApiService.post({
            path: 'episodes/' + $scope.roomId + '/watch',
            courseId: $scope.episode.courseId
        }, {
            headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
            transformRequest: function (obj) {
                var str = [];
                for (var p in obj) {
                    str.push(encodeURIComponent(p) + '=' + encodeURIComponent(obj[p]));
                }
                return str.join('&');
            },
            data: {
                'is_finished': $scope.isClassEnded,
                'is_live':true,
                'total_length': 0,
                'watched_length':watchedTime
            }
        });
    });
    init.call(this);
};

Episode.prototype = Object.create(Base.prototype);

module.exports = [
    '$scope',
    '$location',
    '$interval',
    '$timeout',
    'WebApiService',
    'TimeAdjustService',
    'DialogService',
    'UserInfoService',
    'UAService',
    '$window',
    'CourseMapService',
    '$rootScope',
    '$http',
    'EventListenerService',    
    Episode
];
