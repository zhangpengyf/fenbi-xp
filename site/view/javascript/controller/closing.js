'use strict';

module.exports =  ['$scope', '$interval',
    function ($scope, $interval) {
        var totalTime = 10;  // 10s
        var startTime = Date.now();
        $scope.secondsLeft = totalTime;

        var promise = $interval(function () {
            var ellapsed = (Date.now() - startTime) / 1000;
            if (ellapsed < 10) {
                $scope.secondsLeft = Math.round(totalTime - ellapsed);
            } else {
                $scope.hideClosingDialog();
                $scope.doEndClass();
            }
        }, 100);

        $scope.$on('$destroy', function () {
            $interval.cancel(promise);
        });

        $scope.cancel = function () {
            $scope.hideClosingDialog();
        };
    }
];
