'use strict';

var nativeUI = require('../engine/nativeUI.js');

module.exports = [
    '$scope',
    'WebApiService',
    'CourseMapService',
    function ($scope, WebApiService, CourseMapService) {
        var indexWidth = 1024;
        var indexHeight = 714;
        nativeUI.sizeTo(indexWidth, indexHeight);
        nativeUI.setTitleBarArea(50, [
            [10, 10, 250, 30], // logo
            [-280, 10, 155, 20], // account dropdown
            [-80, 15, 20, 20], //min
            [-60, 15, 20, 20], //max
            [-40, 15, 20, 20]//close
        ]);
        $scope.setMinMaxDimension(960, 640, -1, -1);
        //refresh course map
        WebApiService.get({
            path: 'courses'
        },{
            trumanNoGlobalErrorHanlder: true
        }).success(function (data) {
            CourseMapService.setCourseMap(data);
        });
    }
];
