'use strict';

var nativeUI = require('../engine/nativeUI');

module.exports =  [
    '$scope',
    'WebApiService',
    'CourseMapService',
    'UAService',
    function (
        $scope,
        WebApiService,
        CourseMapService,
        UAService
    ) {
    var stat = $scope.episode.commentStat;
    if (!stat) {
        stat = {
            count: 0,
            totalStar: 5,
            avgStar: 0,
            avgScore: 0,
            myStar: 0,
            myScore: 0,
        };
    }
    stat.avgStar = Math.round(stat.avgScore * 5 / 10);
    stat.myStar = Math.round(stat.myScore * 5 / 10);
    stat.avgScore = Number(stat.avgScore).toFixed(1);

    $scope.commentStat = stat;

    $scope.openComment = function () {
        var url = WebApiService.server('truman') + '/' +
                CourseMapService.getCourseUrl($scope.episode.courseId) +
                '/episodes/' + $scope.episode.id + '/comments';
        nativeUI.openBrowser(url);
        UAService.logAction('openComment');
    };

    $scope.comment = function () {
        $scope.showCommentDialog($scope.episode);
        UAService.logAction('comment');
    };
}];
