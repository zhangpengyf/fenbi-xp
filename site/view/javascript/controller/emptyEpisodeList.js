'use strict';

module.exports =  ['$scope', 'WebApiService', 'DialogService',
    function ($scope, WebApiService, DialogService) {
        $scope.courses = [];

        WebApiService.get({
            path: 'courses/stat'
        }, {}).success(function (data) {
            $scope.courses = data;
        }).error(function (data, status) {
            if (status !== 0) {
                DialogService.alert('加载失败');
            }
        });

        $scope.openCourse = function (course) {
            var url = WebApiService.server('truman') + '/' + course.prefix;
            $scope.openUrl(url);
        };
    }
];
