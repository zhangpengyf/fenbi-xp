'use strict';

var nativeUI = require('../engine/nativeUI');
//var $ = require('jquery');

module.exports =  [
    '$scope',
    '$location',
    'WebApiService',
    'localStorageService',
    'DialogService',
    'EpisodesService',
    '$timeout',
    function ($scope, $location, WebApiService,localStorageService,DialogService,EpisodesService,$timeout) {

        $scope.classId = '';
        $scope.showpList = false;
        $scope.showGuideImg = false;
        $scope.blurPromise = null;

        var courseHistroy = localStorageService.get('inputHistory');
        try{
            $scope.courseHistory = angular.fromJson(courseHistroy);
        }
        catch(e) {
            $scope.courseHistory = [];
        }

        var addCourseId = function(courseId,courseTitle)
        {
            if($scope.courseHistory === null) {
                $scope.courseHistory = [];
            }

            for(var i = 0; i < $scope.courseHistory.length; i++)
            {
                var course = $scope.courseHistory[i];
                if(course.courseId == courseId) {
                    $scope.courseHistory.splice(i, 1);
                    break;
                }
            }
            $scope.courseHistory.unshift({'courseId':courseId,'courseTitle':courseTitle});

            if($scope.courseHistory.length > 10) {
                $scope.courseHistory.splice($scope.courseHistory.length - 1, 1);
            }

            localStorageService.set('inputHistory',angular.toJson($scope.courseHistory));
        };


        $scope.getClassByCode = function () {
            var words = $scope.classId.split(' ');

            if($scope.classId == 'ifenbi') {
                EpisodesService.setLectureId(undefined);
                $location.path('/episodes');
                return;
            }
            else if($scope.classId.indexOf('replay') >= 0) {
                $location.path('/replay').search({
                    courseId: 1,
                    episodeId: words[1],
                });
                return;
            }
            else if($scope.classId.indexOf('live') >= 0) {
                $location.path('/live').search({
                    courseId: 1,
                    episodeId: words[1],
                });
                return;
            }

            WebApiService.get({
                path: EpisodesService.getLectureUrl('lectures/code/' + $scope.classId ),
                server: 'truman'
            }, {}).success(function (data) {
                if(data.code == 1) {
                    EpisodesService.setLectureId(data.data.id);
                    EpisodesService.setLectureTitle(data.data.title);
                    EpisodesService.setEpisodeList(null);

                    addCourseId($scope.classId,data.data.title);
                    $location.path('/course');
                }
                else{
                    DialogService.alert('在' + EpisodesService.getEpisodeSubject() + '中查找课程失败，请检查输入后重试');
                }
            }).error(function (data, status) {
                if (status !== 0) {
                    DialogService.alert('查找课程失败，请检查输入后重试');
                }
            });
        };

        $scope.openDownloadUrl = function() {
            nativeUI.openBrowser('http://fenbi.com/xingce');
        };

        $scope.openFavorite = function() {
            EpisodesService.setEpisodeList(null);
            EpisodesService.setLectureId(0);
            $location.path('/favorite');
        };

        $scope.itemClick = function($event,$index) {
            $scope.classId = $scope.courseHistory[$index].courseId;
            $scope.getClassByCode();
        };

        $scope.itemRemove = function($event,$index) {
            $scope.courseHistory.splice($index,1);
            localStorageService.set('inputHistory',angular.toJson($scope.courseHistory));
        };

        $scope.showDropList = function(bshow) {
            $scope.showpList = bshow;
            if($scope.courseHistory && $scope.courseHistory.length && $scope.courseHistory.length <= 0 ) {
                $scope.showpList = false;
            }
            $scope.showGuideImg = false;
        };

        $scope.showGuide = function(bshow) {
            $scope.showGuideImg = bshow;
        };

        $scope.delayHideDropList = function() {
            $scope.blurPromise = $timeout(function(){
                $scope.showDropList(false);
            },300);
        };

        $scope.cancelHideDropList = function() {
            $timeout.cancel($scope.blurPromise);
        };

        $scope.$on('$viewContentLoaded', function() {
            $scope.setCurrentPage(0);
            //console.log('enter page ' + $scope.$parent.disableFavor + ' .. ' + $scope.disableFavor);
        });
    }
];
