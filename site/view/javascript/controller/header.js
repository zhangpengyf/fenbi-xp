'use strict';

var nativeUI = require('../engine/nativeUI');

module.exports = [
    '$scope',
    '$location',
    '$window',
    'UAService',
    'EventListenerService',
    'localStorageService',
    'WebApiService',
    'EpisodesService',
    'CourseMapService',
    function (
        $scope,
        $location,
        $window,
        UAService,
        EventListenerService,
        localStorageService,
        WebApiService,
        EpisodesService,
        CourseMapService
    ) {
        $scope.dialog = {};
        $scope.isMaximize = false;
        $scope.subjectList = '';
        $scope.subject = '';
        $scope.prefix = 0;
        $scope.isSubjectSwitch = false;

        var curPath = $location.path();

        var trumanWindowResizeHandler = function (event) {
            var command = parseInt(event.detail);
            $scope.safeApply(function () {
                $scope.isMaximize = (command === 3);
            });
        };

        $window.addEventListener('truman.windowResized', trumanWindowResizeHandler);

        $scope.maximize = function () {
            if ($scope.isMaximize) {
                UAService.logAction('restore');
                nativeUI.restore();
                $scope.isMaximize = false;
            } else {
                UAService.logAction('maximize');
                nativeUI.maximize();
                $scope.isMaximize = true;
            }
        };

        $scope.minimize = function () {
            UAService.logAction('minimize');
            nativeUI.minimize();
        };

        $scope.close = function () {
            UAService.logAction('close');
            $scope.confirmCloseAction('close', function () {
                nativeUI.close();
            });
        };


        $scope.showFeedbackDialog = function () {
            $scope.dialog.feedback = true;
        };

        $scope.hideFeedbackDialog = function () {
            $scope.dialog.feedback = false;
        };

        EventListenerService.subscribe('head.nickname.showNicknameDialog', function () {
            $scope.showNicknameDialog();
        });

        $scope.showNicknameDialog = function () {
            $scope.dialog.nickname = true;
        };

        EventListenerService.subscribe('head.nickname.hideNicknameDialog', function () {
            $scope.hideNicknameDialog();
        });

        $scope.hideNicknameDialog = function () {
            $scope.dialog.nickname = false;
            EventListenerService.trigger('head.nickname.nicknameDialogHide');
        };

        $scope.getImageById = function (id) {
            return  '../images/' + id.prefix + '.png';
        };

        $scope.switchSubject = function ($event,$index) {
            $scope.prefix  = $scope.subjectList[$index].prefix;
            $scope.subject = $event.currentTarget.innerText;

            document.getElementById('popout_11').style.visibility='hidden';

            EpisodesService.setEpisodePrefix($scope.prefix);
            EpisodesService.setEpisodeSubject($scope.subject);
        };

        $scope.showAboutDialog = function () {
            $scope.dialog.about = true;
        };

        $scope.hideAboutDialog = function () {
            $scope.dialog.about = false;
        };

        $scope.$on('$destroy', function () {
            EventListenerService.cancel('head');
            $window.removeEventListener('truman.windowResized', trumanWindowResizeHandler);
        });

        $scope.$on('$locationChangeSuccess', function(event,newurl ,oldurl) {
            console.log('header js route change' + event);

            if(oldurl.indexOf(curPath) > 0){
               $scope.isSubjectSwitch = true;
            }

            if(newurl.indexOf(curPath) > 0){
                $scope.isSubjectSwitch = false;
            }
        });

        var init = function() {
            if($scope.isStudent) {
                $scope.subjectList = CourseMapService.getCourseList();

                $scope.subject = EpisodesService.getEpisodeSubject();
                $scope.prefix = EpisodesService.getEpisodePrefix();

                console.log('prefix ' + $scope.prefix );

                WebApiService.get({
                    path: EpisodesService.getLectureUrl('apps/config'),
                    server: 'truman'
                },{
                    params: {
                        app: EpisodesService.getEpisodePrefix()
                    }
                }).success(function (data) {
                    $scope.showFavorite(data.params.disableFavor);
                }).error(function (data, status) {
                    console.log('get disable fav fail ' + data + status);
                });
            }
            else
            {
                $scope.showFavorite(false);
            }

            nativeUI.isFullscreen(function (retCode, isFullscreen) {
                if (retCode > 0) {
                    $scope.isFullscreen = isFullscreen;
                }
            });
        };

        init();
    }
];
