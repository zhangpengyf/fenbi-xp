'use strict';

var device = require('../engine/device');

module.exports = [
	'$scope',
    '$rootScope',
	'$interval',
    'DialogService',
    'UAService',
    'EventListenerService',
	function ($scope, $rootScope,$interval, DialogService, UAService,EventListenerService) {
        var microphoneTimer = null;
        var detectMuteTimer = null;
        var detectInputTimer = null;
        var context = null;

        $scope.showSettingMic = true;
        $scope.microphoneLevel = 0;
        $scope.recordingDevices = [];
        $scope.selectedDevice = '';
        $scope.outputVolumePercent = 0;
        $scope.outputMuteState = null;
        $scope.inputMuteState = null;

        $scope.videoCapDevices = [];
        $scope.selectedVideoDevice = '';

        var cleanInputTimer = function() {
            if(detectInputTimer !== null) {
                $interval.cancel(detectInputTimer);
            }
        };

        var cleanLevelTimer = function () {
            if (microphoneTimer !== null) {
                $interval.cancel(microphoneTimer);
            }
        };

        var cleanMuteTimer = function () {
            if (detectMuteTimer !== null) {
                $interval.cancel(detectMuteTimer);
            }
        };

       var isInputMute = function () {
            device.isInputMute(function (retCode, isMute) {
                if (retCode >= 0) {
                    $scope.inputMuteState = isMute;
                    $rootScope.$broadcast('isInputMute',isMute );
                } else {
                    //console.log('detect microphone mute state failed');
                }
            });
        };

       var isOutputMute = function () {
            device.isOutputMute(function (retCode, isMute) {
                if (retCode >= 0) {
                    $scope.outputMuteState = isMute;
                    $rootScope.$broadcast('isOutputMute',isMute );
                } else {
                    //console.log('detect bugle mute state failed');
                }
            });
        };

       detectMuteTimer = $interval(function () {
            isInputMute();
            isOutputMute();
        }, 500);

       detectInputTimer = $interval(function () {
            device.getRecordingDevices(function (retCode, recordingDevices) {
                if (retCode >= 0) {
                    if($scope.recordingDevices.length != recordingDevices.length) {
                        if (recordingDevices.length > 0) {
                            $scope.selectedDevice = recordingDevices[0];
                        }
                    }
                    $scope.recordingDevices = recordingDevices;

                    if($scope.recordingDevices.length <= 0) {
                        console.log('recordingDevicesChange');
                        if($scope.isTeacher) {
                            $rootScope.$broadcast('recordingDevicesChange');
                        }
                    }
                } else {
                    console.log('getRecordingDevices failed!');
                }
            });
        }, 2000);

        angular.element(document).ready(function () {
            var videoCanvas = document.getElementById('video-preview');
            context = videoCanvas.getContext('2d');
        });

        device.getVideoCaptureDevices(function (retCode, videoCapDevices) {
            if (retCode >= 0) {
                if($scope.videoCapDevices.length != videoCapDevices.length) {
                    if (videoCapDevices.length > 0) {
                        $scope.selectedVideoDevice = videoCapDevices[0];
                        console.log('set video device ' + $scope.selectedVideoDevice);
                    }
                }
                $scope.videoCapDevices = videoCapDevices;                
              
            } else {
                console.log('getVideoCaptureDevices failed!');
            }
        });

       var getOutputVolume = function () {
            device.getOutputVolume( function (retCode, volumePercent) {
                if (retCode >= 0) {
                    $scope.outputVolumePercent = volumePercent;
                    $scope.$apply();
                }
            });
        };

       $scope.setOutputVolume = function () {
            device.setOutputVolume($scope.outputVolumePercent, function (retCode) {
                if (retCode < 0) {
                    DialogService.alert('设置音量失败');
                }
            });
        };

        $scope.showSettingUi = function(showMic) {
            $scope.showSettingMic = showMic;

            if(showMic) {
                device.closeVideoPreview();
            }
            else
            {
                device.setVideoCaptureDevice($scope.selectedVideoDevice,function(ret){
                    console.log('set video device ' + ret);

                    device.openVideoPreview(function(){
                        console.log('open video device ' + ret);
                    });
                });
            }
        };

       $scope.$on('startMicrophoneTest', function () {
            microphoneTimer = $interval( function () {
                getMicroPhoneInputLevel();
            }, 100);
            getOutputVolume();
        });

       $scope.$on('endMicrophoneTest', function () {
            cleanLevelTimer();
        });

       $scope.$on('dialogPopup', function () {
            UAService.logAction('microphoneSetting');
            getOutputVolume();
        });

        $scope.$on('dialogClose', function () {
            $scope.setOutputVolume();
        });

       $scope.$watch('selectedDevice', function () {
            var selectedDeviceIndex = -1;
            for (var i = 0;i < $scope.recordingDevices; i++) {
                var recordingDevice = $scope.recordingDevices[i];
                if (recordingDevice === $scope.selectedDevice) {
                    selectedDeviceIndex = i;
                    break;
                }
            }
            if (selectedDeviceIndex >= 0) {
                device.setRecordingDevice(selectedDeviceIndex, function(retCode) {
                    if (retCode < 0) {
                        console.log('selectedDevice ' + $scope.selectedDevice + ' failed');
                    }
                });
            }
        });

        $scope.$watch('selectedVideoDevice',function() {
            var selectedDeviceIndex = -1;

            for (var i = 0;i < $scope.videoCapDevices.length; i++) {
                var videoCapDevice = $scope.videoCapDevices[i];
                if (videoCapDevice === $scope.selectedVideoDevice) {
                    selectedDeviceIndex = i;
                    break;
                }
            }

            console.log('selectedVideoDevice ' + selectedDeviceIndex);

            //device.closeVideoPreview();

            if (selectedDeviceIndex >= 0) {                
                device.setVideoCaptureDevice(selectedDeviceIndex, function(retCode) {
                    if (retCode < 0) {
                        console.log('selectedVideoDevice ' + $scope.selectedVideoDevice + ' failed');
                    }
                });
            }
        });

       var getMicroPhoneInputLevel = function () {
            device.getInputLevel(true, function (retCode, level) {
                if (retCode >= 0) {
                    $scope.microphoneLevel = parseInt(level / 9 * 12);
                } else {
                    console.log('getMicroPhoneInputLevel failed');
                }
            });
        };

        EventListenerService.subscribe('room.video.event.42', function (rotation,url) {            
            var imageObj = new window.Image();
            imageObj.onload = function() {
                if(context) {
                    context.drawImage(this, 0, 0, 200, 150);
                }
            };
            imageObj.src = url;
        });

       $scope.$on('$destroy', function () {
            cleanLevelTimer();
            cleanMuteTimer();
            cleanInputTimer();
        });
}];