'use strict';

var nativeUI = require('../engine/nativeUI.js');
var fileApi = require('../engine/fileApi.js');

var $ = require('jquery');
var URL = require('url');

module.exports =  [
    '$scope',
    '$location',
    '$window',
    'localStorageService',
    'DialogService',
    'UAService',
    '$rootScope',
    function ($scope, $location, $window, localStorageService, DialogService, UAService, $rootScope) {
        var UserType = {
            Teacher: 1,
            Student: 2,
            Admin: 3,
            Assistant: 4
        };
        $scope.loginUser = localStorageService.get('userName');
        $scope.isFullscreen = false;
        $scope.isTeacher = truman.userType == UserType.Teacher;
        $scope.isStudent = truman.userType == UserType.Student;
        $scope.isAdmin =  truman.userType == UserType.Admin;
        $scope.isAssistant =  truman.userType == UserType.Assistant;
        $scope.trumanUserType = truman.userType;
        $scope.disableFavor = false;
        $scope.currentPage = 0;

        var actived = localStorageService.get('actived');
        if (!actived) {
            localStorageService.set('actived', 'true');
            UAService.logAction('activeProgram');
        }

        nativeUI.isFullscreen(function (retCode, isFullscreen) {
            if (retCode > 0) {
                $scope.isFullscreen = isFullscreen;
            }
        });

        $(document).keyup(function(e) {
            if (e.keyCode == 27) {//ESC
                if ($scope.isFullscreen) {//exit fullscreen
                    $scope.toggleFullscreen();
                }
            }
        });

        var jumpToLogin = function (loginUrl) {
            $window.location.href = loginUrl;
        };

        $window.addEventListener('truman.externalOpen', function (event) {
            var encodedLaunchArgs = event.detail;
            var href = $window.location.href;
            if (href.lastIndexOf('login.html') >= 0) {
                $window.location.href = 'login.html?redirectUrl=index.html%23'+encodedLaunchArgs;
                return;
            }
            var launchArgs = decodeURIComponent(encodedLaunchArgs);
            var url = $location.url();
            if (url.length > 1) {
                var subUrl = url.slice(1, url.length); //remove leading /
                if (launchArgs == subUrl) {
                    return; // same page no need to jump
                }
            }

            var targetURL = URL.parse(launchArgs);
            var keyValuePairs = targetURL.query.split('&');
            var searchValue = {};
            keyValuePairs.forEach( function(keyValue) {
                var keyValueArray = keyValue.split('=');
                var key = keyValueArray[0];
                var value = keyValueArray[1];
                searchValue[key] = value || '';
            });

            var path = $location.path();
            if (path.lastIndexOf('/live', 0) !== 0 && path.lastIndexOf('/replay', 0) !== 0) {
                $scope.safeApply(function () {
                    $location.path('/' + targetURL.pathname).search(
                        searchValue
                    );
                });
                return;//not in live/replay room, no confirm dialog
            }
            DialogService.confirm({
                message: '确定切换当前直播课?',
                okCallback: function() {
                    $scope.safeApply(function () {
                        $location.path('/' + targetURL.pathname).search(
                            searchValue
                        );
                    });
                }
            });
        });

        $scope.showFavorite = function(bshow) {
            $scope.disableFavor = bshow;
        };

        $scope.setCurrentPage = function(page) {
            $scope.currentPage = page;
        };

        $scope.toggleFullscreen = function () {
            $scope.isFullscreen = !$scope.isFullscreen;
            nativeUI.toggleFullscreen();
            $scope.$broadcast('toggleFullscreen');
            var action = '';
            if ($scope.isFullscreen) {
                action = 'enterFullscreen';
            } else {
                action = 'exitFullscreen';
            }
            UAService.logAction(action);
        };

        $scope.logout = function () {
            $scope.confirmCloseAction('logout', function () {
                fileApi.deleteCookie('', '', function (ret) {
                    if (ret === 1) {
                        $scope.loginUser = null;
                        localStorageService.add('autoLogin', 'false');
                        jumpToLogin('login.html');
                    } else {
                        DialogService.alert('无法登出！');
                    }
                });
            });
        };

        $scope.saveLoginUser = function (account) {
            $scope.loginUser = account;
        };

        $scope.closeInterceptor = null;

        $scope.registerCloseInterceptor = function (interceptor) {
            $scope.closeInterceptor = interceptor;
        };

        $scope.deregisterCloseInterceptor = function () {
            $scope.closeInterceptor = null;
        };

        $scope.confirmCloseAction = function (source, closeAction) {
            if ($scope.closeInterceptor) {
                $scope.closeInterceptor(source, closeAction);
            } else {
                if (closeAction) {
                    closeAction();
                }
            }
        };

        $scope.setMinMaxDimension = function (minWidth, minHeight, maxWidth, maxHeight) {
            nativeUI.setMinWidth(minWidth);
            nativeUI.setMinHeight(minHeight);
            nativeUI.setMaxWidth(maxWidth);
            nativeUI.setMaxHeight(maxHeight);
        };

        $rootScope.inArray = function (value, array, field) {
            if (!(array instanceof Array)) {
                return;
            }
            if (array.length <= 0) {
                return false;
            }
            var i = 0;
            if (field) {
                for (i = 0; i < array.length; i++) {
                    if (array[i][field] == value) {
                        return true;
                    }
                }
            } else {
                for (i = 0; i < array.length; i++) {
                    if (array[i] == value) {
                        return true;
                    }
                }
            }
            return false;
        };
        // Prevent the backspace key from navigating back.
        $(document).unbind('keydown').bind('keydown', function (event) {
            var doPrevent = false;
            if (event.keyCode === 8) {
                var d = event.srcElement || event.target;
                if (
                        (d.tagName.toUpperCase() === 'INPUT' &&
                            (
                                d.type.toUpperCase() === 'TEXT' ||
                                d.type.toUpperCase() === 'PASSWORD'
                            )
                        ) ||
                        d.tagName.toUpperCase() === 'TEXTAREA'
                    ) {
                    doPrevent = d.readOnly || d.disabled;
                }
                else {
                    doPrevent = true;
                }
            }

            if (doPrevent) {
                event.preventDefault();
            }
        });
    }
];
