'use strict';

var nativeUI = require('../engine/nativeUI.js');
var fileApi = require('../engine/fileApi.js');
var URL = require('url');

module.exports = [
    '$scope',
    '$location',
    '$window',
    'localStorageService',
    'WebApiService',
    function ($scope, $location, $window, localStorageService, WebApiService) {
        $scope.userName = localStorageService.get('userName');
        $scope.submitFailed = false;
        $scope.errMessage = '';
        var readInStorage = false;
        var skipStoragePwd = false;

        var loginWidth = 300;
        var loginHeight = 270;
        var EMAIL = /^[a-zA-Z0-9_\-\.]{1,}@[a-zA-Z0-9_\-]{1,}\.[a-zA-Z0-9_\-.]{1,}$/;
        var PHONE = /^1[345789]\d{9}$/;

        var whichUserType = function(userName) {
            if (EMAIL.exec(userName)) {
                return 'email';
            } else if (PHONE.exec(userName)) {
                return 'phone';
            } else {
                return 'unknown';
            }
        };

        nativeUI.sizeTo(loginWidth, loginHeight);

        $scope.setMinMaxDimension(loginWidth, loginHeight, loginWidth, loginHeight);

        nativeUI.setTitleBarArea(50, [
            [24, 26, 223, 28], // logo
            [249, 0, 20, 20], // min
            [270, 0, 20, 20] //max
        ]);

        $scope.encodePassword = function(password) {
            var publicKey = 'ANKi9PWuvDOsagwIVvrPx77mXNV0APmjySsYjB1/GtUTY6cyKNRl2RCTt608m9nYk5VeCG2EAZRQmQNQ' +
                'TyfZkw0Uo+MytAkjj17BXOpY4o6+BToi7rRKfTGl6J60/XBZcGSzN1XVZ80ElSjaGE8Ocg8wbPN18tbmsy761zN5SuIl';
            var encryptedPassword = $window.encrypt(publicKey, password);
            return encryptedPassword;
        };

        $scope.savePassword = localStorageService.get('savePassword');
        if ($scope.savePassword === 'true') {
            if (localStorageService.get('password')) {
                $scope.password = '********';
                readInStorage = true;
            }
        }

        $scope.autoLogin = localStorageService.get('autoLogin');

        var urlObj = URL.parse($window.location.href, true, true);
        $scope.redirectUrl = urlObj.query['redirectUrl'];

        $scope.onKeydown = function(event) {
            if (event.which === 13) {
                $scope.login();
            }
        };

        $scope.switchSavePassword = function(event) {
            var checkbox = event.target;
            if (checkbox.checked) {
                $scope.savePassword = 'true';
            } else {
                $scope.savePassword = 'false';
            }
            localStorageService.add('savePassword', $scope.savePassword);
        };

        $scope.switchAutoLogin = function(event) {
            var checkbox = event.target;
            if (checkbox.checked) {
                $scope.autoLogin = 'true';
            } else {
                $scope.autoLogin = 'false';
            }
            localStorageService.add('autoLogin', $scope.autoLogin);
        };

        $scope.skipStoragePwd = function() {
            skipStoragePwd = true;
        };

        $scope.loginSuccess = function() {
            //we need this callback, because this function is params for cef
            fileApi.flushCookie(function(ret){
                console.log(ret);
            });
            localStorageService.add('userName', $scope.userName);
            if ($scope.redirectUrl) {
                $window.location.href= $scope.redirectUrl;
            } else {
                if(! $scope.isStudent) {
                    $window.location.href='index.html#episodes';
                }
                else{
                    $window.location.href='index.html#enter';
                }
            }
        };

        $scope.loginFailed = function(status) {
            $scope.skipStoragePwd = false;
            $scope.submitFailed = true;
            if (status === 401) {
                $scope.errMessage = '用户名或者密码错误';
            } else if (status === -1) {
                $scope.errMessage = '用户过期， 请重新登录';
            } else if (status === -2) {
                $scope.errMessage = '写cookie失败';
            } else if (status === -3) {
                $scope.errMessage = '账户不能为空';
            } else if (status === -4) {
                $scope.errMessage = '密码不能为空';
            } else if (status === -5) {
                $scope.errMessage = '账户格式错误';
            } else if (status === 0) {
                $scope.errMessage = '请检查网络连接';
            } else {
                $scope.errMessage = '错误代码：'  + status;
            }
            console.log('login failed ' + status + ' ' + $scope.errMessage);
        };



        $scope.login = function ($event) {
            if($event) {
                $event.stopPropagation();
            }            

            if ($scope.userName === undefined || $scope.userName.length === 0) {
                $scope.loginFailed(-3);
                return;
            }
            if (!$scope.password || $scope.password.length === 0) {
                $scope.loginFailed(-4);
                return;
            }
            var encryptedPassword = '';
            if (skipStoragePwd) {
                encryptedPassword = $scope.encodePassword($scope.password);
            } else {
                if ($scope.savePassword === 'true') {
                    encryptedPassword = localStorageService.get('password');
                    if (encryptedPassword === null) {
                        encryptedPassword = $scope.encodePassword($scope.password);
                    }
                } else {
                    if (readInStorage) {
                        encryptedPassword = localStorageService.get('password');
                    } else {
                        encryptedPassword = $scope.encodePassword($scope.password);
                    }
                }
            }
            var type = whichUserType($scope.userName);
            var data = {};
            if (type === 'phone') {
                data = {
                    'phone' : $scope.userName,
                    'password': encryptedPassword,
                    'persistent': 'true'
                };
            } else if (type === 'email') {
                data = {
                    'email' : $scope.userName,
                    'password': encryptedPassword,
                    'persistent': 'true'
                };
            } else {
                $scope.loginFailed(-5);
                return;
            }
            WebApiService.post({
                path: 'users/login',
                server: 'ytk'
            }, {
                headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
                transformRequest: function (obj) {
                    var str = [];
                    for (var p in obj) {
                        str.push(encodeURIComponent(p) + '=' + encodeURIComponent(obj[p]));
                    }
                    return str.join('&');
                },
                data: data
            }).success(function (data) {
                console.log(data);
                if ($scope.savePassword === 'true') {
                    localStorageService.add('password', encryptedPassword);
                }

                $scope.loginSuccess();
            }).error(function (data, status) {
                localStorageService.remove('password');
                $scope.password = '';
                $scope.loginFailed(status);
                console.log(data);
            });
        };

        $scope.requestAccount = function(persistent) {
            WebApiService.get({
                path: 'users/current',
                server: 'ytk'
            }, {
                headers: {
                   'cookie': 'persistent=' + persistent
                }
            }).success(function (data) {
                $scope.userName = data['identity'];
                $scope.loginSuccess();
            }).error(function(data, status) {
                $scope.loginFailed(status);
            });

        };


        $scope.signup = function() {
            nativeUI.openBrowser('http://fenbi.com/signup');
        };

        $scope.retrieve = function() {
            nativeUI.openBrowser('http://fenbi.com/retrieve');
        };

        $scope.oauth = function(service) {
            window.open(WebApiService.server('ytk') + '/oauth/redirect?service=' + service +'&redirect=' + encodeURIComponent('truman-redirect-local.html'), '');
        };
        //alert($scope.autoLogin);
        // Auto login
        if ($scope.autoLogin === 'true') {
            fileApi.getCookie('persistent', function(ret, value) {
                if (ret === 1) {
                    var persistent = value;
                    if (persistent !== '') {
                        $scope.requestAccount(persistent);
                        return;
                    }
                }
            });
        }
    }
];
