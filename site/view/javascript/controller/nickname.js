'use strict';

module.exports =  [
    '$scope',
    '$timeout',
    'WebApiService',
    'CurrentUserInfoService',
    'UserInfoService',
    'EventListenerService',
    '$q',
    function (
        $scope,
        $timeout,
        WebApiService,
        CurrentUserInfoService,
        UserInfoService,
        EventListenerService,
        $q
    ) {
        $scope.nickname = '';
        var deferred = $q.defer();
        CurrentUserInfoService.getNickname().then(function (nickname) {
            if (!nickname) {
                deferred.resolve();
            }
            $scope.nickname = nickname;
        });

        $scope.submit = function () {
            if ($scope.submitting) {
                return;
            }

            if (!$scope.validate()) {
                return;
            }

            $scope.submitting = true;
            var nickname = $scope.nickname;
            WebApiService.post({
                path: 'users/settings',
                server: 'truman'
            }, {
                params: {
                    nickname: nickname
                }
            }).success(function () {
                deferred.promise.then(function () {
                    EventListenerService.trigger('episode.nickname.init', nickname);
                });
                $scope.showSuccessTip('保存成功');
                $timeout(function () {
                    $scope.hideNicknameDialog();
                }, 500);
            }).error(function (data, status) {
                if (status === 400) {
                    $scope.showErrorTip('昵称只能包含中英文和数字以及_和-');
                } else if (status === 403) {
                    $scope.showErrorTip('昵称不可包含敏感信息');
                } else if (status === 409) {
                    $scope.showErrorTip('该昵称已存在');
                } else {
                    $scope.showErrorTip('服务器异常，请稍后再试');
                }
            }).finally(function () {
                $scope.submitting = false;
            });
        };

        $scope.showErrorTip = function (tip) {
            $scope.tip = tip;
            $scope.tipType = 'error';
        };

        $scope.showSuccessTip = function (tip) {
            $scope.tip = tip;
            $scope.tipType = 'success';
        };

        $scope.hideTip = function () {
            $scope.tip = null;
        };

        $scope.validate = function () {
            var nickname = $scope.nickname;

            if (!nickname) {
                $scope.showErrorTip('请输入昵称');
                return false;
            }

            if (nickname.length < 2 || nickname.length > 12) {
                $scope.showErrorTip('昵称请控制在2-12个字符');
                return false;
            }

            var nicknameRegExp = /^[a-zA-Z0-9_\-\u4e00-\u9fa5]{2,12}$/;
            if (!nickname.match(nicknameRegExp)) {
                $scope.showErrorTip('昵称只能包含中英文和数字以及_和-');
                return false;
            }

            $scope.hideTip();
            return true;
        };
    }
];
