'use strict';

var moment = require('moment');
var nativeUI = require('../../engine/nativeUI');
var replay = require('../../engine/replay');
var fileApi = require('../../engine/fileApi.js');

module.exports =  [
    '$scope',
    '$location',
    '$interval',
    'WebApiService',
    'EpisodesService',
    'CourseMapService',
    'DialogService',
    'UAService',
    'EventListenerService',
    'MaterialService',
    function ($scope,
        $location,
        $interval,
        WebApiService,
        EpisodesService,
        CourseMapService,
        DialogService,
        UAService,
        EventListenerService,
        MaterialService){

        var EpisodeStatus = {
            Coming: 0,
            Live: 1,
            Converting: 2,
            Replay: 3,
            Expired: 4
        };
        var PagerConst = {
            Reserve: 2,
            SideReserve: 1,
        };
        var params = $location.search();
        var fetchedTime = 0;

        $scope.episodeFilter = null;
        $scope.dialog = {
            episodeIdToComment: 0,
        };

        $scope.episodeList = [];
        $scope.purchased = false;
        $scope.pager = {};
        $scope.loading = false;

        $scope.currentEpsoide = null;
        //this.$scope = $scope;

        var init = function () {
            if (params.filter) {
                $scope.episodeFilter = params.filter;
            } else {
                $scope.episodeFilter = 'live';
            }
            fetchList();
        };        

        var fetchList = function () {
            $scope.loading = true;
            WebApiService.get({
                path: 'episodes'
            }, {
                params: {
                    filter: $scope.episodeFilter,
                    page: $scope.currentPage,
                    pageSize: 7,
                    userType: $scope.trumanUserType,
                }
            }).success(function (data) {
                var episodeList = data.list;
                EpisodesService.setEpisodeList(episodeList);
                $scope.episodeList = episodeList;
                $scope.preparePager(data.pageInfo);
                $scope.loading = false;
                $scope.purchased = true;
                fetchedTime = Date.now();
            }).error(function (data, status) {
                $scope.loading = false;
                console.log('failed: ' + status + ' data: ' + data.toString());
                if (status === 403) {
                    var clientType = '老师版';

                    if ($scope.isAdmin) {
                        clientType = '管理员版';
                    } else if ($scope.isAssistant) {
                        clientType = '助教版';
                    }
                    DialogService.alert('你没有权限登录' + clientType, function () {
                        $scope.logout();
                    });
                } else if (status === 404) {
                    $scope.purchased = false;
                }
            });
        };

        $scope.preparePager = function (pageInfo) {
            $scope.pager = {
                firstPage: pageInfo.currentPage === 0,
                lastPage: pageInfo.currentPage === pageInfo.totalPage - 1,
                hasHeadSection: pageInfo.currentPage > PagerConst.Reserve,
                hasTailSection: pageInfo.totalPage - pageInfo.currentPage - 1 > PagerConst.Reserve,
                currentPage: pageInfo.currentPage,
                totalPage: pageInfo.totalPage,
            };
            var i = 0;
            if ($scope.pager.hasHeadSection) {
                $scope.pager.headSection = [];
                for (i = 0; i < PagerConst.SideReserve; i++) {
                    $scope.pager.headSection.push({
                        pageNum: i
                    });
                }
            }
            if ($scope.pager.hasTailSection) {
                $scope.pager.tailSection = [];
                for (i = 0; i < PagerConst.SideReserve; i++) {
                    $scope.pager.tailSection.push({
                        pageNum: pageInfo.totalPage - PagerConst.SideReserve + i
                    });
                }
            }
            var startPage = pageInfo.currentPage - PagerConst.Reserve;
            if (startPage < 0) {
                startPage = 0;
            }
            var endPage = pageInfo.currentPage + PagerConst.Reserve;
            if (endPage >= pageInfo.totalPage) {
                endPage = pageInfo.totalPage - 1;
            }
            $scope.pager.currentSection = [];
            for (i = startPage; i <= endPage; i++) {
                $scope.pager.currentSection.push({
                    pageNum: i,
                    currentPage: pageInfo.currentPage === i
                });
            }
            $scope.pager.showHeadEllipsis = startPage > 1;
            $scope.pager.showTailEllipsis = endPage < pageInfo.totalPage - 2;
        };


        $scope.nextPage = function () {
            $scope.setCurrentPage($scope.currentPage+1);
            fetchList();
        };

        $scope.previousPage = function () {
            $scope.setCurrentPage($scope.currentPage-1);
            fetchList();
        };

        $scope.pageTo = function (pageNum) {
            $scope.setCurrentPage(pageNum);
            fetchList();
        };

        $scope.refreshEpisodeList = function () {
            $scope.setCurrentPage(0);
            $scope.episodeList = [];
            $scope.pager = {};
            fetchList();
            UAService.logAction('refreshList');
        };

        $scope.fetchLiveList = function () {
            if ($scope.episodeFilter == 'live') {
                return;
            }
            $location.path('/episodes').search({
                filter: 'live',
            });
        };

        $scope.fetchReplayList = function () {
            if ($scope.episodeFilter == 'replay') {
                return;
            }
            $location.path('/episodes').search({
                filter: 'replay',
            });
        };

        $scope.enterRoom = function (episode) {
            var roomId = parseInt(episode.id);
            var episodeStatus = episode.status;

            if (episodeStatus == EpisodeStatus.Replay) {
                $location.path('/replay').search({
                    courseId: episode.courseId,
                    episodeId: roomId,
                });
            } else if (episodeStatus == EpisodeStatus.Live) {
                $location.path('/live').search({
                    courseId: episode.courseId,
                    episodeId: roomId,
                });
            }
        };

        $scope.episodeFinishedString = function(episode) {
            if (episode.teachChannel === -1) {
                if(episode.offline.uploadStatus === -2) {
                    return '上传中...';
                }
                return '上传';
            }
            return '';
        };

        $scope.episodeUploadClassName = function (episode) {
            var episodeStatus = episode.status;

            if(episode.offline.uploadStatus === -2) {
                return 'converting';
            }
            else if(episode.offline.uploadStatus === -1) {
                return 'replay';
            }

            return '' + episodeStatus;
        };

        $scope.episodeStatusString = function (episode) {
            var episodeStatus = episode.status;

            if (episodeStatus == EpisodeStatus.Coming) {
                return '未开播';
            } else if (episodeStatus == EpisodeStatus.Live) {
                return '进入直播';
            } else if (episodeStatus == EpisodeStatus.Replay) {
                return '看回放';
            } else if (episodeStatus == EpisodeStatus.Converting) {
                return '准备中';
            } else if (episodeStatus == EpisodeStatus.Expired) {
                return '已过期';
            }else {
                return '' + episodeStatus;
            }
        };

        $scope.episodeButtonClassName = function (episode) {
            var episodeStatus = episode.status;
            if (episodeStatus == EpisodeStatus.Coming) {
                return 'not-ready';
            } else if (episodeStatus == EpisodeStatus.Live) {
                return 'live';
            } else if (episodeStatus == EpisodeStatus.Replay) {
                return 'replay';
            } else if (episodeStatus == EpisodeStatus.Converting) {
                return 'converting';
            } else {
                return 'expired';
            }
        };

        $scope.episodeTime = function (episode) {
            var startTime = episode.startTime;
            var endTime = episode.endTime;
            return moment.unix(startTime / 1000).format('MM.DD HH:mm') +
                ' - ' + moment.unix(endTime / 1000).format('HH:mm');
        };

        $scope.showCommentDialog = function (episode) {
            $scope.dialog.episodeToComment = episode;
        };

        $scope.hideCommentDialog = function () {
            $scope.dialog.episodeToComment = null;
        };

        $scope.showUserCountPanel = function (episode) {
            console.log('show user count panel ' + episode.id);
            EpisodesService.setEpisodeId(episode.id);
            $scope.dialog.userCountPanel = true;
        };

        $scope.hideUserCountPanel = function () {
            $scope.dialog.userCountPanel = false;
        };

        $scope.uploadFile = function(epsoide) {

            if($scope.currentEpsoide === null) {
                EventListenerService.subscribe('rec.media.upload.progress',function(current,total){
                    $scope.dealUploadProgress(current,total);
                },$scope);
            }
            /*else if($scope.currentEpsoide.id == epsoide.id) {
                return; // in case upload twice
            }*/

            $scope.currentEpsoide = epsoide;
            epsoide.offline.uploadStatus = -2;

            var url = WebApiService.fullurl({
                path:'v3/offline/episodes/'+ epsoide.id +  '/upload'
            }) ;

            var cookies = '';

            fileApi.getCookie('persistent', function(ret, value) {
                if (ret === 1) {
                    cookies += ('persistent=' + value + ';');

                    fileApi.getCookie('sid', function(ret, value) {
                        if (ret === 1) {
                            cookies += ('sid=' + value + ';');

                            fileApi.getCookie('sess', function(ret, value) {
                                if (ret === 1) {
                                    cookies += ('sess=' + value + ';');

                                    fileApi.getCookie('userid', function(ret, value) {
                                        if (ret === 1) {
                                            cookies += ('userid=' + value + ';');

                                            console.log('uploading ....' + $scope.currentEpsoide.status);

                                            replay.uploadRecFile(epsoide.id,url,cookies);
                                        }
                                    });
                                }
                            });
                        }
                    });
                }
            });
        };

        $scope.dealUploadProgress = function(current,total) {

            console.log('rec.media.upload.progress  ....' + $scope.currentEpsoide.status);

            if(current === -1) {
                //upload finished
                if(total === 1) {
                    $scope.currentEpsoide.offline.uploadStatus = 1;
                    $scope.currentEpsoide.teachChannel = 1; //mark uploaded
                    $scope.$apply();
                }
            }
            $scope.$apply();
        };

        $scope.openUrl = function (url) {
            console.log(url);
            nativeUI.openBrowser(url);
        };

        $scope.openLectureUrl = function (courseId, lectureId) {
            UAService.logAction('belongedLecture');
            var url = WebApiService.server('truman') + '/' +
                    CourseMapService.getCourseUrl(courseId) + '/lectures/' +
                    lectureId + '/class';
            $scope.openUrl(url);
        };

        $scope.downloadMaterial = function (materialId) {
            WebApiService.get({
                path: '/api/v3/materials/' +  materialId + '/path'
            }).success(function(data){
                if (data.code == 1) {
                    MaterialService.downloadMaterials(data.data.urls);
                }
            }).error(function (res, status) {
                console.log('get download url failed ' + res + ' ' + status);
            });

            //var url = WebApiService.server('truman') + '/api/materials/' + materialId;
            //$scope.openUrl(url);
        };

        $scope.openTrumanWeb = function () {
            $scope.openUrl(WebApiService.server('truman'));
            UAService.logAction('openTrumanWeb');
        };

        var statusChangeIntervalPromise = $interval(function () {
            var currentTime = Date.now();
            for (var i = 0; i < $scope.episodeList.length; i++) {
                var episode = $scope.episodeList[i];
                var timeToOpen = episode.enterRoomTimeInterval;
                if (episode.status == EpisodeStatus.Coming &&
                        timeToOpen && currentTime - fetchedTime > timeToOpen) {
                    episode.status = EpisodeStatus.Live;
                }
            }
        }, 1000); // check every 1s

        $scope.$on('$destroy', function () {
            $interval.cancel(statusChangeIntervalPromise);
        });

        init();
    }
];
