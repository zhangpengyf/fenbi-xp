'use strict';

var moment = require('moment');
var nativeUI = require('../../engine/nativeUI');

module.exports =  [
    '$scope',
    '$location',
    '$interval',
    'WebApiService',
    'EpisodesService',
    'CourseMapService',
    'DialogService',
    'UAService',
    '$window',
    'MaterialService',
    function ($scope,
        $location,
        $interval,
        WebApiService,
        EpisodesService,
        CourseMapService,
        DialogService,
        UAService,
        $window,
        MaterialService){
        var EpisodeStatus = {
            Coming: 0,
            Live: 1,
            Converting: 2,
            Replay: 3,
            Expired: 4
        };
        var PagerConst = {
            Reserve: 2,
            SideReserve: 1,
        };

        var fetchedTime = 0;

        $scope.episodeFilter = null;
        $scope.dialog = {
            episodeIdToComment: 0,
        };

        $scope.preparePager = 0;
        $scope.episodeList = [];
        $scope.purchased = false;
        $scope.pager = {};
        $scope.loading = false;
        $scope.newsytle = true;
        var pageSize = 7;

        var init = function () {
            initFetchList();
        };

        var initFetchList = function() {
            var episodeListCache = EpisodesService.getEpisodeList();

            if(episodeListCache !== null) {
                console.log('cached list ...' + EpisodesService.getEpisodeCurPage() + ' ' + EpisodesService.getEpisodePageCount());
                $scope.episodeList = [];
                $scope.pager = {};
                $scope.setCurrentPage( EpisodesService.getEpisodeCurPage());
                updatePager(episodeListCache,EpisodesService.getEpisodePageCount(),false);

                return;
            }

            fetchList();
        };

        var fetchList = function () {
            $scope.loading = true;
            console.log('prefix '  +EpisodesService.getEpisodePrefix());
            WebApiService.get({
                path: EpisodesService.getEpisodePrefix() + '/user/favorites/episodes'
            }, {
                params: {
                    start: $scope.currentPage * pageSize,
                    len: pageSize,
                    cat: 0,
                }
            }).success(function (data) {
                updatePager(data.datas,data.total,true);
            }).error(function (data, status) {
                $scope.loading = false;
                console.log('failed: ' + status + ' data: ' + data.toString());
                if (status === 403) {
                    var clientType = '老师版';

                    if ($scope.isAdmin) {
                        clientType = '管理员版';
                    } else if ($scope.isAssistant) {
                        clientType = '助教版';
                    }
                    DialogService.alert('你没有权限登录' + clientType, function () {
                        $scope.logout();
                    });
                } else if (status === 404) {
                    $scope.purchased = false;
                }
            });
        };

        var updatePager = function(episodeList,pageCount,bCache) {
            $scope.pager.currentPage = $scope.currentPage;
            $scope.pager.totalPage = Math.ceil(pageCount / pageSize);

            if(bCache) {
                EpisodesService.setEpisodeList(angular.copy(episodeList));
                EpisodesService.setEpisodeCurPage($scope.currentPage);
                EpisodesService.setEpisodePageCount(pageCount);

                fetchedTime = Date.now();
            }

            $scope.preparePager( $scope.pager);
            $scope.episodeList = episodeList;
            $scope.loading = false;
            $scope.purchased = true;
        };

        $scope.preparePager = function (pageInfo) {
            $scope.pager = {
                firstPage: pageInfo.currentPage === 0,
                lastPage: pageInfo.currentPage === pageInfo.totalPage - 1,
                hasHeadSection: pageInfo.currentPage > PagerConst.Reserve,
                hasTailSection: pageInfo.totalPage - pageInfo.currentPage - 1 > PagerConst.Reserve,
                currentPage: pageInfo.currentPage,
                totalPage: pageInfo.totalPage,
            };
            var i = 0;
            if ($scope.pager.hasHeadSection) {
                $scope.pager.headSection = [];
                for (i = 0; i < PagerConst.SideReserve; i++) {
                    $scope.pager.headSection.push({
                        pageNum: i
                    });
                }
            }
            if ($scope.pager.hasTailSection) {
                $scope.pager.tailSection = [];
                for (i = 0; i < PagerConst.SideReserve; i++) {
                    $scope.pager.tailSection.push({
                        pageNum: pageInfo.totalPage - PagerConst.SideReserve + i
                    });
                }
            }
            var startPage = pageInfo.currentPage - PagerConst.Reserve;
            if (startPage < 0) {
                startPage = 0;
            }
            var endPage = pageInfo.currentPage + PagerConst.Reserve;
            if (endPage >= pageInfo.totalPage) {
                endPage = pageInfo.totalPage - 1;
            }
            $scope.pager.currentSection = [];
            for (i = startPage; i <= endPage; i++) {
                $scope.pager.currentSection.push({
                    pageNum: i,
                    currentPage: pageInfo.currentPage === i
                });
            }
            $scope.pager.showHeadEllipsis = startPage > 1;
            $scope.pager.showTailEllipsis = endPage < pageInfo.totalPage - 2;
        };


        $scope.nextPage = function () {
            $scope.setCurrentPage($scope.currentPage+1);
            fetchList();
        };

        $scope.previousPage = function () {
            $scope.setCurrentPage($scope.currentPage-1);
            fetchList();
        };

        $scope.pageTo = function (pageNum) {
            $scope.setCurrentPage(pageNum);
            fetchList();
        };

        $scope.refreshEpisodeList = function () {
            $scope.setCurrentPage(0);
            $scope.episodeList = [];
            $scope.pager = {};
            fetchList();
            UAService.logAction('refreshList');
        };

        $scope.fetchLiveList = function () {
            if ($scope.episodeFilter == 'live') {
                return;
            }
            $location.path('/episodes').search({
                filter: 'live',
            });
        };

        $scope.enterRoom = function (episode) {
            EpisodesService.enterRoom(episode);
        };

        $scope.goBack = function() {
            $window.history.back();
        };

        $scope.episodeStatusString = function (episode) {
            return EpisodesService.episodeStatusString(episode);
        };

        $scope.episodeButtonClassName = function (episode) {
            return EpisodesService.episodeButtonClassName(episode);
        };

        $scope.episodeTime = function (episode) {
            var startTime = episode.startTime;
            var endTime = episode.endTime;
            return moment.unix(startTime / 1000).format('MM.DD HH:mm') +
                ' - ' + moment.unix(endTime / 1000).format('HH:mm');
        };

        $scope.episodeWatchHistory = function(episode) {
            return EpisodesService.episodeWatchHistory(episode);
        };

        $scope.showCommentDialog = function (episode) {
            $scope.dialog.episodeToComment = episode;
        };

        $scope.hideCommentDialog = function () {
            $scope.dialog.episodeToComment = null;
        };

        $scope.openUrl = function (url) {
            console.log(url);
            nativeUI.openBrowser(url);
        };

        $scope.openLectureUrl = function (courseId, lectureId) {
            UAService.logAction('belongedLecture');
            var url = WebApiService.server('truman') + '/' +
                    CourseMapService.getCourseUrl(courseId) + '/lectures/' +
                    lectureId + '/class';
            $scope.openUrl(url);
        };

        $scope.downloadMaterial = function (materialId) {
            WebApiService.get({
                path: '/api/v3/materials/' +  materialId + '/path'
            }).success(function(data){
                if (data.code == 1) {
                    MaterialService.downloadMaterials(data.data.urls);
                }
            }).error(function (res, status) {
                console.log('get download url failed ' + res + ' ' + status);
            });

            //var url = WebApiService.server('truman') + '/api/materials/' + materialId;
            //$scope.openUrl(url);
        };

        $scope.openTrumanWeb = function () {
            $scope.openUrl(WebApiService.server('truman'));
            UAService.logAction('openTrumanWeb');
        };

        var statusChangeIntervalPromise = $interval(function () {
            var currentTime = Date.now();
            for (var i = 0; i < $scope.episodeList.length; i++) {
                var episode = $scope.episodeList[i];
                var timeToOpen = episode.enterRoomTimeInterval;
                if (episode.status == EpisodeStatus.Coming &&
                        timeToOpen && currentTime - fetchedTime > timeToOpen) {
                    episode.status = EpisodeStatus.Live;
                }
            }
        }, 1000); // check every 1s

        $scope.$on('$destroy', function () {
            $interval.cancel(statusChangeIntervalPromise);
        });

        init();
    }
];
