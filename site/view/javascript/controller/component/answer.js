'use strict';
var live = require('../../engine/live');

module.exports =  [
    '$scope',    
    function ($scope) {   

        $scope.question = {
            type:$scope.roomInfo.questionInfo.type,
            labels:[]
        };                
       
        $scope.dosubmit = function() {
            var correctOpt = [];
            for(var i = 0; i < $scope.question.labels.length; i++) {
                if($scope.question.labels[i].selected) {                                        
                    correctOpt.push(i);
                }
            } 

            var ans = {
                questionId: $scope.roomInfo.questionInfo.questionId,
                selectedOptions: correctOpt  
            };
            
            if(correctOpt.length > 0 ) {                           
                $scope.setAnswer(ans);
                $scope.hideQuestionPanel();                

                live.myAnswer(JSON.stringify(ans));
                $scope.question.labels = [];
            }            
        };       

        $scope.clickOption = function(opt) {                        
            if($scope.question.type === 1) {
                for(var i = 0; i < $scope.question.labels.length; i++) {
                    if($scope.question.labels[i] != opt){
                        $scope.question.labels[i].selected = false;
                    }
                    else{
                        $scope.question.labels[i].selected = true;
                    }                        
                }
            }
            else{
                opt.selected = !opt.selected;
            }
            
        };

        var init = function(){
            $scope.question['type'] = $scope.roomInfo.questionInfo.type;
            for(var i = 0; i < $scope.roomInfo.questionInfo.optionNum; i++) {
                $scope.question.labels.push(
                    {text:String.fromCharCode('A'.charCodeAt(0) + i),
                    'selected': false
                });
            }
        };
        init();
}];