'use strict';
var live = require('../../engine/live');

module.exports =  [
    '$scope',    
    function ($scope) {        
        $scope.question = {
            type: 1,            
            labels:  [
                {text:'A','selected':true},
                {text:'B','selected':false},
                {text:'C','selected':false},
                {text:'D','selected':false},
            ]
        };
       
        $scope.dosubmit = function() {
            $scope.hideQuestionPanel();

            var correctOpt = [];
            for(var i = 0; i < $scope.question.labels.length; i++) {
                if($scope.question.labels[i].selected) {
                    correctOpt.push(i);
                }
            }

            var quiz = {
                type: parseInt($scope.question.type),
                correctOptions: correctOpt,
                optionNum: $scope.question.labels.length,
            };
            
            live.addQuestion(JSON.stringify(quiz));
        };

        $scope.delOption = function() {
            if($scope.question.labels.length > 2) {
                $scope.question.labels.splice($scope.question.labels.length - 1,1);
            }            
        };

        $scope.addOption = function() {
            if($scope.question.labels.length < 6) {
                var label = {};
                angular.copy($scope.question.labels[$scope.question.labels.length-1],label);     

                label.selected = false;
                label.text  = String.fromCharCode(label.text.charCodeAt(0) + 1);

                $scope.question.labels.push(label);               
            }            
        };

        $scope.clickOption = function(opt) {                        
            if( parseInt($scope.question.type) === 1) {
                for(var i = 0; i < $scope.question.labels.length; i++) {
                    if($scope.question.labels[i] != opt){
                        $scope.question.labels[i].selected = false;
                    }
                    else{
                        $scope.question.labels[i].selected = true;
                    }                        
                }
            }
            else{
                opt.selected = !opt.selected;
            }
            
        };

        $scope.$watch('question.type',function(){            
            if(parseInt($scope.question.type) === 1) {
                 for(var i = 0; i < $scope.question.labels.length; i++) {                   
                    if($scope.question.labels[i].selected) {
                        $scope.question.labels[i].selected = false;
                        //$scope.clickOption($scope.question.label[i]);                    
                        //break;
                    }
                }                
            }            
        });
}];