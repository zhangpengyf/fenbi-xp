'use strict';

module.exports =  [
    '$scope',    
    function ($scope) {
        $scope.labels = [];
        $scope.correctOpts = [];
        $scope.scorekeepers = 0;
        $scope.attendStudents = 0;
        $scope.quizType = 1;
        $scope.correctRate = 0;

        var getOptStatNum = function(idx,answerSummary) {
            for(var i = 0; i < answerSummary.optionStats.length; i++) {
                if(answerSummary.optionStats[i].option == idx) {
                    return answerSummary.optionStats[i].num;
                }
            }
        };

        var option2Text = function(idx) {
            return String.fromCharCode('A'.charCodeAt(0) + idx);
        };

        var correct = function(idx) {                        
            if($scope.roomInfo.questionInfo.status == 2) {                
                return $scope.roomInfo.questionInfo.correctOptions.indexOf(idx);            
            }            
            return 1;
        };

        var myanswer = function(idx) {                        
            if($scope.answer) {
                return $scope.answer.selectedOptions.indexOf(idx);                        
            }
            return -1;            
        };

        var bindQuetion = function(question) {
             $scope.labels = [];
             if(!question) {
                 return;                 
            }                
            
            $scope.quizType = question.type;
            if(question.answerSummary) {
                $scope.scorekeepers = question.answerSummary.scorekeepers;
                $scope.attendStudents = question.answerSummary.attendStudents;

                if(question.answerSummary.attendStudents > 0) {
                    $scope.correctRate = Math.round(100 * $scope.scorekeepers / question.answerSummary.attendStudents);
                }           
            }            

            for(var i = 0; i < question.optionNum; i++) {
                var optNum = 0; 
                var optRate = 0;

                if(question.answerSummary) {
                    optNum = getOptStatNum(i,question.answerSummary);

                    if(question.answerSummary.attendStudents > 0) {
                        optRate = Math.round(optNum / question.answerSummary.attendStudents * 100);
                    }
                }                

                var opt = {
                    text: option2Text(i),
                    correct: correct(i),
                    selected: myanswer(i),
                    attendStudents: optNum,
                    correctRate: optRate
                };

                $scope.labels.push(opt);
            }                       
        };  

        $scope.$on('question.summary', function () {          
            bindQuetion($scope.roomInfo.questionInfo);
        });

        $scope.onClose = function() {
            $scope.removeAnswer();
            $scope.hideQuestionPanel();
        };

        var init = function(){
            for(var i = 0; i < $scope.roomInfo.questionInfo.correctOptions.length; i++) {
               $scope.correctOpts.push(option2Text($scope.roomInfo.questionInfo.correctOptions[i]));
            }
            
            bindQuetion($scope.roomInfo.questionInfo);
        };

        init();
}];