'use strict';
var live = require('../../engine/live');

module.exports =  [
    '$scope',    
    function ($scope) {
        $scope.labels = [];
        $scope.correctOpts = [];
        $scope.scorekeepers = 0;
        $scope.attendStudents = 0;
        $scope.hasEndQuestion = false;
        $scope.quizType = 1;
        $scope.correctRate = 0;

        var getOptStatNum = function(idx,answerSummary) {
            for(var i = 0; i < answerSummary.optionStats.length; i++) {
                if(answerSummary.optionStats[i].option == idx) {
                    return answerSummary.optionStats[i].num;
                }
            }
        };

        var option2Text = function(idx) {
            return String.fromCharCode('A'.charCodeAt(0) + idx);
        };

        var bindQuetion = function(question) {
             $scope.labels = [];
             if(!question || !question.answerSummary ) {
                 return;
             }

             $scope.quizType = question.type;
             $scope.scorekeepers = question.answerSummary.scorekeepers;
             $scope.attendStudents = question.answerSummary.attendStudents;
             

            if(question.answerSummary.attendStudents > 0) {
                $scope.correctRate = Math.round(100 * $scope.scorekeepers / question.answerSummary.attendStudents);
            }                             
            
            for(var i = 0; i < question.optionNum; i++) {
                var optNum = getOptStatNum(i,question.answerSummary);
                var optRate = 0;
                if(question.answerSummary.attendStudents > 0) {
                    optRate = Math.round(100 * optNum / question.answerSummary.attendStudents);
                }

                var opt = {
                    text: option2Text(i),
                    correct: $scope.roomInfo.questionInfo.correctOptions.indexOf(i),
                    attendStudents: optNum,
                    correctRate: optRate
                };

                $scope.labels.push(opt);
            }            
        };
      
        $scope.dosubmit = function() {            
            $scope.hasEndQuestion = true;
            live.endQuestion($scope.roomInfo.questionInfo.questionId);
        };

        $scope.removeQuestion = function() {                        
            live.removeQuestion($scope.roomInfo.questionInfo.questionId);
        };        

        $scope.$on('question.summary', function () {          
            bindQuetion($scope.roomInfo.questionInfo);
        });

        var init = function(){
            console.log('questionInfo change ');            

            for(var i = 0; i < $scope.roomInfo.questionInfo.correctOptions.length; i++) {
               $scope.correctOpts.push(option2Text($scope.roomInfo.questionInfo.correctOptions[i]));
            }
            
            bindQuetion($scope.roomInfo.questionInfo);
        };

        init();
}];