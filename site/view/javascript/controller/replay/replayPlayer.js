'use strict';
var replay = require('../../engine/replay');
var device = require('../../engine/device');
var $ = require('jquery');
var _ = require('underscore');

module.exports = [
    '$scope',
    '$interval',
    'UAService',
    'EpisodesService',
    'WebApiService',
    'KeynoteManageService',
    'EventListenerService',
    function (
        $scope,
        $interval,
        UAService,
        EpisodesService,
        WebApiService,
        KeynoteManageService,
        EventListenerService
    ) {
        var updateTimer = null;
        var enterPoint = false;
        var moveProgress = false;
        var verifyNptTime = 0;
        var firstPageIndex = 0;
        var totalTimeInMilliSeconds = 0;

        $scope.playProgress = 0;
        $scope.pointList = [];
        $scope.totalTimeInSeconds = 0;
        $scope.seekPrevSectionAble= false;
        $scope.seekNextSectionAble = true;
        $scope.isPlaying = false;
        $scope.npt = 0;
        $scope.isContinued = false;



        var init = function () {
            console.log('init replay player , bind key up event');
            $(document).on('keydown', function (event) {
                console.log('keyup ' + event.keyCode);
                if (event.keyCode === 37) {
                    $scope.seekToPrevSection();
                } else if (event.keyCode === 39) {
                    $scope.seekToNextSection();
                }else if(event.keyCode === 32) {
                    $scope.playOrPause();
                }
            });
        };

        var cleanUpdateTimer = function () {
            if (updateTimer !== null) {
                $interval.cancel(updateTimer);
            }
        };

        var parseNptToSecond = function (pageToPointList) {
            return pageToPointList.map(function (value) {
                value.npt = Math.ceil(value.npt / 1000);
                return value;
            });
        };

        var filterByNpt = function (pageToPointList) {
            var stayInterval = 5; //seconds
            var tempPointList = pageToPointList.filter(function (value, index) {
                if (index === pageToPointList.length - 1) {
                    if (Math.abs(pageToPointList[index].npt - $scope.totalTimeInSeconds) <= stayInterval) {
                        return false;
                    }
                } else if (Math.abs(pageToPointList[index].npt - pageToPointList[index + 1].npt) <= stayInterval) {
                    return false;
                }
                return true;
            });
            return tempPointList.filter(function (value, index) {
                if (index === 0) {
                    return true;
                }
                if (tempPointList[index].pageTo === tempPointList[index - 1].pageTo) {
                    return false;
                }
                return true;
            });
        };


        var getMouseRelativeX = function (eventX) {
            return eventX - $('.progress').offset().left;
        };

        var resetBreviaryStatusByPoint = function (point) {
            $scope.breviaryTime = point.npt;
            if ($scope.breviaryPageIndex !== point.pageTo) {
                KeynoteManageService.renderWithHeight(
                    $scope.breviaryPageIndex + 1,
                    $('#breviary-keynote').height(),
                    $('#breviary-keynote').get(0)
                );
            }
            $scope.breviaryPageIndex = point.pageTo;
        };
        var adjustRelativeX = function (relativeX) {
            return relativeX < 0 ? 0 : (relativeX > $('.progress').width() ? $('.progress').width() : relativeX);
        };

        var resetBreviaryStatus = function (relativeX) {
            relativeX = adjustRelativeX(relativeX);
            $scope.breviaryTime = Math.ceil((relativeX / $('.progress').width()) * $scope.totalTimeInSeconds);

            var resetPageIndex = false;
            var breviaryPageIndex = null;
            for (var i = 0; i < $scope.pointList.length; i ++) {
                if ($scope.pointList[i].npt > $scope.breviaryTime ) {
                    if (i === 0) {
                        breviaryPageIndex = firstPageIndex;
                        resetPageIndex = true;
                        break;
                    }
                    breviaryPageIndex = $scope.pointList[i - 1].pageTo;
                    resetPageIndex = true;
                    break;
                }
            }
            if (!resetPageIndex) {
                if (!$scope.pointList.length) {
                    breviaryPageIndex = $scope.pageNumber - 1;
                } else {
                    breviaryPageIndex = $scope.pointList[$scope.pointList.length - 1].pageTo;
                }
            }
            if ($scope.breviaryPageIndex !== breviaryPageIndex) {
                KeynoteManageService.renderWithHeight(
                    breviaryPageIndex + 1,
                    $('#breviary-keynote').height(),
                    $('#breviary-keynote').get(0),
                    function () {
                        $scope.breviaryPageIndex = breviaryPageIndex;
                    }
                );
            }
        };

        var repositionBreviary = function (relativeX) {
            relativeX = adjustRelativeX(relativeX);
            var $breviary = $('.breviary');
            var width = $breviary.width();
            if (!width) {
                setTimeout(repositionBreviary.bind(null, relativeX), 30);
                return 0;
            }
            $breviary.css({left: relativeX});

            if (relativeX < width/2) {
                $breviary.css({'margin-left': 0});
                $scope.breviaryStyle = 'left';
            } else if (relativeX > $('.progress').width() - width/2) {
                $breviary.css({'margin-left': -width});
                $scope.breviaryStyle = 'right';
            } else {
                $breviary.css({'margin-left': -width/2});
                $scope.breviaryStyle = 'middle';
            }
        };

        var resetSeekSectionStatus = function () {
            if ($scope.pointList.length <= 0) {
                $scope.seekPrevSectionAble = false;
                $scope.seekNextSectionAble = false;
            } else if ($scope.npt <= $scope.pointList[0].npt) {
                $scope.seekPrevSectionAble= false;
                $scope.seekNextSectionAble = true;
            } else if ($scope.npt >= $scope.pointList[$scope.pointList.length -1].npt) {
                $scope.seekPrevSectionAble= true;
                $scope.seekNextSectionAble = false;
            } else {
                $scope.seekPrevSectionAble= true;
                $scope.seekNextSectionAble = true;
            }
        };

        var updatePlayStatus = function () {
            if (!$scope.isPlaying) {
                return;
            }
            replay.getPlayProgress(function (retCode, millSeconds) {
                if (retCode === 0) {
                    verifyNptTime ++;
                    var seconds = Math.ceil(millSeconds / 1000);
                    if (verifyNptTime > 5 ||
                        seconds - $scope.npt <= 5) {
                        $scope.npt = seconds > $scope.totalTimeInSeconds? $scope.totalTimeInSeconds : seconds;
                        verifyNptTime = 0;
                    }
                } else {
                    console.log('get play status failed');
                }
            });
        };

        var seekByNpt = function (npt) {
            cleanUpdateTimer();
            $scope.npt = npt;
            seekTo(npt);
        };

        var seekByProgress = function (playProgress) {
            cleanUpdateTimer();
            var seekedSec = playProgress * $scope.totalTimeInSeconds;
            $scope.npt = seekedSec;
            seekTo(seekedSec);
        };

        var seekTo = function (seekedSec) {
            var seekedMilliSecond = parseInt(Math.abs(seekedSec * 1000 - totalTimeInMilliSeconds) >= 1000 ? seekedSec * 1000 : totalTimeInMilliSeconds);
            EventListenerService.trigger('replay.stroke.clearCurrentBuffer');
            $scope.questionMask = false;

            replay.seekMedia(seekedMilliSecond, function (retCode) {
                if (retCode >= 0) {
                    UAService.logAction('seekTo' + seekedMilliSecond);                    
                }
            });
        };
    
        var onMouseup = function () {
            $scope.safeApply(function () {
                seekByProgress($scope.playProgress);
            });
            moveProgress = false;
            $scope.breviaryVisible = false;
            $(document).off('mousemove');
            $(document).off('mouseup');
        };

        var onMousemove = function (event) {
            var width = $('.progress').width();
            var relativeX = getMouseRelativeX(event.clientX);
            var progress = relativeX / width;
            $scope.safeApply(function () {
                $scope.playProgress = progress > 0 ? (progress < 1 ? progress : 1) : 0;
                renderBreviary(event);
            });
        };

        var renderBreviaryByPoint = function (event, point) {
            var relativeX = getMouseRelativeX(event.clientX);
            resetBreviaryStatusByPoint(point);
            repositionBreviary(relativeX);
        };

        var renderBreviary = function (event) {
            var relativeX = getMouseRelativeX(event.clientX);
            resetBreviaryStatus(relativeX);
            repositionBreviary(relativeX);
        };

        $scope.beginSeek = function () {
            cleanUpdateTimer();
            moveProgress = true;
            $(document).on('mousemove', onMousemove);
            $(document).on('mouseup', onMouseup);
        };

        $scope.$watch('npt', function () {
            $scope.playProgress = ($scope.npt / $scope.totalTimeInSeconds) || 0;
            resetSeekSectionStatus();
        });
        $scope.$on('setFirstPageIndex', function (event, firstPageIndex) {
            firstPageIndex = firstPageIndex;
        });

        EventListenerService.subscribe('room.onEndClass', function () {
            $scope.stopMedia();
        });

        EventListenerService.subscribe('replay.onMediaInfo', function (lengthInSeconds, pageToPointList) {
            $scope.pointList = filterByNpt(parseNptToSecond(pageToPointList));
            totalTimeInMilliSeconds = lengthInSeconds;
            $scope.totalTimeInSeconds = Math.ceil(lengthInSeconds / 1000);
            // for trigger onkeynoteinfo
            $scope.playMedia();
        });

        EventListenerService.subscribe('room.keynote.loaded', function () {
            $scope.playMedia();
        });

        EventListenerService.subscribe('room.onRoomInfo', function(){
            if (!$scope.isContinued) {
                $scope.isContinued = true;
                var npt = EpisodesService.getEpisodeWatchedPercent($scope.roomId) * $scope.totalTimeInSeconds;
                $scope.npt = npt;
                seekTo(npt);
            }
        });


        $scope.onPointMouseenter = function (event, point) {
            enterPoint = true;
            event.stopPropagation();
            renderBreviaryByPoint(event, point);
            $scope.breviaryVisible = true;
        };

        $scope.onPointMouseleave = function () {
            enterPoint = false;
        };


        $scope.onSliderClick = function (event) {
            event.stopPropagation();
        };

        $scope.onProgressClick = function (event) {
            var relativeX = getMouseRelativeX(event.clientX);
            seekByProgress(relativeX / $('.progress').width());
        };

        $scope.onSyncMedia = function () {
            if ($scope.isPlaying) {
                cleanUpdateTimer();
                updateTimer = $interval(function () {
                    updatePlayStatus();
                }, 300);
            }
        };

        $scope.onPointClick = function (event, npt) {
            event.stopPropagation();
            seekByNpt(npt);
        };

        $scope.seekToPrevSection = function () {
            if (!$scope.seekPrevSectionAble) {
                return;
            }

            for (var i = 1; i < $scope.pointList.length; i++) {
                if ($scope.pointList[i].npt > $scope.npt) {
                    if (i <= 1) {
                        seekByNpt(0);
                    } else {
                        seekByNpt($scope.pointList[i - 2].npt);
                    }
                    return;
                }
            }
            if ($scope.pointList.length > 1) {
                seekByNpt($scope.pointList[$scope.pointList.length - 2].npt);
            } else {
                seekByNpt(0);
            }
            UAService.logAction('seekToPrevSection');
        };

        $scope.seekToNextSection = function () {
            if (!$scope.seekNextSectionAble) {
                return;
            }
            for (var i = 0; i < $scope.pointList.length; i++) {
                if ($scope.pointList[i].npt > $scope.npt) {
                    seekByNpt($scope.pointList[i].npt);
                    break;
                }
            }
            UAService.logAction('seekToNextSection');
        };

        $scope.setVolume = function () {
            device.setOutputVolume($scope.volumePercent, function (retCode) {
                if (retCode < 0) {
                    console.log('setOutputVolume failed');
                }
            });
            UAService.logAction('changeVolume');
        };



        $scope.stopMedia = function () {
            replay.stopMedia(function (retCode) {
                if (retCode < 0) {
                    console.log('stopMedia failed');
                } else {
                    $scope.isPlaying = false;
                }
            });
        };

        $scope.playMedia = function () {
            replay.playMedia(function (retCode) {
                if (retCode < 0) {
                    console.log('can not play');
                    return;
                } else {
                    console.log('replay.playMedia');
                    $scope.isPlaying = true;
                }
            });
        };

        $scope.pauseMedia = function () {
            replay.pauseMedia(function (retCode) {
                if (retCode < 0) {
                    console.log('pauseMedia failed');
                } else {
                    console.log('replay.pauseMedia');
                    $scope.isPlaying = false;
                }
            });
        };


        $scope.playOrPause = function () {
            if ($scope.isPlaying) {
                $scope.pauseMedia();
            } else {
                $scope.playMedia();
            }
            UAService.logAction('playOrPause');
        };

        $scope.onProgressMouseenter = function (event) {
            renderBreviary(event);
            $scope.breviaryVisible = true;
        };

        $scope.onProgressMousemove = _.debounce(function (event) {
            if (!enterPoint) {
                renderBreviary(event);
            }
        }, 5);

        $scope.onProgressMouseleave = function () {
            if (!moveProgress) {
                $scope.breviaryVisible = false;
            }
        };

        $scope.formatTimeToStr = function (timeInSeconds) {
            var minutes = Math.floor(timeInSeconds / 60);
            var seconds = Math.floor(timeInSeconds % 60);
            var timeStr = '';
            timeStr += minutes + ':';
            if (seconds < 10) {
                seconds = '0' + seconds;
            }
            timeStr += seconds;
            return timeStr;
        };

        $scope.$on('$destroy', function () {
            cleanUpdateTimer();

            console.log('destroy player ' + $scope.roomId + ' ntp' + $scope.npt + ' total ' + $scope.totalTimeInSeconds + ' cur ' + $scope.npt / $scope.totalTimeInSeconds);

            $(document).off('keyup');
        });
        init();
    }
];