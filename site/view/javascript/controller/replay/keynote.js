'use strict';
var Base = require('../base/keynote');
var replay = require('../../engine/replay');
//var nativeUI = require('../../engine/nativeUI');

var Keynote = function (
    $scope,
    $window,
    WebApiService,
    KeynoteService,
    $interval,
    KeynoteManageService,
    $rootScope,
    UAService,
    EventListenerService,
    $timeout,
    MaterialService
) {
    Base.call(
        this,
        $scope,
        KeynoteService,
        $interval,
        $window,
        KeynoteManageService,
        WebApiService,
        EventListenerService,
        $timeout,
        MaterialService
    );
    var that = this;    

    EventListenerService.subscribe('replay.keynote.onKeynoteInfo', function (keynoteId, totalPageNum, currentPageIndex) {
        this.onKeynoteInfo(keynoteId, totalPageNum, currentPageIndex);
        if (!$scope.keynote) {
            $rootScope.$broadcast('setFirstPageIndex', currentPageIndex);
        }
        var loadkeynote = false;
        
        if (!$scope.keynote || $scope.keynote.totalPageNum === 0) {
            loadkeynote = true;
        }
        
        console.log('replay.onKeynoteInfo: is loaded ' + keynoteId + ' currentPageIndex ' + currentPageIndex);

        $scope.keynote = KeynoteService.createKeynote(keynoteId, totalPageNum, currentPageIndex);
        $scope.safeApply();
        if (loadkeynote) {
            $scope.hasStartLoadKeynote = true;
            $scope.loadKeynote();
            console.log('replay.onKeynoteInfo hasStartLoadKeynote' + $scope.hasStartLoadKeynote);
        }
    }, this);

    $scope.onPageTo = function (pageIndex) {
        $scope.keynote.currentPageIndex = pageIndex;      
    };

    $scope.fullScreen = function () {
        that.fullScreen();
        UAService.logAction('replayFullScreen');
    };

    $scope.setPlaySpeed = function () {
        //EventListenerService.trigger('replay.keynote.setplayspeed');

        replay.setPlaySpeed(parseFloat($scope.playSpeed), function (retCode) {
            if (retCode < 0) {
                console.log('changePlaySpeed failed');
            }
        });
        console.log('changePlaySpeed' + $scope.playSpeed);
        //UAService.logAction('changePlaySpeed' + $scope.playSpeed);
    };

    $scope.downloadMaterial = function (materialId) {
        WebApiService.get({
            path: '/api/v3/materials/' +  materialId + '/path'
        }).success(function(data){
            if (data.code == 1) {
                MaterialService.downloadMaterials(data.data.urls);
            }
        }).error(function (res, status) {
            /*WebApiService.updlog({
                'statusCode': -3,
                'dataName':materialId,
            });*/
            console.log('get download url failed ' + res + ' ' + status);
        });        
    };

    EventListenerService.subscribe('av.network.status', function (singal) {
        if(singal > 0) {
            console.log('replay: av.network.status ' + singal);
        }
    });
};
Keynote.prototype = Object.create(Base.prototype);
module.exports = [
    '$scope',
    '$window',
    'WebApiService',
    'KeynoteService',
    '$interval',
    'KeynoteManageService',
    '$rootScope',
    'UAService',
    'EventListenerService',
    '$timeout',
    'MaterialService',
    Keynote
];