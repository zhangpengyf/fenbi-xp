'use strict';

var Base = require('../base/microphoneQueue');

var MicrophoneQueue = function (
    $scope,
    $interval,
    DialogService,
    UserInfoService,
    EventListenerService
) {
    Base.call(
        this,
        $scope,
        $interval,
        UserInfoService,
        EventListenerService
    );

    $scope.engineType = 1;

    $scope.onMicrophoneApplied = function (userId, roomId,nickName) {
        this.onMicrophoneApplied(userId, roomId,nickName);
    }.bind(this);
    $scope.onMicrophoneApproved = function (userId, roomId,channelId,userType,nickName,status) {
        this.onMicrophoneApproved(userId, roomId,channelId,userType,nickName,status);
    }.bind(this);

    $scope.onMicrophoneCanceled = function (userId, roomId) {
        this.onMicrophoneCanceled(userId, roomId);
    }.bind(this);

    $scope.onUserBanned = function (userId) {
        this.onUserBanned(userId);
    }.bind(this);

    $scope.onUserUnBanned = function (userId) {
        this.onUserUnBanned(userId);
    }.bind(this);
};
MicrophoneQueue.prototype = Object.create(Base.prototype);
module.exports = [
    '$scope',
    '$interval',
    'DialogService',
    'UserInfoService',
    'EventListenerService',
    MicrophoneQueue
];
