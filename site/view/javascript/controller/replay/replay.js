'use strict';

var Base = require('../base/room');
var $ = require('jquery');
var replay = require('../../engine/replay');
var device = require('../../engine/device');

var Replay = function (
    $scope,
    $location,
    $interval,
    $timeout,
    $window,
    WebApiService,
    UserInfoService,
    DialogService,
    UAService,
    CourseMapService,
    $rootScope,
    $http,
    EventListenerService,
    EpisodesService
) {
    Base.call(
        this,
        $scope,
        WebApiService,
        DialogService,
        $window,
        CourseMapService,
        $timeout,
        EventListenerService,
        UserInfoService,
        $location
    );
    var enteringTimeoutPromise = null;
    var pptZoom = 'smaller';
    var self = this;
    $rootScope.inReplay = true;
    UserInfoService.getPool('chat').pause();
    UserInfoService.getPool('preFetch').pause();

    $scope.playSpeed = 1.0;
    $scope.volumePercent = 0;
    $scope.dialog = {};
    $scope.users = {};
    $scope.dataLoaded = false;
    $scope.isAssistantOnBoard = false;

    var init = function () {
        if (!$scope.episode) {
            WebApiService.get({
                path: 'users/current'
            }, {
                params: {
                    nickname: true
                }
            }).success(function (data) {
                $scope.user = data;
                self.onDataLoaded();
            }).error(function (data, status) {
                 if (status !== 0) {
                    DialogService.alert('用户信息加载失败', $scope.backToList);
                } else {
                    DialogService.alert('进入教室过程中，网络连接出现问题', $scope.backToList, '退出教室');
                }
            });
        }
    };

    this.onDataLoaded = function () {
        if ($scope.episode && $scope.user && $scope.sessionCookie ) {
            $scope.user.name = $scope.user.nickname;
            $scope.dataLoaded = true;
            $scope.episode.ticket.cookie = $scope.sessionCookie;
            $scope.episode.ticket.host = '';
            this.enterRoom();
        }
    };


    var closeMedia = function (callback) {
        $scope.dialog.closedLoading = true;
        replay.closeMedia(function () {
            if (callback) {
                callback();
            }
            $scope.dialog.closedLoading = false;
        });
    };

    this.enterRoom = function (errorMessage) {
        if ($scope.dialog.entering) {
            return;
        }
        this.baseEnterRoom($scope.episode);
        
        $scope.title = $scope.episode.title;
        replay.openMedia($scope.episode.ticket, function (retCode) {
            errorMessage = errorMessage || '回放数据加载失败';
            if (retCode < 0) {
                console.log('enterRoom failed');
                setTimeout(function () {
                    DialogService.alert(errorMessage, $scope.backToList, '退出教室');
                }, 0);
            } else {
                this.enterTimeout(errorMessage);
            }
        }.bind(this));
    };

    $scope.backToList = function () {
        console.log('back to replay list from replay room ' );
        var replayPlayerScope = angular.element(document.getElementById('replay-player')).scope();
        EpisodesService.setEpisodeWatcherHistory(replayPlayerScope.roomId, replayPlayerScope.npt / replayPlayerScope.totalTimeInSeconds);

        WebApiService.post({
            path: 'episodes/' + replayPlayerScope.roomId + '/watch',
            courseId: replayPlayerScope.episode.courseId
        }, {
            headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
            transformRequest: function (obj) {
                var str = [];
                for (var p in obj) {
                    str.push(encodeURIComponent(p) + '=' + encodeURIComponent(obj[p]));
                }
                return str.join('&'); 
            },
            data: {
                'is_finished': (replayPlayerScope.npt + 2 > replayPlayerScope.totalTimeInSeconds),
                'is_live':false,
                'total_length': replayPlayerScope.totalTimeInSeconds,
                'watched_length':replayPlayerScope.npt
            }
        });
        $scope.confirmCloseAction('', function () {
            $scope.dialog = {}; // clear all the dialog

            if($scope.isStudent) {
                //$location.path('/course');
                $window.history.back();
            }
            else{
                $location.path('episodes').search({
                 filter: 'replay',
                });
            }
        });
        UAService.logAction('backToReplayList');
    };

    EventListenerService.subscribe('replay.onMediaInfo', function (lengthInSeconds, pageToPointList) {
        this.clearEnterTimeout();
        console.log('replay.mediaInfo, lengthInSeconds is ' + lengthInSeconds + ' keynote ' + $scope.hasStartLoadKeynote + pageToPointList);
        $scope.dialog.entering = false;
        device.getOutputVolume(function (retCode, volumePercent) {
            if (retCode >= 0) {
                if (volumePercent <= 1.0) {
                    $scope.safeApply(function () {
                        $scope.volumePercent = volumePercent;
                    });
                }
            } else {
                console.log('get volume failed');
            }
        });
    }, self);

    $scope.switchMicrophoneQueue = function (isOpen) {
        $scope.isMicrophoneQueueOpen = isOpen;
    };

    $scope.pptZoomClick = function () {
        $('.user-panel-wrapper').toggleClass('display-none');
        $('.replay-ppt-wrapper').toggleClass('bigger');
        if (pptZoom === 'smaller') {
            $('.replay-ppt-zoom').addClass('bigger');
            UAService.logAction('replayHideUserPanel');
            pptZoom = 'bigger';
        } else {
            $('.replay-ppt-zoom').removeClass('bigger');
            UAService.logAction('replayShowUserPanel');
            pptZoom = 'smaller';
        }
        UAService.logAction('hideUserPanel');
    };

    EventListenerService.subscribe('room.onRoomInfo', function (teacherList,
        studentList,
        isMicrophoneQueueOpen,
        microphoneList,
        speakingUserList,
        userCount,
        startTime, // is String in ms
        latestMessageId,
        latestStrokeId,
        isAssistantOnBoard) {
        console.log('replay.onRoomInfo');
        $scope.isMicrophoneQueueOpen = isMicrophoneQueueOpen;
        $scope.isAssistantOnBoard = isAssistantOnBoard;
        self.onRoomInfo(arguments);
    });

    var replayUrl = function(url) {
        var lectureId = EpisodesService.getLectureId();

        if(lectureId === undefined ) {
            var params = $location.search();
            if(!params.hasOwnProperty('lectureId')) {
                return url;
            }
            lectureId = params['lectureId'];
        }
        return 'replay/lectures/' + lectureId + '/' + url;
    };

    /*var replayMetaDownload = function (successCb, errorCb) {
        WebApiService.get({
            path: replayUrl('episodes/' + $scope.roomId + '/downloads/meta'),
            courseId: $scope.courseId
        }).success(function (data) {
            if (successCb) {
                successCb(data);
            }
        }).error(function (res, status) {
            if (errorCb) {
                errorCb(res, status);
            }
        });
    };

    var replayDataDownload = function (type, chunkId, successCb, errorCb) {
        console.log('download type ' + type + ' chunkId ' + chunkId);
        WebApiService.get({
            path: replayUrl('episodes/' + $scope.roomId + '/downloads/media'),
            courseId: $scope.courseId
        }, {
            params: {
                type: type,
                chunkId: chunkId,
                encode: 'base64',
                timeout:30000
            }
        }).success(function (data) {
            if (successCb) {
                successCb(data);
            }
        }).error(function (res, status) {
            if (errorCb) {
                errorCb(res, status);
            }
        });
    };*/

    
    var base64Encode = function(input) {
        var keyStr =
            'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=';
        var output = '';
        var chr1, chr2, chr3, enc1, enc2, enc3, enc4;
        var i = 0;

        while (i < input.length) {
            chr1 = input[i++];
            chr2 = i < input.length ? input[i++] : Number.NaN;
            chr3 = i < input.length ? input[i++] : Number.NaN;

            enc1 = chr1 >> 2;
            enc2 = ((chr1 & 3) << 4) | (chr2 >> 4);
            enc3 = ((chr2 & 15) << 2) | (chr3 >> 6);
            enc4 = chr3 & 63;

            if (isNaN(chr2)) {
                enc3 = enc4 = 64;
            } else if (isNaN(chr3)) {
                enc4 = 64;
            }
            output += keyStr.charAt(enc1) + keyStr.charAt(enc2) +
                keyStr.charAt(enc3) + keyStr.charAt(enc4);
        }
        return output;
    };

    var replayMetaDownload = function (successCb, errorCb) {
        console.log('v3/' + replayUrl('episodes/' + $scope.roomId + '/metapath'));
        WebApiService.get({
            path: 'v3/' + replayUrl('episodes/' + $scope.roomId + '/metapath'),
            courseId: $scope.courseId
        }).success(function (data) {
            if (data.code == 1) {
                if (data.data.urls && data.data.urls instanceof Array) {
                    WebApiService.getUrls(data.data.urls.map(function(url){
                        return {
                            path: url,
                            server: 'external'
                        };
                    }), {
                        responseType: 'arraybuffer',
                        __from: 'room',
                        transformResponse: function (data) {
                            return base64Encode(new window.Uint8Array(data));
                        }
                    }).then(function(data){
                        if (successCb) {
                            successCb(data);
                        }
                    }, function(res, status) {
                        if (errorCb) {
                            errorCb(res, status);
                        }   
                    });
                } else {
                    WebApiService.getUrl({
                            path: data.data.url,
                            server: 'external'
                    }, {
                        responseType: 'arraybuffer',
                        __from: 'room',
                        transformResponse: function (data) {
                            return base64Encode(new window.Uint8Array(data));
                        }
                    }).success(function(data){
                        if (successCb) {
                            successCb(data);
                        }
                    }).error(function(res, status) {
                        if (errorCb) {
                            errorCb(res, status);
                        }   
                    });
                }
            }
        }).error(function (res, status) {
            if (errorCb) {
                errorCb(res, status);
            }
        });
    };

    var replayDataDownload = function (type,chunkId,successCb, errorCb) {
        WebApiService.get({
            path: 'v3/' + replayUrl('episodes/' + $scope.roomId + '/mediapath'),
            courseId: $scope.courseId
        },{
            params: {
                type: type,
                'chunk_ids': chunkId,                
                //encode: 'base64',
                timeout:30000
            }
        }).success(function (data) {
            if (data.code == 1) {
                if (data.datas[0].urls && data.datas[0].urls instanceof Array) {
                    WebApiService.getUrls(data.datas[0].urls.map(function(url){
                        return {
                            path: url,
                            server: 'external'
                        };
                    }), {
                        responseType: 'arraybuffer',
                        __from: 'room',
                        transformResponse: function (data) {
                            return base64Encode(new window.Uint8Array(data));
                        }
                    }).then(function(data){
                        if (successCb) {
                            successCb(data);
                        }
                    }, function(res, status) {
                        if (errorCb) {
                            errorCb(res, status);
                        } 
                        console.log('replayDataDownload failed ');  
                    });
                } else {
                    WebApiService.getUrl({
                            path: data.datas[0].url,
                            server: 'external'
                    }, {
                        responseType: 'arraybuffer',
                        __from: 'room',
                        transformResponse: function (data) {
                            return base64Encode(new window.Uint8Array(data));
                        }
                    }).success(function(data){
                        if (successCb) {
                            successCb(data);
                        }
                    }).error(function(res, status) {
                        if (errorCb) {
                            errorCb(res, status);
                        } 
                        console.log('replayDataDownload failed ');  
                    });
                }
            }
        }).error(function (res, status) {
            if (errorCb) {
                errorCb(res, status);
            }
        });
    };

    var reconnetCount = 0;
    var showReconnectDialog = function () {
        $scope.dialog.entering = true;
        $scope.dialog.reconnecting = true;
        reconnetCount++;
        return reconnetCount;
    };

    var hideReconnectDialog = function () {
        $scope.dialog.entering = false;
        $scope.dialog.reconnecting = false;
        reconnetCount = 0;
    };

    var RECONNECT_LIMIT = 20;
    var kickoutHandle = null;
    var hideKickoutDialog = function () {
        if (kickoutHandle) {
            kickoutHandle.cancel();
        }
    };
    EventListenerService.subscribe('replay.storageData.get', function (key) {
        var fragment = key.split(':');
        var _download = null;

        var rtpType = 'rtp';
        if(fragment[0] === 'replay_rtp_new_chunk' ) {
            rtpType = 'rtp_new';
        }
        else if(fragment[0] === 'replay_video_chunk') {
            rtpType = 'video';
        }

        console.log('replay.storageData.get ' + key);

        if (fragment[0] === 'replay_media_info') {
            _download = function () {
                replayMetaDownload(function (data) {
                    console.log('replay setStorageData of ' + key);
                    replay.setStorageData(key, data, 0, function () {
                        console.log('replay setStorageData of ' + key + ' failed');
                    });
                }, function (res, status) {
                    if (status === 404 ||
                        status === 403 ||
                        status === 409) {
                        replay.setStorageData(key, '', status, function () {
                            console.log('replay setStorageData of ' + key + ' failed');
                        });
                    } else if(status === 0) {
                        $timeout(function () {
                            _download();
                        }, 1000);
                    } else {
                        _download();
                    }
                });
            };
        } else if (fragment[0] === 'replay_cmd_chunk') {
            _download = function () {
                replayDataDownload('cmd', fragment[2], function (data) {
                    hideReconnectDialog();
                    hideKickoutDialog();
                    console.log('replay setStorageData of ' + key );

                    replay.setStorageData(key, data, 0, function () {
                        console.log('replay setStorageData of ' + key + ' failed');
                    });
                }, function (res, status) {
                    if (status === 0) {
                        var reconnectCount = showReconnectDialog();
                        if (reconnectCount >= RECONNECT_LIMIT && $scope.inReplay) {
                            kickoutHandle = DialogService.alert(
                                '网络连接有点问题，无法连接服务器',
                                function () {
                                    $scope.backToList(true);  // force quit
                                    $scope.safeApply();
                                },
                                '退出教室'
                            );
                        }
                    } else {
                        hideReconnectDialog();
                    }
                    if (status === 400 ||
                        status === 403 ||
                        status === 404) {
                        replay.setStorageData(key, '', status, function () {
                            console.log('replay setStorageData of ' + key + ' failed');
                        });
                    } else if (status === 0) {
                        $timeout(function () {
                            _download();
                        }, 1000);
                    } else {
                        _download();
                    }
                });
            };
        } else if (fragment[0].indexOf('replay_rtp') >= 0) {
            _download = function (rtpType) {
                replayDataDownload(rtpType, fragment[2], function (data) {
                    hideReconnectDialog();
                    hideKickoutDialog();
                    console.log('replay setStorageData of ' + key );
                    replay.setStorageData(key, data, 0, function () {
                        console.log('replay setStorageData of ' + key + ' failed');
                    });
                }, function (res, status) {
                    if (status === 0) {
                        var reconnectCount = showReconnectDialog();
                        if (reconnectCount >= RECONNECT_LIMIT && $scope.inReplay) {
                            kickoutHandle = DialogService.alert(
                                '网络连接有点问题，无法连接服务器',
                                function () {
                                    $scope.backToList(true);  // force quit
                                    $scope.safeApply();
                                },
                                '退出教室'
                            );
                        }
                    } else {
                        hideReconnectDialog();
                    }
                    if (status === 400 ||
                        status === 403 ||
                        status === 404) {
                        replay.setStorageData(key, '', status, function () {
                            console.log('replay setStorageData of ' + key + ' failed');
                        });
                    } else if (status === 0) {
                        $timeout(function () {
                            _download(rtpType);
                        }, 1000);
                    } else {
                        _download(rtpType);
                    }
                });
            };
        }
        else if (fragment[0] === 'replay_video_chunk') {
            _download = function (rtpType) {
                replayDataDownload(rtpType, fragment[2], function (data) {
                    hideReconnectDialog();
                    hideKickoutDialog();
                    console.log('replay setStorageData of ' + key );
                    replay.setStorageData(key, data, 0, function () {
                        console.log('replay setStorageData of ' + key + ' failed');
                    });
                }, function (res, status) {
                    if (status === 0) {
                        var reconnectCount = showReconnectDialog();
                        if (reconnectCount >= RECONNECT_LIMIT && $scope.inReplay) {
                            kickoutHandle = DialogService.alert(
                                '网络连接有点问题，无法连接服务器',
                                function () {
                                    $scope.backToList(true);  // force quit
                                    $scope.safeApply();
                                },
                                '退出教室'
                            );
                        }
                    } else {
                        hideReconnectDialog();
                    }
                    if (status === 400 ||
                        status === 403 ||
                        status === 404) {
                        replay.setStorageData(key, '', status, function () {
                            console.log('replay setStorageData of ' + key + ' failed');
                        });
                    } else if (status === 0) {
                        $timeout(function () {
                            _download(rtpType);
                        }, 1000);
                    } else {
                        _download(rtpType);
                    }
                });
            };
        }
        if (_download) {
            _download(rtpType);
        }
    });

    EventListenerService.subscribe('room.onEndClass', function () {
        console.log('end class');
    });

    EventListenerService.subscribe('upload.client.log', function (type,log) {        
         WebApiService.updlog(type,log);
    });

    $scope.showCommentDialog = function (episode) {
        $scope.dialog.episodeToComment = episode;
        UAService.logAction('comment');
    };
    $scope.$on('assistOnBoard', function (event, isBoard) {
        $scope.isAssistantOnBoard = isBoard;
    });
    $scope.switchMicrophoneQueue = function (isOpen) {
        $scope.isMicrophoneQueueOpen = isOpen;
    };
    $scope.hideCommentDialog = function () {
        $scope.dialog.episodeToComment = null;
    };

    $scope. registerCloseInterceptor(function (source, closeAction) {
        if (source === 'close' ) {
            DialogService.confirm({
                message: '当前正在看回放，关闭窗口将会关闭直播课客户端，确定关闭吗？',
                okCallback: function () {
                    closeMedia(closeAction);
                }
            });
        } else {
            closeMedia(closeAction);
        }
    });


    $scope.$on('$destroy', function () {
        EventListenerService.cancel('replay');
        $scope.dialog = {};
        $scope.deregisterCloseInterceptor();
        $timeout.cancel(enteringTimeoutPromise);
        $rootScope.inReplay = false;
    });

    init.call(this);
};

Replay.prototype = Object.create(Base.prototype);

module.exports = [
    '$scope',
    '$location',
    '$interval',
    '$timeout',
    '$window',
    'WebApiService',
    'UserInfoService',
    'DialogService',
    'UAService',
    'CourseMapService',
    '$rootScope',
    '$http',
    'EventListenerService',
    'EpisodesService',
    Replay
];
