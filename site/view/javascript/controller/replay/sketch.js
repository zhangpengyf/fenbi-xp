'use strict';

var Base = require('../base/sketch');

var Sketch = function (
    $scope,
    WebApiService,
    $interval,
    EventListenerService,
    $timeout,
    StrokeBufferService
) {
    Base.call(
        this,
        $scope,
        WebApiService,
        $interval,
        EventListenerService,
        $timeout,
        StrokeBufferService
    );
    var self = this;        

    this.onKeynoteRendered = function () {         
        self.eraseStrokeCanvas();

        var strokes = StrokeBufferService.get(
            $scope.roomId,
            $scope.keynote.keynoteId,
            $scope.keynote.currentPageIndex
        );

        self.renderStrokes(strokes);        
        console.log('replay render ' + $scope.keynote.currentPageIndex + ' ' + strokes.length);        
    };
};
Sketch.prototype = Object.create(Base.prototype);

module.exports = [
    '$scope',
    'WebApiService',
    '$interval',
    'EventListenerService',
    '$timeout',
    'StrokeBufferService',
    Sketch
];