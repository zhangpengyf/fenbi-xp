'use strict';
var Base = require('../base/chat');
var $ = require('jquery');
var Chat = function (
    $scope,
    $interval,
    WebApiService,
    ChatMessageService,
    DialogService,
    $compile,
    UserInfoService,
    EventListenerService,
    $timeout
) {
    Base.call(
        this,
        $scope,
        $interval,
        WebApiService,
        ChatMessageService,
        DialogService,
        $compile,
        UserInfoService,
        EventListenerService
    );
    var $messageList = $('.message-list');
    var messageListDOM = $messageList.get(0);
    var isMessageListBottom = function () {
        var redundancy = 10;
        if (messageListDOM.scrollHeight - messageListDOM.scrollTop - redundancy <= messageListDOM.clientHeight) {
            return true;
        } else {
            return false;
        }
    };

    EventListenerService.subscribe(
        'room.onRoomInfo',
        function () {
            $scope.chatMessageList = [];
        }
    );

    EventListenerService.subscribe('episode.chat.messageList.keepBottom', function () {
        if (!isMessageListBottom()) {
            return;
        }
        var VERIFY_TIME = 3;
        var VERIFY_INTERVAL = 20;
        var count = VERIFY_TIME;
        var lastHeight = null;
        var newHeight = null;
        var oldHeight = null;

        var _verify = function () {
            newHeight = $messageList.height();
            oldHeight = lastHeight;
            if (newHeight === oldHeight) {
                count --;
            } else {
                count = VERIFY_TIME;
            }
            lastHeight = newHeight;
            $scope.scrollToBottom();
            if (count > 0) {
                $timeout(_verify, VERIFY_INTERVAL);
            }
        };
        _verify();
    });
};
Chat.prototype = Object.create(Base.prototype);
module.exports = [
    '$scope',
    '$interval',
    'WebApiService',
    'ChatMessageService',
    'DialogService',
    '$compile',
    'UserInfoService',
    'EventListenerService',
    '$timeout',
    Chat
];