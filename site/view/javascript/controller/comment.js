'use strict';

module.exports =  ['$scope', 'WebApiService','DialogService',
    function ($scope, WebApiService,DialogService) {
        $scope.submitting = false;
        $scope.comment = {
            score: 0,
            comment: '',
            wordLimit: 140,
        };

        $scope.onRate = function (stars) {
            $scope.comment.stars = stars;
            $scope.comment.score = 2 * stars;
        };

        $scope.doSumbit = function() {
            $scope.submitting = true;

            WebApiService.post({
                    path: 'episodes/' + $scope.episode.id + '/comments',
                    courseId: $scope.episode.courseId
                },
                {
                    data: {
                        episodeId: $scope.episode.id,
                        score: $scope.comment.score,
                        comment: $scope.comment.comment
                    }
                }).success(function () {
                    // update comment stat
                    var stat = $scope.episode.commentStat;
                    stat.myScore = $scope.comment.score;
                    stat.myStar = $scope.comment.stars;
                    stat.avgScore = ((stat.avgScore * stat.count + stat.myStar * 2) /
                    (stat.count + 1)).toFixed(1);
                    stat.avgStar = Math.round(stat.avgScore * 5 / 10);
                    stat.count += 1;
                    $scope.submitting = false;
                    $scope.hideCommentDialog();
                }).error(function (data, status) {
                    console.log('failed: ' + status + ' data: ' + data.toString());
                });
        };

        $scope.submit = function () {
            if ($scope.submitting ||
                $scope.comment.score === 0 ||
                    $scope.comment.comment.length > $scope.comment.wordLimit) {
                return;
            }

            if($scope.comment.comment.length <= 0) {
                DialogService.confirm({
                    message: '打分的同时别忘了写下听课体会喲',
                    cancelText: '继续评价',
                    okText: '确认提交',
                    okCallback: function() {
                        console.log('..................');
                        $scope.doSumbit();
                    },
                });
                return;
            }

            $scope.doSumbit();
        };
    }
];
