'use strict';

var $ = require('jquery');

module.exports =  ['$scope', 'WebApiService', function ($scope, WebApiService) {

    $scope.feedback = {};

    $scope.uploadFile = function () {
        $('#fileInput').click();
    };

    $scope.onFileSelect = function ($files) {
        if (!$files || $files.length === 0) {
            $scope.feedback.image = null;
        } else {
            var image = $files[0];
            if (image.size > 1024 * 1024) {
                $scope.feedback.imageTip = '图片文件太大';
                $scope.feedback.image = null;
            } else {
                $scope.feedback.imageTip = null;
                $scope.feedback.image = image;
            }
        }
    };

    $scope.submit = function () {
        if (!$scope.feedback.content) {
            $scope.feedback.contentTip = '请输入反馈内容';
            return;
        } else if ($scope.feedback.content.length > 500) {
            $scope.feedback.contentTip = '反馈内容不能多于500字';
            return;
        } else {
            $scope.feedback.contentTip = '';
        }
        $scope.submitting = true;
        WebApiService.upload({
            path: 'feedback'
        }, {
            data: {
                courseId: 1,
                content: $scope.feedback.content,
                contact: $scope.feedback.contact
            },
            file: $scope.feedback.image,
            fileFormDataName: 'image'
        }).success(function () {
            $scope.hideFeedbackDialog();
        }).error(function () {
        }).finally(function () {
            $scope.submitting = false;
        });
    };

}];
