'use strict';

module.exports =  [
    '$scope',
    'WebApiService',
    'EventListenerService',
    'EpisodesService',
    function ($scope, WebApiService,EventListenerService,EpisodesService) {

        $scope.labels = [];
        $scope.labelsdata = [];

        WebApiService.get({
            path: EpisodesService.getLectureUrl('user/lectures/' + EpisodesService.getLectureId() + '/tags')
        }).success(function (labels) {
            for(var i = 0; i < labels.datas.length; i ++) {
                $scope.labels.push(labels.datas[i].name);
            }
            $scope.labelsdata = labels.datas;
        }).error(function (data, status) {
            console.log('fetch label failed ' + status + ' ' + data);
        });

        $scope.selectLabel = function ($index) {
            EventListenerService.trigger('selectLabel',$scope.labelsdata[$index]);
        };
}];