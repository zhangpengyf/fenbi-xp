'use strict';

module.exports =  [
    '$scope',
    'WebApiService',
    function ($scope, WebApiService) {

    $scope.versionInfo = truman.version;

    var compareVersion = function (oldVersion, newVersion) {
        var oldVersionArray = oldVersion.split('.');
        var newVersionArray = newVersion.split('.');
        if (oldVersionArray.length > newVersionArray.length) {
            return -1;
        } else if (oldVersionArray.length < newVersionArray.length) {
            return 1;
        }

        for (var i = 0; i < oldVersionArray.length; i++) {
            var num1 = parseInt(oldVersionArray[i]);
            var num2 =  parseInt(newVersionArray[i]);
            if (num1 < num2) {
                return 1;
            } else if (num1 === num2) {
                continue;
            } else {
                return -1;
            }
        }
    };

    var identity = truman.userType === 1? 'teacher' : 'student';
    WebApiService.get({
        path: 'apps'
    }, {
        params: {
            userType: identity
        }
    }).success(function (newestVersionInfo) {
        if (compareVersion($scope.versionInfo, newestVersionInfo.current) == 1) {
            $scope.hasNewVersion = true;
        }
    }).error(function (data, status) {
        console.log('fetchUserInfo failed ' + status + ' ' + data);
    });

    $scope.updateNow = function () {
        truman.sendMessage('update.download', []);
    };
}];