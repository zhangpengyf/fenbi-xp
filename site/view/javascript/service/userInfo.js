'use strict';

module.exports = [
    '$rootScope',
    '$interval',
    'WebApiService',
    '$timeout',    
    function (
        $rootScope,
        $interval,
        WebApiService,
        $timeout
    ) {
        var userInfoCache = {};
        var pools = {};
        var createUserInfo = function (userId, nickname, status,right) {
            nickname = nickname || userId;
            status = status || 0;
            right = right || -1;

            return {
                id: userId,
                name: nickname,
                status: status,
                right:right
            };
        };
        function fetch() {
            //userIdMap, successCb, errorCb;
            /*var userIds = [];

            for(var user in userIdMap) {
                userIds.push(user);
            }

            var userIdStr = userIds.join(',');
            var href = $window.location.href;
            var keyfrom = null;
            if (href.indexOf('#/live') >= 0) {
                keyfrom = 'live';
            } else if (href.indexOf('#/replay') >= 0) {
                keyfrom = 'replay';
            }

            if(userIdStr === '') { //TODO fix me after new server
                return;
            }

            WebApiService.get({
                path: 'users'
            }, {
                params: {
                    userIds: userIdStr,
                    keyfrom: keyfrom
                }
            }).success(function (data) {
                for (var i = 0; i < data.length; i++) {
                    var userInfo = data[i];
                    if (userInfo) {
                        var userId = userInfo.id;
                        var nickname = userInfo.nickname;
                        var rights = -1;

                        if(userIdMap[userId]) { //don't update rights
                            rights = userIdMap[userId].right;
                        }

                        userInfoCache[userId] = createUserInfo(userId, nickname, 1,rights);
                    }
                }
                if (successCb) {
                    successCb(data);
                }
            }).error(function (data, status) {
                if (errorCb) {
                    errorCb(data, status);
                }
                console.log('UserInfoService fetch user info failed ' + status + ' ' + data);
            });*/
        }

        var Pool = function (minTime, maxTime, isUpdateOld) {
            this.preFetchBuffer = [];
            this.startFetch = false;
            this.importantLength = 0;
            this.minTime = minTime;
            this.maxTime = maxTime;
            this.isUpdateOld = isUpdateOld;
        };
        Pool.prototype.pause = function () {
            this.notWork = true;
        };

        /*
         * status of UserInfoCache element is 0 indicate buffer is old that need to update
         * status of UserInfoCache element is 1 indicate buffer is new that don't need to update
         */
        Pool.prototype.add = function (userId, important,right,nickname) {
            if (this.notWork) {
                return;
            }

            if(nickname !== undefined) {
                userInfoCache[userId] = createUserInfo(userId, nickname, 1,right);
                return;
            }

            if (!userInfoCache[userId] ||
                !userInfoCache[userId].name ||
                this.isUpdateOld && !userInfoCache[userId].status
            ) {
                this.delete(userId);

                if(right === undefined) {
                    right = -1;
                }
                var user = {id:userId,right:right};

                if (important) {
                    this.preFetchBuffer.push(user);//unshift()
                    this.importantLength ++;
                } else {
                    this.preFetchBuffer.splice(this.importantLength, 0, user);
                }
                if (!this.startFetch) {
                    this.fetch();
                }
            }
        };

        Pool.prototype.addArray = function (userIds) {
            if (this.notWork) {
                return;
            }
            var self = this;
            userIds.forEach(function (userId) {
                self.add(userId,0,-1);
            });
        };

        Pool.prototype.addArrayObj = function (userIds) {
            if (this.notWork) {
                return;
            }
            var self = this;
            userIds.forEach(function (userId) {
                self.add(userId.id,0,userId.right);
            });
        };

        Pool.prototype.delete = function (userId) {
            if (this.notWork) {
                return;
            }
            var index = this.preFetchBuffer.indexOf(userId);
            if (index > -1) {
                this.preFetchBuffer.splice(index, 1);
                if (index < this.importantLength) {
                    this.importantLength --;
                }
            }
        };

        Pool.prototype.getFetchUsers = function (length) {
            if (this.notWork) {
                return;
            }

            var fetchUsers = {};
            for (var i = 0; i < length;) {
                var user = this.preFetchBuffer[i];
                if (!user) {
                    break;
                }
                var userId = user.id;

                if (userInfoCache[userId] &&
                    userInfoCache[userId].name &&
                    (!this.isUpdateOld || userInfoCache[userId].status)) {
                    this.delete(userId);
                } else {
                    fetchUsers[userId] = this.preFetchBuffer[i];
                }
                i++;
            }
            return fetchUsers;
        };

        Pool.prototype.fetch = function () {
            if (this.notWork) {
                return;
            }
            var self = this;
            self.startFetch = true;
            var delayTime = parseInt((Math.random() * self.maxTime + self.minTime) * 1000);
            var fetchUsers = null;
            var successCb = function () {
                for (var i = 0; i < fetchUsers.length; i++) {
                    self.delete(fetchUsers[i]);
                }
                self.fetch();
            };
            var errorCb = function () {
                //self.fetch();
            };

            $timeout(function () {
                fetchUsers = self.getFetchUsers(100);
                if (Object.getOwnPropertyNames(fetchUsers).length <= 0) {
                    self.startFetch = false;
                    return;
                }
                fetch(fetchUsers, successCb, errorCb);
            }, delayTime);
        };
        Pool.prototype.cancel = function () {
            this.preFetchBuffer = [];
            this.startFetch = false;
            this.importantLength = 0;
            this.notWork = false;
        };
        return {
            get: function (userId) {
                if (userInfoCache[userId]) {
                    return userInfoCache[userId];
                } else {
                    return createUserInfo(userId);
                }
            },

            set: function (userId, nickName) {
                if(userInfoCache[userId]) {
                    userInfoCache[userId].name = nickName;
                }
                else  {
                    userInfoCache[userId] = createUserInfo(userId, nickName, 1);
                }
                return userInfoCache[userId];
            },

            setOld: function (userId) {
                if (userInfoCache[userId]) {
                    userInfoCache[userId].status = 0;
                }
            },

            setAllOld: function () {
                Object.keys(userInfoCache).forEach(function (key) {
                    userInfoCache[key].status = 0;
                });
            },

            initPool: function (poolName, minTime, maxTime, isUpdateOld) {
                pools[poolName] = new Pool(minTime, maxTime, isUpdateOld);
                return pools[poolName];
            },

            getPool: function (poolName) {
                return pools[poolName];
            },

            destroyPool: function (poolName) {
                if (pools[poolName]) {
                    pools[poolName].cancel();
                    pools[poolName] = null;
                }
            },

            fetchAllUsers: function (users) {
                var pool = new Pool(0, 0, true);
                pool.addArray(users);
                return pool;
            },

            fetchAllStudent: function (users) {
                var pool = new Pool(0, 0, true);
                pool.addArrayObj(users);
                return pool;
            },

        };
    }
];