'use strict';
var $ = require('jquery');

module.exports = function () {
    var showDisconnect = function () {
        $('.disconnect-wrapper').show();
    };
    var hideDisconnect = function () {
        $('.disconnect-wrapper').hide();
    };
    return {
        appear: function () {
            showDisconnect();
        },
        disappear: function () {
            hideDisconnect();
        }
    };
};