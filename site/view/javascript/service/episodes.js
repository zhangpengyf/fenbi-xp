'use strict';

module.exports =  [ '$location','localStorageService',function ($location,localStorageService) {
    var episodeList = null;
    var episodePageCount = 0;
    var episodeCurPage = 0;
    var lecture;
    var episode;
    var lectureTitle;
    var prefix;
    var subject = null;

    var EpisodeStatus = {
            Coming: 0,
            Live: 1,
            Converting: 2,
            Replay: 3,
            Expired: 4
        };

    subject  = decodeURI(localStorageService.get('subject'));
    if(subject.length <= 0) {
        subject = '[公考员考试]';
    }

    prefix  = localStorageService.get('prefix');
    if(prefix === null) {
        prefix = 'gwy';
    }

    return {
        setLectureId : function(lectureId) {
            lecture = lectureId;
        },

        getLectureId : function() {
            return lecture;
        },

        setEpisodeId : function(episodeId) {
            episode = episodeId;
        },

        getEpisodeId : function() {
            return episode;
        },

        setLectureTitle : function(title) {
            lectureTitle = title;
        },

        getLectureTitle : function() {
            return lectureTitle;
        },

        setEpisodeSubject: function(presubject) {
            subject = presubject;
            localStorageService.set('subject',encodeURI(subject));
        },

        getEpisodeSubject: function() {
            return subject;
        },

        setEpisodePrefix: function(prefixname) {
            prefix = prefixname;
            localStorageService.set('prefix',prefix);
        },

        getEpisodePrefix : function() {
            return prefix;
        },

        getLectureUrl : function( url ) {
            var fullUrl = prefix + '/' + url;
            console.log(fullUrl);

            return fullUrl;
        },

        getEpisodeList: function () {
            return episodeList;
        },

        setEpisodeList: function (value) {
            episodeList = value;
        },

        setEpisodePageCount : function(value) {
            episodePageCount = value;
        },

        getEpisodePageCount : function() {
            return episodePageCount;
        },

        setEpisodeCurPage : function(value) {
            episodeCurPage = value;
        },

        getEpisodeCurPage : function() {
            return episodeCurPage;
        },

        enterRoom : function (episode) {
            var roomId = parseInt(episode.id);
            var episodeStatus = episode.status;
            if (episodeStatus == EpisodeStatus.Replay) {
                $location.path('/replay').search({
                    courseId: episode.courseId,
                    episodeId: roomId,
                });
            } else if (episodeStatus == EpisodeStatus.Live) {
                $location.path('/live').search({
                    courseId: episode.courseId,
                    episodeId: roomId,
                });
            }
        },

        episodeStatusString : function (episode) {
            var episodeStatus = episode.status;
            if (episodeStatus == EpisodeStatus.Coming) {
                return '未开播';
            } else if (episodeStatus == EpisodeStatus.Live) {
                return '进入直播';
            } else if (episodeStatus == EpisodeStatus.Replay) {
                return '看回放';
            } else if (episodeStatus == EpisodeStatus.Converting) {
                return '准备中';
            } else if (episodeStatus == EpisodeStatus.Expired) {
                return '已过期';
            } else {
                return '' + episodeStatus;
            }
        },

        episodeButtonClassName : function (episode) {
            var episodeStatus = episode.status;
            if (episodeStatus == EpisodeStatus.Coming) {
                return 'not-ready';
            } else if (episodeStatus == EpisodeStatus.Live) {
                return 'live';
            } else if (episodeStatus == EpisodeStatus.Replay) {
                return 'replay';
            } else if (episodeStatus == EpisodeStatus.Converting) {
                return 'converting';
            } else {
                return 'expired';
            }
        },

        episodeWatchHistory : function(episode) {
            if(episode.watchHistory === undefined || episode.watchHistory === null) {
                return '未观看';
            }
            else{
                var percernt = Math.floor(episode.watchHistory.watchedPercent * 100);
                if(percernt == 100) {
                    return '已看完';
                }
                else if(percernt === 0) {
                    percernt = 1;
                }
                return '观看至' +  percernt + '%';
            }
        },

        setEpisodeWatcherHistory : function(roomId,percent) {
            if(episodeList) {
                for(var i = 0; i < episodeList.length; i++){
                    if(episodeList[i].id == roomId) {
                        if(episodeList[i].watchHistory) {
                            episodeList[i].watchHistory.watchedPercent = percent;
                        }
                        else{
                            episodeList[i].watchHistory = {watchedPercent:percent,watchedTimes:1};
                        }

                        var jstr = JSON.stringify(episodeList[i]);
                        console.log('setEpisodeWatcherHistory ' + jstr);
                    }
                }
            }
        },

        getEpisodeWatchedPercent : function(roomId) {
            if(episodeList) {
                for(var i = 0; i < episodeList.length; i++){
                    if(episodeList[i].id == roomId) {
                        if(episodeList[i].watchHistory) {
                            return episodeList[i].watchHistory.watchedPercent;
                        }
                        else{
                            return 0;
                        }
                    }
                }
            }
        },

        getEpisode: function (roomId) {
            for (var i = 0; i <= episodeList.length; i++) {
                var episode = episodeList[i];
                if (episode.id == roomId) {
                    return episode;
                }
            }
            return null;
        }
    };
}];
