'use strict';

var _ = require('underscore');
var webServerMap = {};

if (truman.buildType === 'test') {//test
    webServerMap = {
        truman: 'http://ke.fenbilantian.cn',
        ytk: 'http://www.fenbilantian.cn',
        cdn: 'http://ke.fenbilantian.cn',
        log: 'http://log.fenbilantian.cn',
        live: 'http://live.fenbilantian.cn'
    };
} else if (truman.buildType === 'beta') {//beta
    webServerMap = {
        truman: 'http://kb.fenbi.com',
        ytk: 'http://www.fenbi.com',
        cdn: 'http://kb.fbcontent.cn',
        log: 'http://log.fenbi.com',
        live: 'http://live.fenbi.com'
    };
} else {
    webServerMap = {
        truman: 'http://ke.fenbi.com',
        ytk: 'http://www.fenbi.com',
        cdn: 'http://ke.fbcontent.cn',
        log: 'http://log.fenbi.com',
        live: 'http://live.fenbi.com'
    };
}



module.exports = [
    '$http',
    '$upload',
    'CourseMapService',
    '$timeout',
    '$q',
    function ($http, $upload, CourseMapService, $timeout, $q) {
        var getFullUrl = function(trumanWebApiConfig, ngHttpConfig) {
            var apiPath = trumanWebApiConfig['path'];
            var courseId = trumanWebApiConfig['courseId'];
            var server = trumanWebApiConfig['server'];
            if (!server) {
                server = 'truman';
            }

            var webServer = webServerMap[server];
            var prefix = 'api';
            if (server === 'truman') {
                prefix = 'win';

                if(ngHttpConfig === undefined) {
                    ngHttpConfig = {};
                }
                if (!ngHttpConfig.params) {
                    ngHttpConfig.params = {};
                }
                ngHttpConfig.params.version = truman.version;
            }           

            var clientPrefix = webServer + '/' + prefix;
            if (apiPath) {
                var apiUrl = '';
                if(server != 'external') {
                    var courseUrl = '';
                    apiUrl = clientPrefix;
                    if (courseId) {
                        courseUrl = CourseMapService.getCourseUrl(courseId);
                        apiUrl += ('/' + courseUrl);
                    }
                    apiUrl += ('/' + apiPath);
                }
                else
                {
                    apiUrl = apiPath;
                }

                return apiUrl;
            }

            return '';
        };

        var getCombinedConfig = function (trumanWebApiConfig, ngHttpConfig) {
            var apiPath = trumanWebApiConfig['path'];
            var courseId = trumanWebApiConfig['courseId'];
            var server = trumanWebApiConfig['server'];
            if (!server) {
                server = 'truman';
            }

            var webServer = webServerMap[server];
            var prefix = 'api';
            if (server === 'truman') {
                prefix = 'win';
                if (!ngHttpConfig.params) {
                    ngHttpConfig.params = {};
                }
                ngHttpConfig.params.version = truman.version;
            }
          
            var clientPrefix = webServer + '/' + prefix;
            if(server === 'log') {
                clientPrefix = webServer;
            }

            if (apiPath) {
                var apiUrl = '';
                if(server != 'external') {
                    var courseUrl = '';
                    apiUrl = clientPrefix;
                    if (courseId) {
                        courseUrl = CourseMapService.getCourseUrl(courseId);
                        apiUrl += ('/' + courseUrl);
                    }
                    apiUrl += ('/' + apiPath);
                }
                else
                {
                    apiUrl = apiPath;
                }
                return _.defaults({
                    url: apiUrl
                }, ngHttpConfig);
            } else {
                return {};
            }
        };

        var invokeWebApi = function (trumanWebApiConfig, ngHttpConfig) {
            var combinedConfig = getCombinedConfig(trumanWebApiConfig, ngHttpConfig);
            /**
             * angular-file-upload has wrapper setRequestHeader method of xmlHttpRequest object
            **/
            var deferred = $q.defer();

            if(combinedConfig.hasOwnProperty('params') && combinedConfig.params.hasOwnProperty('timeout')) {
                combinedConfig.timeout = combinedConfig.params.timeout;
            }
            else
            {
                combinedConfig.timeout = deferred.promise;
            }

            if (window.XMLHttpRequest.__isShim) {
                if (!combinedConfig.headers) {
                    combinedConfig.headers = {};
                }
                combinedConfig.headers['__setXHR_'] = function() {
                    return function(xhr) {
                        if (!xhr) {
                            return;
                        }
                        xhr.addEventListener('progress', function(e) {
                            if (combinedConfig.progress) {
                                $timeout(function() {
                                    if(combinedConfig.progress) {
                                        combinedConfig.progress(e);
                                    }
                                });
                            }
                        }, false);
                    };
                };
            }

            var promise = $http (combinedConfig);
            promise.progress = function (fn) {
                combinedConfig.progress = fn;
                return promise;
            };
            promise.abort = function () {
                deferred.resolve();
                deferred = null;
            };
            return promise;
        };
        function getUrls (trumanWebApiConfigs, combinedConfig, index, deferred, progressCb) {
            var len = trumanWebApiConfigs.length;
            var promise = invokeWebApi(trumanWebApiConfigs[len - index], combinedConfig);
            if (progressCb) {
                promise.progress(progressCb);
            }
            promise.then(function(data) {
                deferred.resolve(data.data, data.status);
            }, function(data){
                if (index === 1) {
                    deferred.reject(data.data, data.status);
                } else {
                    getUrls(trumanWebApiConfigs, combinedConfig, index - 1, deferred);
                }
            });
            return deferred.promise;
        }

        return {
            get : function (trumanWebApiConfig, ngHttpConfig) {
                var combinedConfig = _.defaults({method: 'Get'}, ngHttpConfig);
                return invokeWebApi(trumanWebApiConfig, combinedConfig);
            },
            post: function (trumanWebApiConfig, ngHttpConfig) {
                var combinedConfig = _.defaults({method: 'Post'}, ngHttpConfig);
                return invokeWebApi(trumanWebApiConfig, combinedConfig);
            },
            upload: function (trumanWebApiConfig, angularFileUploadConfig) {
                var combinedConfig = getCombinedConfig(trumanWebApiConfig, angularFileUploadConfig);
                return $upload.upload(combinedConfig);
            },
            server: function (serverName) {
                return webServerMap[serverName];
            },
            fullurl: function(trumanWebApiConfig, ngHttpConfig) {
                return getFullUrl(trumanWebApiConfig,ngHttpConfig);
            },
            getUrls: function (trumanWebApiConfigs, ngHttpConfig, progressCb) {
                var index = trumanWebApiConfigs.length;
                var combinedConfig =  _.defaults({method: 'Get'}, ngHttpConfig);
                var deferred = $q.defer();
                return getUrls(trumanWebApiConfigs, combinedConfig, index, deferred, progressCb);                
            },
            updlog: function(type,log) {                
                this.post({
                    path: 'logcollector/liveinfo',
                    server: 'log'
                }, {
                    headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
                    transformRequest: function (obj) {
                        var str = [];
                        for (var p in obj) {
                            str.push(encodeURIComponent(p) + '=' + encodeURIComponent(obj[p]));
                        }
                        return str.join('&');
                    },
                    data: {
                        'data': log
                    }
                });
            }
        };
    }
];
