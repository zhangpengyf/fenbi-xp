'use strict';

module.exports = function () {
    var clientMessageId = 0;
    function ChatMessage(roomId, userId, messageId, clientMessageId, content, timestamp, type,nickname) {
        this.roomId = roomId;
        this.userId = userId;
        this.messageId = messageId;
        this.clientMessageId = clientMessageId;
        this.content = content;
        this.timestamp = timestamp;
        this.type = type;
        this.nickname = nickname;
    }
    return {
        createServerMessage: function (roomId, userId, messageId, clientMessageId, content, timestamp, type,nickname) {
            return new ChatMessage(roomId, userId, messageId, clientMessageId, content, timestamp, type,nickname);
        },
        createClientMessage: function (roomId, userId, content, type) {
            return new ChatMessage(roomId, userId, null, clientMessageId++, content, null, type,null);
        }
    };
};
