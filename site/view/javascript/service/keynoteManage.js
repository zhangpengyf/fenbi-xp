'use strict';

module.exports = [
    '$rootScope',
    '$interval',
    '$timeout',
    function (
        $rootScope,
        $interval,
        $timeout
    ) {
    PDFJS.disableCreateObjectURL = true;
    var pdf = null,
        pageRendering = false,
        nextRenderOptions = {};
    var getScale = function (page, container) {
        var viewport = page.getViewport(1);
        var pWidth = viewport.width;
        var pHeight = viewport.height;
        var cWidth = container.width();
        var cHeight = container.height();
        if (pWidth / pHeight > cWidth / cHeight) {
            return cWidth / pWidth;
        } else {
            return cHeight / pHeight;
        }
    };

    var renderPage = function (num, container, height, canvas, successCb, errorCb) {
        pageRendering = true;
        if (!height) {
            canvas = container.find('canvas').get(0);
        }
        var context = canvas.getContext('2d');
        pdf.getPage(num).then(function (page) {
            if (!pdf) {
                return;
            }
            var viewport = page.getViewport(1);
            var scale = null;
            if (height) {
                scale = height / viewport.height;
            } else {
                scale = getScale(page, container);
            }
            var scaleViewport = page.getViewport(scale);
            canvas.width = scaleViewport.width;
            canvas.height = scaleViewport.height;
            var renderContext = {
                canvasContext: context,
                viewport: scaleViewport
            };
            var renderTask = page.render(renderContext);
            renderTask.promise.then(function () {
              if (!pdf) {
                 return;
              }
              pageRendering = false;
              if (successCb) {
                $timeout(successCb.bind(null, num), 30);
              }
              if (nextRenderOptions.num) {
                renderPage(
                    nextRenderOptions.num,
                    nextRenderOptions.container,
                    nextRenderOptions.height,
                    nextRenderOptions.canvas,
                    nextRenderOptions.successCb,
                    nextRenderOptions.errorCb
                );
                nextRenderOptions = {};
              }
            }).catch(function () {
                pageRendering = false;
                if (errorCb) {
                    $timeout(errorCb.bind(null, num), 0);
                }
            });
        }).catch(function () {
            pageRendering = false;
        });
    };
    var queueRenderPage = function (num, container, height, canvas, successCb, errorCb) {
        if (!pdf) {
            return;
        }
        if (pageRendering) {
            nextRenderOptions.container = container;
            nextRenderOptions.height = height;
            nextRenderOptions.canvas = canvas;
            nextRenderOptions.num = num;
            nextRenderOptions.successCb = successCb;
            nextRenderOptions.errorCb = errorCb;
        } else {
            renderPage(num, container, height, canvas, successCb, errorCb);
        }
    };
    return {
        load: function (data, successCb, errorCb) {
            this.clearPdfCache();
            PDFJS.getDocument({
                data: data
            }).then(function (_pdf) {
                pdf = _pdf;
                if (successCb) {
                    successCb();
                }
            }).catch(function (error) {
                if (errorCb) {
                    errorCb(error);
                }
                console.log(error);
            });
        },
        render: function (num, container, successCb, errorCb) {
            queueRenderPage(num, container, null, null, successCb, errorCb);
        },

        renderWithHeight: function (num, height, canvas, successCb, errorCb) {
            queueRenderPage(num, null, height, canvas, successCb, errorCb);
        },

        clearPage: function (canvas) {
            canvas.getContext('2d').clearRect(0, 0, canvas.width, canvas.height);
        },

        clearPdfCache: function () {
            if (pdf) {
                pdf.destroy();
                pdf = null;
                pageRendering = false;
                nextRenderOptions = {};
            }
        }
    };
}];