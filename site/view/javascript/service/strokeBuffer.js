'use strict';

module.exports = [function () {
    var pointsMap = {};
    var createPool = function (roomId, keynoteId, num) {
        if (!pointsMap[roomId]) {
            pointsMap[roomId] = {};
        }
        if (!pointsMap[roomId][keynoteId]) {
            pointsMap[roomId][keynoteId] = {};
        }
        if (!pointsMap[roomId][keynoteId][num]) {
            pointsMap[roomId][keynoteId][num] = [];
        }
    };
    return {
        add: function (roomId, keynoteId, num, points,lineWidth,lineColor) {
            createPool(roomId, keynoteId, num);
            if (points && points.length > 0) {
                pointsMap[roomId][keynoteId][num].push({'points':points,'lineWidth':lineWidth,'lineColor':lineColor});
            }
        },

        reset: function (roomId, keynoteId, num, pointsArray) {
            createPool(roomId, keynoteId, num);
            pointsMap[roomId][keynoteId][num] = pointsArray;
        },

        remove: function (roomId, keynoteId, num) {
            createPool(roomId, keynoteId, num);
            pointsMap[roomId][keynoteId][num] = [];
            console.log('remove stoke ' + num);
        },

        get: function (roomId, keynoteId, num) {
            createPool(roomId, keynoteId, num);
            return pointsMap[roomId][keynoteId][num];
        },

        cache: function (roomId, keynoteId, num) {
            if (!pointsMap[roomId] ||
                !pointsMap[roomId][keynoteId] ||
                !pointsMap[roomId][keynoteId][num]) {
                return false;
            }
            return true;
        },

        cacheAll: function (roomId, keynoteId, maxNum) {
            if (maxNum < 0 ||
                !pointsMap[roomId] ||
                !pointsMap[roomId][keynoteId]) {
                return false;
            }
            for (var i = 0; i <= maxNum; i++) {
                if (!pointsMap[roomId][keynoteId][i]) {
                    return false;
                }
            }
            return true;
        },

        clearAll: function () {
            pointsMap = {};
            console.log('clear stoke ');
        }
    };
}];