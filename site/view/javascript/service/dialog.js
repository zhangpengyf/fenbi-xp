'use strict';
var $ = require('jquery');

module.exports = [
    '$timeout',
    function ($timeout) {
        var autoDismissTimer = null;

        var setPosition = function (dialogWrapper) {
            var width = $(window).width();
            var height = $(window).height();
            var elemWidth = dialogWrapper.width();
            var elemHeight = dialogWrapper.height();

            var left = (width - elemWidth) / 2;
            left = left > 0? left : 0;

            var top = (height - elemHeight) / 2;
            top = top > 0? top : 0;
            dialogWrapper.css('left', left).css('top', top);
        };

        var showDialog = function (dialogConfig) {
            /*
                clear state
            */
            if (autoDismissTimer !== null) {
                $timeout.cancel(autoDismissTimer);
                autoDismissTimer = null;
            }

            var message = dialogConfig.message;
            var hideOkButton = dialogConfig.hideOkButton || false;
            var hideCancelButton = dialogConfig.hideCancelButton || false;
            var okText = dialogConfig.okText || '确定';
            var cancelText = dialogConfig.cancelText || '取消';
            var okCallback = dialogConfig.okCallback;
            var cancelCallback = dialogConfig.cancelCallback;
            var autoDismiss = dialogConfig.autoDismiss || false;


            var divId = 'truman-modal-dialog';

            //remove first
            var $popupWrapper = $('#' + divId);
            if ($popupWrapper.length !== 0) {
                $popupWrapper.remove();
            }

            var dlgContent = '<div id=' + divId + ' class="truman-modal-dialog display-none">' +
                '</div>';

            $(document.body).append(dlgContent);

            $popupWrapper = $('#' + divId);

            var title = document.createElement('div');
            title.className = 'title';
            $(title).append(message);
            $popupWrapper.append(title);
            if (!hideCancelButton || !hideOkButton) {
                $(title).addClass('leading-title');
            } else {
                $(title).addClass('center-title');
            }

            if (!hideOkButton) {
                var okButton = document.createElement('div');
                okButton.className = 'ok-button';
                $(okButton).append(okText);
                $popupWrapper.append(okButton);

                if (hideCancelButton) {
                    $(okButton).addClass('sole-button');
                } else {
                    $(okButton).addClass('left-button');
                }
                $(okButton).click( function () {
                    $popupOverlay.addClass('display-none');
                    $popupWrapper.addClass('display-none');
                    $popupWrapper.remove();
                    if (okCallback) {
                        okCallback();
                    }
                });
            }

            if (!hideCancelButton) {
                var canelButton = document.createElement('div');
                canelButton.className = 'cancel-button';
                $(canelButton).append(cancelText);
                $popupWrapper.append(canelButton);

                if (hideOkButton) {
                    $(canelButton).addClass('sole-button');
                } else {
                    $(canelButton).addClass('right-button');
                }

                $(canelButton).click( function () {
                    $popupOverlay.addClass('display-none');
                    $popupWrapper.addClass('display-none');
                    $popupWrapper.remove();
                    if (cancelCallback) {
                        cancelCallback();
                    }
                });
            }


            var $popupOverlay = $('#popupOverlay');
            if ($popupOverlay.length === 0) {
                var popupOverlay = document.createElement('div');
                popupOverlay.id = 'popupOverlay';
                popupOverlay.className = 'display-none';
                var popupOverlayHeader = document.createElement('div');
                popupOverlayHeader.id = 'popupOverlayHeader';
                var popupOverlayContent = document.createElement('div');
                popupOverlayContent.id = 'popupOverlayContent';
                popupOverlay.appendChild(popupOverlayHeader);
                popupOverlay.appendChild(popupOverlayContent);
                document.body.appendChild(popupOverlay);
                $popupOverlay = $('#popupOverlay');
            }

            $popupOverlay.removeClass('display-none');
            setPosition($popupWrapper);
            $popupWrapper.removeClass('display-none');

            if (autoDismiss) {
                autoDismissTimer = $timeout(function () {
                    $popupOverlay.addClass('display-none');
                    $popupWrapper.addClass('display-none');
                    $popupWrapper.remove();
                    autoDismissTimer = null;
                }, 2000);
            }
            return {
                cancel: function () {
                    $popupOverlay.addClass('display-none');
                    $popupWrapper.addClass('display-none');
                    $popupWrapper.remove();
                }
            };
        };
        return {
            alert: function(alertMessage, callback, okText) {
                return showDialog({
                    message: alertMessage,
                    hideCancelButton: true,
                    okText: okText,
                    okCallback: callback
                });
            },
            confirm: function (confirmDialog) {
                showDialog(confirmDialog);
            },
            notify: function (notifyMessage) {
                showDialog({
                    message: notifyMessage,
                    hideCancelButton: true,
                    hideOkButton: true,
                    autoDismiss: true
                });
            }
        };
    }
];