'use strict';

var moment = require('moment');

module.exports = [
    function () {
        var timeDiff = 0;// serverTime - localTime in ms
        return {
            getLocalTime: function (serverTimeInUnixMs) {
                var serverTimeNumber = Number(serverTimeInUnixMs);
                var serverTime = moment(serverTimeNumber);
                var localTime = serverTime.subtract('milliseconds', timeDiff);
                return localTime.valueOf();
            },
            setServerCurrentTime: function (serverTimeInGMTStr) {
                if (!serverTimeInGMTStr) {
                    return;
                }
                /*
                moment.js do NOT quit support RFC 1123 string.
                It always need to fallback to Date constructor.
                Remove following convertion when moment.js
                add support.
                */
                var serverDate = new Date(serverTimeInGMTStr);
                var now = moment();
                var serverNow = moment(serverDate);
                timeDiff = serverNow.diff(now, 'milliseconds');
            },
            getCurrentTime: function () {
                return Date.now() + timeDiff;
            }
        };
    }
];