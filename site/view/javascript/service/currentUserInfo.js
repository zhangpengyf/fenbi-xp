'use strict';

module.exports = [
    'WebApiService',
    '$q',
    function (WebApiService, $q) {
        var currentUserId = null;
        function _getUserInfo (field) {
            var deferred = $q.defer();
            var params = {};
            if (field === 'id' && currentUserId) {
                deferred.resolve(currentUserId);
            } else if (field === 'nickname') {
                params.nickname = 1;
            }
            WebApiService.get({
                path: '/users/current'
            }, {
                params: params
            }).success(function (data) {
                deferred.resolve(data[field]);
            }).error(function (data, status) {
                deferred.reject();
                console.log('currentUserInfo failed ' + status + ' ' + data);
            });
            return deferred.promise;
        }
        return {
            getUserId: function () {
                var promise = _getUserInfo('id');
                return promise;
            },
            getNickname: function () {
                var promise = _getUserInfo('nickname');
                return promise;
            }
        };
    }
];