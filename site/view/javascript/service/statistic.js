'use strict';

module.exports = [
    '$window',
    '$location',
    '$http',
    function ($window, $location, $http) {
        var protocolVersion = '1';
        var random = Math.floor(Math.random() * 100);//between 0 - 100
        var timeStamp = (new Date()).getTime() % 1000000;
        var clientId = String(random) + String(timeStamp);

        var gaId = '';
        if (truman.buildType === 'online') {
            if (truman.userType === 1) {
                gaId = 'UA-52010280-3';
            } else {
                gaId = 'UA-52010280-2';
            }
        } else {
            gaId = 'UA-52010280-1';
        }

        var sendToGA = function (gaData) {
            $http({
                method: 'POST',
                url: 'http://www.google-analytics.com/collect',
                headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
                    transformRequest: function (obj) {
                    var str = [];
                    for (var p in obj) {
                        str.push(encodeURIComponent(p) + '=' + encodeURIComponent(obj[p]));
                    }
                    return str.join('&');
                },
                data: gaData
            });
        };
        return {

            logPage: function (action) {
                var page = $location.path();
                var title = action || page;
                var gaData = {
                    v: protocolVersion,
                    tid: gaId,
                    cid: clientId,
                    t: 'pageview',
                    dh: 'localhost',
                    dp: page,
                    dt: title //title
                };
                sendToGA(gaData);
            },
            logAction: function (action) {
                var page = $location.path();
                var gaData = {
                    v: protocolVersion,
                    tid: gaId,
                    cid: clientId,
                    t: 'event',
                    ec: page,
                    ea: action,
                    el: truman.version,
                    ev: 1
                };
                sendToGA(gaData);
            }
        };
    }
];