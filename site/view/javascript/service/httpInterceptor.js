'use strict';

module.exports = [
    '$q',
    '$location',
    '$window',
    'TimeAdjustService',
    'DialogService',
    'DisconnectService',

    function ($q, $location, $window, TimeAdjustService, DialogService, DisconnectService) {
        return {
            // optional method
            'response': function(response) {
                var contentType = response.headers('Content-Type');
                if (contentType && contentType.indexOf('json') > 0) {
                    TimeAdjustService.setServerCurrentTime(response.headers('date'));
                }
                return response || $q.when(response);
            },
            // optional method
            'responseError': function(rejection) {
                if (rejection.config.trumanNoGlobalErrorHanlder) {
                    return $q.reject(rejection);
                }
                var href = $window.location.href;
                if (href.lastIndexOf('login.html') >= 0) {
                    return $q.reject(rejection);//pass to login page's handler
                }
                var statusCode = rejection.status;
                if (statusCode === 0 &&
                    rejection.config.__from !== 'episode' &&
                    rejection.config.__from !== 'replay' &&
                    rejection.config.__from !== 'room') {
                    if (href.indexOf('#/live') < 0 && href.indexOf('#/replay') < 0) {
                        DisconnectService.appear();// episode和replay不处理
                    }
                }
                else if (statusCode === 500) {
                    console.log('server 500 ' + rejection.config.url);
                    //DialogService.alert('服务器似乎有点情绪，要不咱让她休息休息？');
                    /*$timeout(function(){
                        $injector.get('$http')(rejection.config).then(function successCallback() {
                            console.log('server 500 retry ok ' + rejection.config.url);
                        }, function errorCallback() {
                            DialogService.alert('服务器似乎有点情绪，要不咱让她休息休息？');
                        });
                    },1000);*/
                }
                else if (statusCode === 401) {
                    var url = $location.url();
                    if (url.length > 1) {
                        var subUrl = url.slice(1, url.length); //remove leading /
                        $window.location.href = 'login.html?redirectUrl='+encodeURIComponent('index.html#'+subUrl);
                    } else {
                        $window.location.href = 'login.html';
                    }
                }

                TimeAdjustService.setServerCurrentTime(rejection.headers('date'));
                return $q.reject(rejection);
            }
        };
    }
];