'use strict';

var messagePrefix = 'nativeUI.';

module.exports = 
    function () {
        function downloadMaterials(urls, index) {
            if (index > -1) {
                truman.sendMessage(messagePrefix + 'openBrowser', [urls[index]], function(ret){
                    if(ret > 39 && ret < 50) {
                        return;
                    } else {
                        downloadMaterials(urls, index - 1);
                    }
                });
            }
        }
        return {
            downloadMaterials: function (urls) {
                var len = urls.length;
                downloadMaterials(urls, len - 1);
            }
        };
    };

