'use strict';

var _ = require('underscore');

var defaultCourseMap = {
    1: {
        prefix: 'gwy'
    },
    2: {
        prefix: 'gaokao'
    },
    3: {
        prefix: 'sikao'
    },
    4: {
        prefix: 'kaoyan'
    }
};

module.exports = [
    'localStorageService',
    function (localStorageService) {
        var courseList = [];
        var courseMap = localStorageService.get('cousreMap');
        if (!courseMap) {
            courseMap = defaultCourseMap;
        }
        return {
            getCourseUrl: function (courseId) {
                var mapping = courseMap[courseId];
                if (mapping) {
                    return mapping.prefix;
                } else {
                    return '';
                }
            },
            setCourseMap: function (courseMapArray) {
                var courseDic = {};

                for(var j = 0; j < courseMapArray.length; j++) {
                    if(courseMapArray[j].enable) {
                        courseList.push(courseMapArray[j]);
                    }
                }

                for (var i = 0; i < courseMapArray.length; i++) {
                    var course = courseMapArray[i];
                    var courseId = course.id;
                    var coursePrefix = course.prefix;
                    if (courseId && coursePrefix) {
                        courseDic[courseId] = {prefix: coursePrefix};
                    }
                }
                var combinedCourseMap = _.defaults(courseDic, defaultCourseMap);
                courseMap = combinedCourseMap;
                localStorageService.set('cousreMap', courseMap);
            },

            getCourseList: function() {
                return courseList;
            }
        };
    }
];