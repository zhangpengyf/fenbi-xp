'use strict';

module.exports = function () {
    function Keynote(keynoteId, totalPageNum, currentPageIndex) {
        this.keynoteId = keynoteId;
        this.totalPageNum = totalPageNum;
        this.currentPageIndex = currentPageIndex;
    }
    return {
        createKeynote: function (keynoteId, totalPageNum, currentPageIndex) {
            return new Keynote(keynoteId, totalPageNum, currentPageIndex);
        }
    };
};