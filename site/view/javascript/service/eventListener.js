'use strict';

module.exports = [function () {
    var events = {};
    return {
        cancel: function (eventName) {
            if (!eventName) {
                return;
            }
            var names = eventName.split('.');
            var temp = events;
            for (var i = 0;i < names.length - 1; i++) {
                var name = names[i];
                temp = temp[name];
                if (!temp) {
                    return;
                }
            }
            temp[names[names.length - 1]] = null;
        },
        subscribe: function (eventName, func, self) {
            if (!eventName || !func || typeof func !== 'function') {
                return;
            }

            var names = eventName.split('.');
            var temp = events;
            for (var i = 0;i < names.length; i++) {
                var name = names[i];
                if (!temp[name]) {
                    temp[name] = {};
                    temp[name].__funcs = [];
                }
                temp = temp[name];
            }
            func.self = self;
            temp.__funcs.push(func);
        },
        trigger: function (eventName) {
            if (!eventName) {
                return;
            }
            var names = eventName.split('.');
            var temp = events;
            for (var i = 0; i < names.length; i++) {
                var name = names[i];
                temp = temp[name];
                if (!temp) {
                    console.warn('there is no listeners subscribe for event "' + eventName + '"');
                    return;
                }
            }
            if (temp.__funcs && temp.__funcs.length > 0) {
                var funcs = temp.__funcs;
                for (var j = 0; j < funcs.length; j++) {
                    funcs[j].apply(funcs[j].self, Array.prototype.slice.call(arguments, 1));
                }
            } else {
                console.warn('there is no listeners subscribe for event "' + 'eventName"');
            }
        }
    };
}];