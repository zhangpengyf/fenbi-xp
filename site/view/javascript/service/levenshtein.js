'use strict';

module.exports = [function () {
    var minimum = function (a,b,c) {
        return a<b?(a<c?a:c):(b<c?b:c);
    };
    return {
        getLevenshteinDistance: function (s, t) {
            var n=s.length;// length of s
            var m=t.length;// length of t
            var d=[];// matrix
            var i;// iterates through s
            var j;// iterates through t
            var si;// ith character of s
            var tj;// jth character of t
            var cost;// cost
            // Step 1
            if (n === 0) {
                return m;
            }
            if (m === 0) {
                return n;
            }
            // Step 2
            for (i = 0; i <= n; i++) {
                d[i]=[];
                d[i][0] = i;
            }
            for (j = 0; j <= m; j++) {
                d[0][j] = j;
            }
            // Step 3
            for (i = 1; i <= n; i++) {
                si = s.charAt (i - 1);
            // Step 4
                for (j = 1; j <= m; j++) {
                    tj = t.charAt (j - 1);
            // Step 5
                    if (si == tj) {
                        cost = 0;
                    } else {
                        cost = 1;
                    }
            // Step 6
                    d[i][j] = minimum (d[i-1][j]+1, d[i][j-1]+1, d[i-1][j-1] + cost);
                }
            }
            // Step 7
            return d[n][m];
        },
        getSimilarity: function (s, t) {
            var l=s.length>t.length?s.length:t.length;
            var d = this.getLevenshteinDistance(s,t);
            return (1-d/l).toFixed(4);
        }
    };
}];