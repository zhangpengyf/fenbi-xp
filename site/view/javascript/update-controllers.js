'use strict';

var trumanUpdateControllers = angular.module('trumanUpdateControllers', []);

trumanUpdateControllers.controller('UpdatingCtrl', ['$scope', '$location',
    function ($scope, $location) {
        $scope.updateAvailable = function () {
            $location.path('updateAvailable').search({
                version: window.updateVersion,
                requiredFlag: window.requiredFlag,
                downloaing: false
            });
        };

        $scope.noUpdate = function () {
            $location.path('noUpdates');
        };

        $scope.updateError = function () {
            $location.path('updateError');
        };
  }]);

trumanUpdateControllers.controller('NoUpdateCtrl', ['$scope',
    function ($scope) {
        return $scope;
    }
]);

trumanUpdateControllers.controller('UpdateErrorCtrl', require('./controller/updateError'));

trumanUpdateControllers.controller('UpdateAvailableCtrl', [
    '$scope',
    '$location',
    function ($scope, $location) {
        var params = $location.search();
        console.log(params);
        if (truman.userType === 1) {
            $scope.updateDesc =  '检测到粉笔直播课老师版新版本v' + params.version;
        } else {
            $scope.updateDesc =  '检测到粉笔直播课新版本v' + params.version;
        }
        $scope.required = params.requiredFlag && (parseInt(params.requiredFlag) === 1);
        var updateProcess = 0;
        $scope.updateProgres = '0%';
        $scope.isDownloading = (params.downloading === 'true');
        $scope.isDownloaded = false;

        $scope.installUpdate = function () {
            $scope.isDownloading = true;
            truman.sendMessage('update.download', []);
        };

        $scope.skipUpdate = function () {
            window.close();
        };

        $scope.updateDownloaded = function () {
            $scope.isDownloading = false;
            $scope.isDownloaded = true;
        };

        $scope.closeProgram = function () {
            truman.sendMessage('update.install', []);
        };

        $scope.updateDownloadProgress = function (progress) {
            if (progress > 100) {
                progress = 100;
            }
            if (progress < 0) {
                progress = 0;
            }
            if (progress < updateProcess) {
                return;
            }
            updateProcess = progress;
            $scope.updateProgres = updateProcess + '%';
        };

        $scope.updateError = function () {
            $location.path('updateError');
        };
    }
]);