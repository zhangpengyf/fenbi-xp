'use strict';

module.exports = function () {
	return {
		restrict: 'CE',
		scope: {
			'labelText': '@',
			'labelClose': '@',
			'onClose': '&onClose',
		},
		templateUrl: 'template/label.html',
		link: function (scope) {
			scope.close = function() {
				if(scope.onClose) {
					scope.onClose();
				}
			};
		}
	};
};
