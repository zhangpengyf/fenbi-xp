'use strict';

var live = require('../engine/live');

var videoPanel = function (EventListenerService) {
	return {
		restrict: 'E',
		scope: {
			nickname: '=',
			eventname: '=',
			isvideoopen: '=',
			switchable: '=',
			largeUid: '=',
			isTeacher: '=',
			maxVolume: '=',
			currentVolume: '=',
			userid: '=',
			width: '=',
			height: '=',
			countdown: '='
		},
		templateUrl: 'template/videoPanel.html',
		link: function (scope, el) {
			var context = angular.element(el)[0].querySelector('canvas[video]').getContext('2d');
			var pos = {
				x: 0,
				y: 0,
				w: scope.width,
				h: scope.height
			};

			/*var renderVideo = function() {
				if(context) {
		                context.drawImage(this, pos.x, pos.y, pos.w, pos.h);
				}	
			};
			

			var imgArray = [];
			var imagIndex = 0;
			for(var i = 0; i < 10; i++) {
				imgArray[i] = new window.Image();
				imgArray[i].onload = renderVideo;
			}*/
			
			context.fillStyle = '#000';
 			context.imageSmoothingEnabled = false;

			var drawImage = function (url,rotation) {
				/*if(imagIndex++ >= 9) {
					imagIndex = 0;
				}

				imgArray[imagIndex].src = url;*/
				

		    	var imageObj = new window.Image();
		        imageObj.onload = function() {
		            if(context) {			
						context.save();						
      					context.fillRect(0, 0, pos.w, pos.h);

						
						var scale = 1;						
						if(rotation == -90 || rotation == -270) {
							var wRatio = pos.h / imageObj.width;
							var hRatio = pos.w / imageObj.height;
							scale = Math.min(wRatio,hRatio);						
						}						

						var dstW = scale * pos.w;
						var dstH = scale * pos.h;						

						context.translate(pos.w / 2, pos.h / 2);
						context.rotate(rotation*Math.PI/180);
						context.drawImage(imageObj,0,0,imageObj.width,imageObj.height,-dstW/2,-dstH/2,dstW, dstH);							
						//context.drawImage(imageObj,0,0,imageObj.width,imageObj.height,-pos.w/2,-pos.h/2,pos.w, pos.h);	

						context.restore();

						//context.drawImage(imageObj,0,0,pos.w,pos.h);													
		            }
					imageObj = null;
		        };
		        imageObj.src = url;
		    };
			EventListenerService.subscribe(scope.eventname, function (rotation,url) {
				if (scope.largeUid == scope.userid) {
					EventListenerService.trigger('room.video.center', rotation,url);
				} else {
					drawImage(url,rotation, context, pos);

				}
		    });
		    
			scope.$watch('largeUid', function (val) {
				if ( scope.userid == 'center' || val != scope.userid)  {
					context.clearRect(0, 0, scope.width, scope.height);
				}
			});
		    angular.element(el.find('img')).bind('click',function () {
				if(!scope.switchable) {
					return;
				}

				console.log('switchVideoStyle', scope.userid, scope.largeUid);
				
		    	if (scope.largeUid == scope.userid) {
		    		live.switchVideoStyle(0,function(){
						console.log('switchVideoStyle ' + 0);
					});	
		    	} else {
		    		live.switchVideoStyle(scope.userid,function(){
						console.log('switchVideoStyle ' + scope.userid);
					});	
		    	}
					  
				scope.$apply();  
			});	
			
		}
	};
};
module.exports = [
'EventListenerService',
videoPanel
];