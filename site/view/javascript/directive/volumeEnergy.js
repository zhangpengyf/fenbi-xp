'use strict';

module.exports = function () {
	return {
		restrict: 'CE',
		scope: {
			maxVolume: '=',
			currentVolume: '='
		},
		templateUrl: 'template/volumeEnergy.html',
		link: function (scope) {
			scope.energyArray = new Array(scope.maxVolume);
			for (var i = 0; i < scope.energyArray.length; i++) {
				scope.energyArray[i] = 0;
			}

			scope.$watch('currentVolume', function () {

				for (var index in scope.energyArray) {
					if (index < scope.currentVolume) {
						scope.energyArray[index] = 1;
					} else {
						scope.energyArray[index] = 0;
					}
				}
			});
		}
	};
};