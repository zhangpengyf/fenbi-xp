'use strict';
var $ = require('jquery');
var _ = require('underscore');

module.exports = function () {
	return {
		restrict : 'C',
		link : function (scope, element) {
			var headHeight = $('.header-wrapper').height();
			var setHeight = function () {
				var height = window.innerHeight;
				var elementHeight = height - headHeight;
				element.css({
					'min-height' : elementHeight + 'px'
				});
			};
			setHeight();
			var throttledSetHeight = _.throttle(setHeight, 60);
			$(window).on('resize', throttledSetHeight);
		}
	};
};