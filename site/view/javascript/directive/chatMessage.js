'use strict';
var nativeUI = require('../engine/nativeUI');
module.exports = function () {
    return {
        restrict: 'CE',
        scope: {
            'message': '@'
        },
        template: '<span ng-repeat="fragment in fragments track by $index">' +
            '<span ng-if="!($index % 2)">' +
            '{{fragment}}' +
            '</span>' +
            '<a ng-if="$index % 2" href="javascript:void(0)" ng-click="openBrowser(fragment);">' +
            '{{fragment}}' +
            '</a>' +
            '</span>',
        link: function (scope) {
            if (!scope.message) {
                return;
            }
            var message = scope.message;
            scope.fragments = [];
            var regExp = /(?:https:\/\/|http:\/\/)[^\u2E80-\u9FFF\s]+/g;
            var sentences = message.split(regExp);
            var urls = message.match(regExp);
            if (urls) {
                for (var i = 0; i < sentences.length - 1; i++) {
                    scope.fragments.push(sentences[i]);
                    scope.fragments.push(urls[i]);
                }
                scope.fragments.push(sentences[sentences.length - 1]);
            } else {
                scope.fragments = sentences;
            }
            scope.openBrowser = nativeUI.openBrowser;
        }
    };
};