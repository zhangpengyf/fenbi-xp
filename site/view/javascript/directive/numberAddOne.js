'use strict';

module.exports = function () {
    return {
        require: '^ngModel',
        restrict: 'A',
        link: function (scope, element, attrs, ngModel) {
            ngModel.$parsers.unshift(function (val) {
                return parseInt(val, 10) - 1;
            });
            ngModel.$formatters.push(function (val) {
                return parseInt(val, 10) + 1;
            });
        }
    };
};