'use strict';
var $ = require('jquery');

module.exports = function () {
	return {
		restrict: 'CE',
		scope: {
			'totalStar': '@',
			'readOnly': '@',
			'fullStar': '@',
			'rate': '=onRate'
		},
		templateUrl: 'template/stars.html',
		link: function (scope) {
			scope.totalStar = parseInt(scope.totalStar) || 5;
			scope.fullStar = parseInt(scope.fullStar) || 0;
            scope.starArray = new Array(scope.totalStar);

			var fillStar = function () {
				for (var i = 0; i < scope.starArray.length; i++) {
					if (i < scope.fullStar) {
						scope.starArray[i] = 'full-star';
					} else {
						scope.starArray[i] = 'empty-star';
					}
				}
			};
			fillStar();
			scope.$watch('fullStar', function () {
				fillStar();
			});
			if (!scope.readOnly) {
				scope.mouseenter =  function (event) {
					var $target = $(event.target);
					scope.fullStar = parseInt($target.attr('star-index')) + 1;
					scope.rate(scope.fullStar);
					fillStar();
				};
			}
		}
	};
};
