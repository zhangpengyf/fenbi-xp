'use strict';

module.exports = function () {
    return {
        restrict: 'C',
        require: '^ngModel',
        scope: {
            fillColor: '=',
            bgColor: '='
        },
        link: function (scope, element, attrs, ngModel) {
            var bgColor = scope.bgColor || '#D9D9D9';
            function updateBg(value) {
                element.css('background-image', '-webkit-gradient(linear, left top, right top, color-stop(' + value + ', ' + scope.fillColor + '), color-stop(' + value + ', ' + bgColor + '))');
            }

            ngModel.$formatters.unshift(function (value) {
                updateBg(value);
                return value;
            });

            ngModel.$viewChangeListeners.push(function () {
                updateBg(ngModel.$modelValue);
            });

            updateBg(ngModel.$modelValue || 0);
        }
    };
};
