'use strict';

var $ = require('jquery');

module.exports = function () {
    return {
        restrict: 'E',
        transclude: true,
        scope: {
            'title': '@',
            'close': '&onClose',
            'hideCloseButton': '@',
        },
        templateUrl: 'template/dialog.html',
        link: function (scope, element) {
            $(element).appendTo(document.body);
        }
    };
};
