'use strict';
//var $ = require('jquery');

module.exports = function () {
	return {
		restrict: 'CE',
		scope: {
			'maxSingal': '=',
			'currentSingal': '='
		},
		templateUrl: 'template/singal.html',
		link: function (scope) {
			var i = 0;
            scope.singalArray = new Array(scope.maxSingal);
			for(i = 0; i < scope.singalArray.length; i++) {
				scope.singalArray[i] = 'singal-gray';
			}

			function fillSingal() {
				console.log('singal' + scope.currentSingal);

				if(scope.currentSingal < 3) {
					for ( i = 0; i < scope.currentSingal; i++) {
						scope.singalArray[i] = 'singal-weak';
					}
				}
				else if(scope.currentSingal == 3) {
					for ( i = 0; i < scope.currentSingal; i++) {
						scope.singalArray[i] = 'singal-strong';
					}
				}
				else{
					for ( i = 0; i < scope.currentSingal; i++) {
						scope.singalArray[i] = 'singal-good';
					}
				}

				for (i = scope.currentSingal; i < scope.singalArray.length; i++) {
					scope.singalArray[i] = 'singal-gray';
				}
			}

			scope.$watch('currentSingal', function () {
				fillSingal();
			});

			fillSingal();
		}
	};
};
