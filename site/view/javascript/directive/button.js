'use strict';

module.exports = function () {
	return {
		restrict: 'CE',
		link: function (scope, element) {
			if (element.hasClass('disabled')) {
				element.unbind();
			} else {
				element.on('mouseenter', function () {
					element.addClass('hovering');
				});
				element.on('mouseleave', function () {
					element.removeClass('hovering');
				});

			}
		}
	};
};