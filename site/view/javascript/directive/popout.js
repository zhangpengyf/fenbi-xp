'use strict';
var $ = require('jquery');

module.exports = function () {
	return {
		restrict: 'ACE',
		scope: {
			popoutIndex: '=',
			popoutIndexClass: '@',
			popoutInner: '@',
			mutualId: '@popoutMutualId',
			onShow: '&'
		},
		link: function (scope, element) {
			var enterPopoutElem = false;
			var enterElem = false;
			var $popoutElem = null;
			if (scope.popoutInner) {
				$popoutElem = $(element).children('.popout');
			} else if (scope.popoutIndexClass) {
				$popoutElem = $('.popout_' + scope.popoutIndex);
			} else {
				$popoutElem = $('#popout_' + scope.popoutIndex);
			}
			if (scope.mutualId) {
				$popoutElem.addClass('popout_mutual_' + scope.mutualId);
			}
			element.on('mouseenter', function () {
				enterElem = true;
				$('.popout_mutual_' + scope.mutualId ).addClass('display-none');
				$popoutElem.removeClass('display-none');
				$popoutElem.css('visibility', 'hidden');
				setTimeout(function () {
					if (scope.onShow) {
						scope.onShow({elem: $(element)});
					}
					$popoutElem.css('visibility', 'visible');
				}, 0);
			});

			$popoutElem.on('mouseenter', function () {
				enterPopoutElem = true;
			});


			$popoutElem.on('mouseleave', function () {
				enterPopoutElem = false;
				$popoutElem.addClass('display-none');
			});

			element.on('mouseleave', function () {
				enterElem = false;
				setTimeout(function () {
					if (!enterPopoutElem && !enterElem) {
						$popoutElem.addClass('display-none');
					}
				}, 500);
			});
		}
	};
};