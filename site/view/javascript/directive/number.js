'use strict';

module.exports = function () {
    return {
        require: '^ngModel',
        restrict: 'A',
        scope: {
            min: '@numberMin',
            max: '@numberMax'
        },
        link: function (scope, element, attrs, ngModel) {
            scope.min = scope.min || 1;
            scope.max = scope.max || 1;
            ngModel.$parsers.unshift(function (val) {
                var $modelValue;
                if (val || val === 0) {
                    $modelValue = parseInt(val.toString().replace(/[^0-9]/,''), 10);
                    $modelValue = $modelValue < scope.min ? scope.min : $modelValue;
                    $modelValue = $modelValue > scope.max? scope.max : $modelValue;
                }
                return $modelValue;
            });
            ngModel.$formatters.push(function (val) {
                var $viewValue;
                if (val || val === 0) {
                    $viewValue = parseInt(val.toString().replace(/[^0-9]/,''), 10);
                    $viewValue = $viewValue < scope.min ? scope.min : $viewValue;
                    $viewValue = $viewValue > scope.max? scope.max : $viewValue;
                }
                return $viewValue;

            });
        }
    };
};