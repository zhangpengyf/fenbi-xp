'use strict';
//truman.userType = 1;
var angular = require('angular');
angular.module('ng').run(['$rootScope', function($rootScope) {
    $rootScope.safeApply = function(fn) {
        var phase = this.$root.$$phase;
        if(phase == '$apply' || phase == '$digest') {
            if(fn && (typeof(fn) === 'function')) {
                fn();
            }
        } else {
            this.$apply(fn);
        }
    };
}]);

require('angular-router-browserify')(angular);
require('angular-local-storage');
require('../javascript/controller/controllers');
require('../javascript/service/services');
require('../javascript/module/angular-file-upload');
require('../javascript/module/angular-timer');

var trumanApp = angular.module('trumanApp', [
    'ngRoute',
    'angularFileUpload',
    'timer',
    'LocalStorageModule',
    'trumanControllers',
    'trumanServices'
]);

trumanApp.directive('trumanMaxlength', require('../javascript/directive/maxLength'))
.directive('button', require('../javascript/directive/button'))
.directive('trumanPopout', require('../javascript/directive/popout'))
.directive('fillscreen', require('../javascript/directive/fillscreen'))
.directive('volumeEnergy', require('../javascript/directive/volumeEnergy'))
.directive('trumanStars', require('../javascript/directive/stars'))
.directive('trumanSingal', require('../javascript/directive/singal'))
.directive('trumanLabel', require('../javascript/directive/label'))
.directive('trumanDialog', require('../javascript/directive/dialog'))
.directive('slide', require('../javascript/directive/slide'))
.directive('chatMessage', require('../javascript/directive/chatMessage'))
.directive('trumanNumber', require('../javascript/directive/number'))
.directive('numberAddOne', require('../javascript/directive/numberAddOne'))
.directive('videoPanel', require('../javascript/directive/videoPanel'));

trumanApp.config([
    '$routeProvider',
    '$httpProvider',
    function ($routeProvider, $httpProvider) {
        $routeProvider.
            when('/episodes', {
                templateUrl: 'page/episodeList.html',
                controller: 'EpisodeListCtrl'
            }).
            when('/course', {
                templateUrl: 'page/courseList.html',
                controller: 'CourseListCtrl'
            }).
            when('/favorite', {
                templateUrl: 'page/favoriteList.html',
                controller: 'FavoriteListCtrl'
            }).
            when('/live', {
                templateUrl: 'page/episode.html',
                controller: 'EpisodeCtrl'
            }).
            when('/replay', {
                templateUrl: 'page/replay.html',
                controller: 'ReplayCtrl'
            }).
            when('/enter', {
                templateUrl: 'page/episodeEnter.html',
                controller: 'EpisodeEnterCtrl'
            }).
            otherwise({
                redirectTo: '/episodes'
            });
        $httpProvider.interceptors.push('HttpInterceptorService');
  }]
);
trumanApp.run([
    'EventListenerService',
    function (EventListenerService) {
        window.EventListenerService = EventListenerService;
    }
]);