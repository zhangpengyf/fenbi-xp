'use strict';

var angular = require('angular');
angular.module('ng').run(['$rootScope', function($rootScope) {
    $rootScope.safeApply = function(fn) {
        var phase = this.$root.$$phase;
        if(phase == '$apply' || phase == '$digest') {
            if(fn && (typeof(fn) === 'function')) {
                fn();
            }
        } else {
            this.$apply(fn);
        }
    };
}]);

require('angular-router-browserify')(angular);

require('../javascript/update-controllers');

var trumanUpdateApp = angular.module('trumanUpdateApp', [
    'ngRoute',
    'trumanUpdateControllers'
]);

trumanUpdateApp.factory('WebApiService', require('../javascript/service/webApi')).
    factory('CourseMap', require('../javascript/service/CourseMap'));

trumanUpdateApp.config([
    '$routeProvider',
    function($routeProvider) {
        $routeProvider.
            when('/updating', {
                templateUrl: 'page/updating.html',
                controller: 'UpdatingCtrl'
            }).
            when('/noUpdates', {
                templateUrl: 'page/noUpdates.html',
                controller: 'NoUpdateCtrl'
            }).
            when('/updateError', {
                templateUrl: 'page/updateError.html',
                controller: 'UpdateErrorCtrl'
            }).
            when('/updateAvailable', {
                templateUrl: 'page/updateAvailable.html',
                controller: 'UpdateAvailableCtrl'
            }).
            otherwise({
                redirectTo: '/updating'
            });
    }
]);
